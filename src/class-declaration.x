#define C(x)     CPP_EXPAND(x)CPP_EXPAND(CLS)
#define VC(x)    virtual CPP_EXPAND(x)CPP_EXPAND(CLS)
#define VM(name) virtual  auto name
#define SM(name) static   auto name
#define M(name)           auto name
#define CO(type) operator      type
#define O(op)             auto operator op
 