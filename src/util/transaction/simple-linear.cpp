#include "stdinc.h"

#include <stack>
#include <vector>
#include <functional>

#include "head/util/transaction/simple-linear.h"

#if E_COMPILER == COMP_CLANG
#  pragma clang diagnostic ignored "-Wdangling-else"
#endif

namespace util        {
namespace transaction {
   using namespace ::config::util::res;
   using std::function;
   using std::stack;
   using std::vector;
   using std::make_pair;
   using std::pair;

    // count type
   typedef simpleLinear::StepN StepN;

   // TransactionStep(step/undo, step) -> success
   typedef simpleLinear::TransactionStep TransactionStep;

   typedef simpleLinear::TransactionPair TransactionPair;

   // error handler
   // TransactionFail(stepOrUndo, step, offendingStep, offendingUndo) -> recovered
   typedef simpleLinear::TransactionFail TransactionFail;
 

   const TransactionFail simpleLinear::FailSilent = [](bool,StepN,TransactionStep,TransactionStep)->bool{return false;};
   const TransactionStep simpleLinear::EmptyStep  = [](bool,StepN)->bool{return true;};

   void simpleLinear::appendCommit() {
      transactionList.push_back(make_pair(([this](bool,StepN)->bool {
         // commit
         isComplete = true;
         if (convert)
            return invert();
         return true;
      }), EmptyStep));
   }

   bool simpleLinear::rewindModified(StepN where, TransactionFail fail) {
      if (where < curStep)
         if (undo(curStep - where))
            if (fail(true, curStep, nullptr, nullptr))
               if (undo(curStep - where))
                  return false;
      return true;
   }

   simpleLinear::simpleLinear(bool _convert, TransactionFail _stepFail)
   : convert(_convert)
   , isInverted(false)
   , isComplete(false)
   , curStep(0)
   , transactionList()
   , stepFail(_stepFail)
   {
      appendCommit();
   }

   // form == true  -> {{do, undo}, {do, undo}, ...}
   // form == false -> {do+undo, do+undo, do+undo, ...}
   simpleLinear::simpleLinear( initializer_list<TransactionStep> steps
                           , bool _convert 
                           , TransactionFail _stepFail 
                           , bool form 
                           )
   : convert(_convert)
   , isInverted(false)
   , isComplete(false)
   , curStep(0)
   , transactionList()
   , stepFail(_stepFail)
   {
      TransactionStep t;
      for (auto step: steps) 
         if (t && form) {
            transactionList.push_back(make_pair( t    ? t    : EmptyStep
                                               , step ? step : EmptyStep
                                               ));
            t = nullptr;
         } else if (form) 
            t = step;
         else
            transactionList.push_back(make_pair( step ? step : EmptyStep
                                               , step ? step : EmptyStep
                                               ));
      appendCommit();
   }

   simpleLinear::~simpleLinear() {
      if (!isComplete && !isInverted)
         rollback();
   }

   bool simpleLinear::addStep(TransactionStep perform) {
      return addStep(perform, EmptyStep);
   }

   bool simpleLinear::addStep(TransactionStep perform, TransactionStep undo) {
      assert(!isComplete && !isInverted);
      if (isComplete || isInverted)
         return false;

      if (!rewindModified(transactionList.size() - 1, stepFail))
         return false;

      transactionList.insert( transactionList.end() - 1
                            , make_pair( perform ? perform : EmptyStep
                                       , undo    ? undo    : EmptyStep
                                       ));
      return true;
   }

   bool simpleLinear::addStep(StepN where, TransactionStep perform) {
      return addStep(where, perform, EmptyStep);
   }

   bool simpleLinear::addStep(StepN where, TransactionStep perform, TransactionStep undo) {
      assert(!isComplete && !isInverted);
      if (isComplete || isInverted)
         return false;

      if (!rewindModified(where, stepFail))
         return false;

      vector<TransactionPair>::iterator what;
      if ((what = transactionList.begin() + where) < transactionList.end() - 1)
         transactionList.insert(what, make_pair(perform, undo));
      else
         transactionList.insert( transactionList.end() - 1
                               , make_pair( perform ? perform : EmptyStep
                                          , undo    ? undo    : EmptyStep
                                          ));
      return true;
   }

   bool simpleLinear::delStep(StepN where) {
      assert(!isComplete && !isInverted);
      if (isComplete || isInverted)
         return false;

      if (!rewindModified(where, stepFail))
         return false;

      vector<TransactionPair>::iterator what;
      if ((what = transactionList.begin() + where) < transactionList.end() - 1)
         transactionList.erase(what);
      else
         return false;
      return true;
   }

   // total steps
   StepN simpleLinear::steps() const {
      return transactionList.size();
   }

   // currently at step
   StepN simpleLinear::atStep() const {
      return curStep;
   }

   // step forward
   StepN simpleLinear::step(StepN count, TransactionFail fail) {
      assert(count >= 0);
      assert(curStep >= 0);
      TransactionPair p;
      fail = nvl(fail, stepFail);

      if (isComplete)
         return 0;

      assert(count - curStep > 0);

      for (; count; count--) {
         if (curStep >= (StepN)transactionList.size()) {
            fail(true, curStep, nullptr, nullptr);
            return count;
         }

         assert(curStep >= 0 && curStep < (decltype(curStep))transactionList.size());

         p = transactionList[curStep];
         if (!p.first(true, curStep)) {
            if (!p.second(false, curStep))
               if (fail(true, curStep, p.first, p.second))
                  undo(1, fail);
               else
                  return count;
            else {
               count++;
               curStep--;
               continue;
            }

         } else
            curStep++;
      }
      return count;
   }

   // go until 
   StepN simpleLinear::until(StepN until, TransactionFail fail) {
      fail = nvl(fail, stepFail);
      until = ::util::math::min(until, transactionList.size());
      until = until - curStep;
      return step(until);
   }

   // step back
   StepN simpleLinear::undo(StepN count, TransactionFail fail) {
      assert(count >= 0);
      assert(curStep > 0);
      TransactionPair p;
      fail = nvl(fail, stepFail);

      // inverted lacks undo!!!
      if (isComplete || isInverted)
         return 0;

      for (; count; count--) {
         if (curStep < 0) {
            fail(false, curStep, nullptr, nullptr);
            return count;
         }

         p = transactionList[--curStep];
         if (!p.second(false, curStep))  
            if (!fail(false, curStep, p.first, p.second))
               return count;
            else {
               count++;
               curStep++;
               continue;
            } 
      }
      return count;
   }

   // undo until
   StepN simpleLinear::unto(StepN until, TransactionFail fail) {
      until = ::util::math::max(0, until);
      until = curStep - until;
      fail = nvl(fail, stepFail);
      if (until >= 0)
         return undo(until);
      else
         return until;
   }

   // perform a transaction or roll back
   bool simpleLinear::transact(TransactionFail fail) {
      return until(TRANSACTION_STEP_N_MAX, fail) == 0;
   }

   bool simpleLinear::rollback(TransactionFail fail) {
      return unto(0, fail) == 0;
   }

   bool simpleLinear::completed() const {
      return isComplete;
   }

   // get a transaction of undos (useful for resource release at a
   // later poStepN in time).
   bool simpleLinear::invert() {
      using std::reverse;

      if (!isInverted) {
         vector<TransactionPair> inverted;
         transactionList.swap(inverted);
         inverted.pop_back();
         reverse(inverted.begin(), inverted.end());

         for (auto x: inverted)
            transactionList.push_back(make_pair(x.second, EmptyStep));
         transactionList.push_back(make_pair([this](bool, StepN)->bool {
            isComplete = true;
            return true;
         }, EmptyStep));

         curStep = 0;
         isComplete = false;
         isInverted = true;

         return true;
      }
      return false;
   }

   bool simpleLinear::inverted() const {
      return isInverted;
   }

   void simpleLinear::invertTo(simpleLinear& child) const {
      using std::move;
      using std::back_inserter;
      
      assert(this);
      assert(&child);

      child.isInverted = false;
      child.isComplete = false;
      child.curStep    = curStep;
      child.stepFail   = stepFail;

      child.transactionList.reserve(transactionList.size());
      move(transactionList.begin(), transactionList.end(), back_inserter(child.transactionList));

      child.invert();

      //for (auto i = transactionList.rbegin(); i != transactionList.rend(); i++)
      //   child.transactionList.push_back(make_pair(i->second, EmptyStep));

      //child.transactionList.push_back(make_pair
      //   ([this, &child](bool, StepN)->bool {
      //      child.isComplete = true;
      //      return true;
      //   }, EmptyStep));
   }

}}
