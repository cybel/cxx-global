#include "stdinc.h"

#include "head/util/pchrono.h"

RealMoment    realStart(std::chrono::system_clock::now());
ProgramMoment programStart(ProgramClock::now());

