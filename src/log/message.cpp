#include "stdinc.h"

#include "head/util/bitfield.h"

#include "head/log/levels.h"
#include "head/log/scopes.h"
#include "head/log/topics.h"
#include "head/log/message.h"

#include "head/util/fastpool.h"

namespace logging {
namespace message {
   using boost::mutex;
   using boost::shared_mutex;
   using boost::shared_lock;
   using boost::unique_lock;
   using boost::upgrade_to_unique_lock;
   using std::atomic;
   using util::math::sclamp;

   //
   // this is a simple memory pooling system for short-lived rapidly
   // acquired/released objects - vis a vis messages.
   // 
   //
   //      /¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
   //      v                                           v
   //    Bank 0 <-> Bank 1 <-> Bank 2 <-> Bank 3 <-> Bank 4
   //    (head)                  |
   //                            |
   //     (Slow allocation)      |               (Locking)
   // - - - - - - - - - - - - - -|- - - - - - - - - - - - - - - -
   //     (Fase allocation)  [Bitfield]         (Lock-free)
   //                         /  |  \
   //                        /   |   \
   //                  Message   |   Message
   //                         Message
   //
   // When the bias is less than zero, searches go backward. Likewise
   // a positive bias will go forward. Similarily, biases will yield
   // fresh banks in the search direction. Each successful search 
   // will bias the number of banks walked in the opposite direction.
   // In this manner, the banks should grow away from head, yielding
   // an approximately n/2 walk distance at any given moment to a 
   // successful allocation. Searching within a bank is itself 
   // O(log_64 M) where M is bits tracked. Where M is smaller than
   // the containing type (uint8 thru uint64), it is O(1).
   //

/*
   typedef bitfield<MESSAGE_COLLECTION_SIZE> CollectionBitfield;
   typedef message Collection[MESSAGE_COLLECTION_SIZE];

   struct messageBank_S {
      Collection         collection;
      CollectionBitfield active;

      messageBank_S() {
         for (msize i = 0; i < MESSAGE_COLLECTION_SIZE; i++) {
            collection[i].bank = this;
            collection[i].bankItem = i;
      }
      }

      atomic<messageBank_S*> prev;
      atomic<messageBank_S*> next;
   };
   CompactTypes(messageBank,_S);

   /////////////////////////////////////////////////////////////////////
   ////// Message Banks // (internal)
   //////////////////////

   namespace {
      shared_mutex bankGrow;
      shared_mutex bankShrink;

      atomic<messageBankPtr> head;
      atomic<msize>          banks;
      atomic<sint16>         bias;
      atomic<sint16>         flow;

#     define bankRead()   shared_lock<shared_mutex> __readLock(bankShrink)
#     define bankShrink() unique_lock<shared_mutex> __shrinkLock(bankShrink)
#     define bankGrow()   unique_lock<shared_mutex> __growLock(bankGrow)


      // ALLOCATION: create a bank. Destroyed when it is determined that
      // there is a surplus
      messageBankPtr bankCreate() {
         return new messageBank;
      }

      // DEALLOCATION: destroy a bank/
      void bankDestroy(messageBankPtr from) {
         delete from;
      }

      // TRIGGER: cause allocation, add to tracking 
      messageBankPtr bankExpand() {
         messageBankPtr n = bankCreate();

         if (!head) {
            n->next = n;
            n->prev = n;
            head = n;
            flow = 1;
            return head;
         }

         if (bias < 0) {
            // before head
            n->next.exchange(head);
            n->prev.exchange((*head).prev);
            (*(*head).prev).next.exchange(n);
            (*head).prev.exchange(n);
         } else  {
            // after head
            n->prev.exchange(head);
            n->next.exchange((*head).next);
            (*(*head).next).prev.exchange(n);
            (*head).next.exchange(n);
         }

         banks++;
         return n;
      }

      // TRIGGER: unstitch and destroy
      void bankContract(messageBankPtr at) {
         (*at->prev).next.exchange((*at).next);
         (*at->next).prev.exchange((*at).prev);
         banks--;
         bankDestroy(at);
      }

      alwaysinline 
      messagePtr bankAcquire() {
         // bankRead since we're acquiring, and might grow
         bankRead();

         messageBankPtr i = head;
         msize index = 0;
         bool step = bias < 0;
         sint16 stepF = 0;

         if (!head) {
            bankGrow();
            i = bankExpand();
         }

         do {
            stepF++;
            if ((index = i->active--) != CollectionBitfield::BitInvalid)
               break;
            if (step)
               i = i->prev;
            else
               i = i->next;
         } while (i != head);

         if (index == CollectionBitfield::BitInvalid) {
            bankGrow();
            i = bankExpand();
            index = i->active--;
         }

         bias += sclamp(step ? stepF : -stepF, BIAS_MIN, BIAS_MAX);
         flow++;

         return (i->collection)+index;
      }

      alwaysinline 
      void bankRelinquish(messageCPtr what) {
         // DON'T "bankRead"! This is an atomic operation!
         // also, it'll deadlock bankContract!
         //assert(!what->line);
         //assert(!what->lines);

         flow--;

         DEBUG_GUARD_ASSERT(*what, 0);
         DEBUG_GUARD_ASSERT(*what, 1);

         messageBankPtr bank = what->bank;
         bank->active << what->bankItem;
         if (  flow < FLOW_SHRINK 
            && bank->active.empty()
            && (bank->next != bank) // keep head (SEGV)
            ) {
            // contraction requires nobody enter the bit being removed
            bankShrink();
            flow += -FLOW_SHRINK;
            bankContract(bank);
         }
      }
   }
*/
   /////////////////////////////////////////////////////////////////////
   ////// Message Banks // (public)
   //////////////////////

   /*messagePtr getMessage() {
      return bankAcquire();
   }

   void returnMessage(messageCPtr msg) {
      bankRelinquish(msg);
   }*/

   util::fastPool<message,MESSAGE_COLLECTION_SIZE,BIAS_MIN,BIAS_MAX,FLOW_SHRINK> pool;

   messagePtr getMessage() {
      return pool.acquire();
   }

   void returnMessage(messageCPtr msg) {
      pool.acquiesce(msg);
   }

}}