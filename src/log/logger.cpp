#include "stdinc.h"

#include "head/log/levels.h"
#include "head/log/topics.h"
#include "head/log/scopes.h"
#include "head/log/message.h"
#include "head/log/formats.h"
#include "head/log/destination.h"

#include "head/log/logger.h"

namespace logging {
   using namespace ::config::logging;
   using namespace bits;

   using std::atomic;
   using std::chrono::microseconds;
   using std::memcpy;
   using std::thread;
   using std::vector;
   using std::snprintf;
   using util::math::min;
   using namespace logging::destination;
   using namespace logging::filter;
   using namespace logging::format;
   using namespace logging::location;
   using namespace logging::message;
   using namespace std::placeholders;
   using namespace std::this_thread;


   /////////////////////////////////////////////////////////////////////
   ////// Logger State // (opaque, internal)
   /////////////////////

   struct loggerState_S {
      DestinationPtr dest;
      FormatterPtr   format;

      // buffering and pumping
      atomic<uint64>              bufferSet;
      atomic<uint64>              readSet;

      DEBUG_GUARD(0);
      atomic<vector<messagePtr>*> messageQueue[PUMP_QUEUE_COUNT];
      DEBUG_GUARD(1);

      // shared
      topicCollection   topics;

      // pump
      atomic<bool> running;
      thread*      pumpThread;

      loggerState_S()
      : dest(nullptr)
      , format(nullptr)
      , bufferSet(0)
      , readSet(0)
      , topics()
      , running(false)
      , pumpThread(nullptr)
      {
         for (msize i = 0; i < PUMP_QUEUE_COUNT; i++)
            messageQueue[i] = new vector<messagePtr>;
         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);
      }

      ~loggerState_S() {
         terminate();
         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);
      }

      void terminate() {
         if (!running)
            return;

         running = false;

         if (pumpThread->get_id() != get_id())
            pumpThread->join();

         delete pumpThread;

         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);

         vector<messagePtr>* qu = nullptr;
         for (msize i = 0; i < PUMP_QUEUE_COUNT; i++) {
            for (msize j = 0; j <100 && !qu; j++) 
               qu = messageQueue[i].exchange(nullptr);
            if (qu) {
               delete qu;
               qu = nullptr;
            }
         } 

         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);
      }

      void push(messagePtr msg) {
         uint64 q; 
         vector<messagePtr>* qu = nullptr;

         if (!running) {
            returnMessage(msg);
            return;
         }

         // lock free exclusive grab.
         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);
         do {
            q = bufferSet.fetch_add(1) % PUMP_QUEUE_COUNT;
         } while (!(qu = messageQueue[q].exchange(nullptr)) && running);

         if (qu) {
            qu->push_back(msg);

            messageQueue[q].exchange(qu);
         }
      }

      void formatQueue(vector<string>& mout) {
         uint64 q; 
         vector<messagePtr>* qu = nullptr;

         // lock free exclusive grab.
         DEBUG_GUARD_ASSERT(*this,0);
         DEBUG_GUARD_ASSERT(*this,1);
         do {
            q = readSet.fetch_add(1) % PUMP_QUEUE_COUNT;
         } while (!(qu = messageQueue[q].exchange(nullptr)) && running);

         if (qu) {
            format->format(qu->begin(), qu->end(), mout);
            for (auto m: *qu)
               returnMessage(m);

            qu->clear();
            messageQueue[q].exchange(qu);
         }

      }

      void pump() {
         vector<string> mout;        
         formatQueue(mout);
         dest->write(mout.begin(), mout.end());
      }
   };


   /////////////////////////////////////////////////////////////////////
   ////// Logger Construction/Destruction // 
   ////////////////////////////////////////

   // Mode: Root
   logger_S::logger_S( DestinationRef dest
                     , FormatterRef format
                     , cstr programName
                     , cstr programPath
                     , MLevelNT _level
                     , microseconds pumpPeriod
                     ) {
      // ALLOCATION: new source (program-wide, program-length lifespan)
      // ALLOCATION: state
      mode   = LogMode::ROOT_LEVEL;
      src = new source(programName, [&programPath](EnscopeFunc enscope) {
         enscope(SourceNature::PROGRAM, programPath);
      });
      scp.push({SourceNature::PROGRAM, programPath});
      level = _level;

      active = nullptr;
      //tpc = nullptr;

      state  = new loggerState;
      state->dest = &dest;
      state->format = &format;

      state->running = true;
      state->pumpThread = new thread(bind(&logger::logPump, this, _1), pumpPeriod);
   }

   // Mode: File
   logger_S::logger_S( logger_S& root
                     , sourceRef _source, SourceNature nature, cstr filePath
                     , MLevelNT _level 
                     ) {
      mode = LogMode::FILE_LEVEL;
      parent = &root;
      src = &_source;
      scp.push({nature, filePath});
      assert(getSource().contains(nature, filePath));
      level = ( _level != (MLevelNT)MLevel::MAX_VALUE__ 
              ? _level
              : root.level
              ? root.level
              : LEVEL_DEFAULT
              );

      active = nullptr;
      //tpc = nullptr;
   }

   // Mode: Leaf
   logger_S::logger_S( logger_S& file
                     , SourceNature nature, cstr path
                     , MLevelNT _level 
                     ) {
      mode = LogMode::LEAF_LEVEL;
      parent = &file;
      src = nullptr;
      scp.push({nature, path});
      assert(parent->getSource().contains(nature, path));
      level = ( _level != (MLevelNT)MLevel::MAX_VALUE__ 
              ? _level
              : file.level
              ? file.level
              : LEVEL_DEFAULT
              );
      
      active = nullptr;

      topicPtr t = getTopics()->currently();
      if (t) {
         topicInherit = true;
         tpc.push(t);
      }
   }

   logger_S::~logger_S() {
      if (active)
         logImmediateFlush();

      if (tpc.size())
         while (tpc.size() > (topicInherit ? 1 : 0)) 
            conclude();

      // DEALLOCATION: release program-wide source
      // DEALLOCATION: release root logger state    
      if (mode == LogMode::ROOT_LEVEL) {
         if (src)
            delete src;
         if (state) 
            delete state;
      }
   }


   /////////////////////////////////////////////////////////////////////
   ////// Logger state query //              
   ///////////////////////////             

   // get info
   LogMode logger_S::getMode()   const {
      return mode;
   }

   loggerPtr logger_S::getParent() const {
      if (mode != LogMode::ROOT_LEVEL)
         return parent;
      return nullptr;
   }

   MLevelNT logger_S::getLevel()  const {
      return level;
   }

   sourceRef logger_S::getSource() {
      assert(src || parent);
      return src ? *src : parent->getSource();
   }

   cscopeRef logger_S::getScope()  const {
      return scp.top();
   }

   topicPtr logger_S::getTopic() {
      return tpc.top();
   }


   /////////////////////////////////////////////////////////////////////
   ////// Modes //
   //////////////

   loggerRef logger_S::numericFormat(NumericFormat nf) {
      numericModeInvalidate();
      nFmt = nf;
      return *this;
   }

   loggerRef logger_S::numericWidth(uint8 w) {
      numericModeInvalidate();
      nWidth = w;
      return *this;
   }

   loggerRef logger_S::numericPrecision(uint8 p) {
      numericModeInvalidate();
      nPrecision = p;
      return *this;
   }

   loggerRef logger_S::numericZeroPad(bool p) {
      numericModeInvalidate();
      nZeroPad = p;
      return *this;
   }

   loggerRef logger_S::numericSpacePad(bool p) {
      numericModeInvalidate();
      nSpacedSign = p;
      return *this;
   }

   loggerRef logger_S::numericPositiveSign(bool s) {
      numericModeInvalidate();
      nPositiveSign = s;
      return *this;
   }

   loggerRef logger_S::numericJustifyLeft(bool l) {
      numericModeInvalidate();
      nLeftJustify = l;
      return *this;
   }


   /////////////////////////////////////////////////////////////////////
   ////// Logger Location //
   ////////////////////////


   loggerRef logger_S::begin(cstrc name) {
      tpc.push(getTopics()->begin(name));
      return *this;
   }

   loggerRef logger_S::conclude() {
      if ((tpc.size() > (topicInherit ? 1 : 0)) ) {
         getTopics()->conclude(tpc.top());
         tpc.pop();
      }
      return *this;
   }

   loggerRef logger_S::enter(SourceNature nature, cstrc path) {
      scp.push({nature, path});
      return *this;
   }

   loggerRef logger_S::exit() {
      if (scp.size() == 1)
         return *this;
      scp.pop();
      return *this;
   }

   loggerRef logger_S::setLevel(MLevel _level) {
      //printf(">>> %s %X %X\n", scp.top().path, level, _level);
      level = (MLevelNT) _level;
      return *this;
   }



   /////////////////////////////////////////////////////////////////////
   ////// Logger Push //              
   ////////////////////  

   namespace {
      cstrc clip  = " ... (message clipped)";
      cstrc trunc = " ... (truncated)";
      const msize clipLen = strlen(clip);
      const msize truncLen = strlen(trunc);
   }

#  if E_DEBUG & DEBUG_NO_SUPPRESS_WARN != 0
#     if E_COMPILER == COMP_CLANG
#        pragma clang diagnostic push
#        pragma clang diagnostic ignored "-Wsign-compare"
#     endif
#  endif
   loggerRef logger_S::log(cstr msg) {
      if (!logImmediateBegin())
         return *this;

      sint32 i = 0, o = 0;
      sint32 slen = strlen(msg);
      cstr   segment[MESSAGE_LINES+1];
      sint32 segLen;
      cstr   result = msg;

      assert(active->lines <= MESSAGE_LINES);

      if (slen == 0)
         return *this;

      if (active->lines == MESSAGE_LINES)
         return *this;

      if (msg[0] == '\n' && active->lines + 1 <= MESSAGE_LINES)
         active->lines++;

      do {
         segment[o] = (*result == '\n' ? result+1 : result);
         result++;
         o++;
      } while ((result = strchr(result, '\n')) && o < MESSAGE_LINES);

      for (i = 0; i < o; i++) {
         str begin = nullptr;
         msize beginO = 0;
         
         assert(active->lines <= MESSAGE_LINES);         
         if (active->lines == MESSAGE_LINES) {
            sprintf( active->line[active->lines - 1] 
                   + min( strlen(active->line[active->lines - 1])
                        , MESSAGE_LINE_LENGTH - clipLen
                        )
                   , "%s"
                   , clip
                   );
            break;
         }

         assert(active->lines < MESSAGE_LINES);
         begin  = active->line[active->lines];
         beginO = strnlen(begin, MESSAGE_LINE_BUFFER);
         segLen = (i+1 < o ? segment[i+1] : msg + slen) - segment[i];
         segLen = min(segLen, MESSAGE_LINE_BUFFER - beginO - 1);

         if (segment[i][segLen-1] == '\n')
            segLen -= 1;

         memcpy( (void*) (begin + beginO)
               , (void*) segment[i]
               , segLen + 1
               );
         *((begin + beginO) + min(segLen, MESSAGE_LINE_BUFFER - 1)) = '\0';

         if (segLen > MESSAGE_LINE_LENGTH) {
            segLen = MESSAGE_LINE_BUFFER - truncLen;
            memcpy( (void*) (begin + beginO + segLen - 1)
                  , (void*) trunc
                  , truncLen + 1
                  );
         } 
         
         if ((i+1) < o)
            active->lines++;
      }

      return *this;
   }
#  if E_DEBUG & DEBUG_NO_SUPPRESS_WARN != 0
#     if E_COMPILER == COMP_CLANG
#        pragma clang diagnostic pop
#     endif
#  endif

   loggerRef logger_S::log(chr c) {
      const chr s[2] = {c, 0};
      return log(s);
   }

   loggerRef logger_S::log() {
      logImmediateFlush();
      return *this;
   }


   /////////////////////////////////////////////////////////////////////
   ////// Logger Internals //              
   /////////////////////////

   topicCollectionPtr logger_S::getTopics() {
      if (mode == LogMode::ROOT_LEVEL)
         return &state->topics;
      else
         return parent->getTopics();
   }

   bool logger_S::logImmediateBegin() {
      if (mode == LogMode::ROOT_LEVEL)
         return false;

      if (!active) {
         active = getMessage();
         // basic setup
         active->lines = 0;
         for (msize i = 0; i < MESSAGE_LINES; i++)
            active->line[i][0] = '\0';
      }

      return active;
   }

   void logger_S::logImmediateFlush() {
      if (mode == LogMode::ROOT_LEVEL)
         return;

      if (!active)
         return;

      if (active->lines < MESSAGE_LINES && active->line[0][0] != '\0')
         active->lines++;

      assert(src || parent);

      active->threadId      = get_id();
      active->cothread      = co_active();
      active->time          = ProgramClock::now();
      active->levelN        = level;
      active->origin.source = src ? src : parent ? parent->src : nullptr;
      scopeRef scpc         = scp.top();
      active->origin.nature = scpc.nature;
      active->origin.path   = scpc.path;
      active->topic         = tpc.size() ? tpc.top() : nullptr;

      parent->logPush(active);

      active = nullptr;
   }

   void logger_S::numericModeInvalidate() {
      nFormat[0] = '\0';
   }

   void logger_S::numericModeActualize(cstrc sizeMod, bool Signed, bool Floating) {
      if (  nFormat[0] != '%' 
         || nSigned    != Signed
         || nFloating  != Floating
         || strcmp(nSizeMod, sizeMod)
         ) {
            nSigned = Signed;
            nFloating = Floating;
            strcpy(nSizeMod, sizeMod);
            numericFormatStrBase( nFormat
                                , nSizeMod
                                , nSigned
                                , nFloating
                                , nFmt
                                , nWidth
                                , nPrecision
                                , nZeroPad
                                , nPositiveSign
                                , nSpacedSign
                                , nLeftJustify
                                );
      }
   }

   bool logger_S::filterMessage(cmessagePtr msg) {
      bool ok = accept(level, msg->levelN);

      //printf("Filter 1 %s (%s) %X %X >> %s\n", scp.top().path, ok ? "in" : "out", level, msg->levelN, msg->line[0]);
      
      if (mode == LogMode::ROOT_LEVEL) {
         ok = ok 
           && (msg->origin.source) ? !msg->origin.source->mute() : true
           && (msg->topic)         ? !msg->topic->mute()         : true
            ;
      }

      //printf("Filter 2 %s (%s) >> %s\n", scp.top().path, ok ? "in" : "out", msg->line[0]);

      return ok;
   }

   void logger_S::logPush(messagePtr msg) {
      if (!filterMessage(msg)) {
         returnMessage(msg);
         return;
      }

      if (mode != LogMode::ROOT_LEVEL) {
         assert(parent);
         parent->logPush(msg);
      } else
         state->push(msg);
   }

   /////////////////////////////////////////////////////////////////////
   ////// Logger Pump Thread //              
   ///////////////////////////  

   void logger_S::logPump(microseconds pumpPeriod) {
      do {
         auto tstart = ProgramClock::now();

         state->pump();

         auto tend = ProgramClock::now();
         while (tend - tstart < pumpPeriod) {
            sleep_for(pumpPeriod - (tend-tstart));
            tend = ProgramClock::now();
         } 
            
      } while (state->running);
   }


   /////////////////////////////////////////////////////////////////////
   ////// Numerics //
   /////////////////

   namespace bits {

      cstr numericFormatStrBase( str           fs
                               , cstrc         sizeMod
                               , bool          Signed
                               , bool          Floating
                               , NumericFormat fmt          
                               , uint8         width        
                               , sint8         precision    
                               , bool          zeroPad      
                               , bool          positiveSign 
                               , bool          spacedSign   
                               , bool          leftJustify  
                               ) {
         uint8 o = 0;
         fs[o++] = '%';

         if (leftJustify)
            fs[o++] = '-';

         if (positiveSign)
            fs[o++] = '+';

         if (spacedSign && !positiveSign)
            fs[o++] = ' ';

         if (zeroPad)
            fs[o++] = '0';

         if (width)
            o += sprintf(fs+o, "%u", width);

         if (precision > -1)
            o += sprintf(fs+o, ".%i", precision);

         o += sprintf(fs+o, "%s", sizeMod);

         switch (fmt) {
            case NumericFormat::NF_DEC:
               fs[o++] = Floating ? ('f')
                                  : (Signed ? 'i' : 'u')
                       ;
               break;
            case NumericFormat::NF_SCI:
               fs[o++] = 'E';
               break;
            case NumericFormat::NF_HEX:
               fs[o++] = Floating ? ('A')
                                  : ('X')
                       ;    
               break;
            case NumericFormat::NF_BIN:
               break;
            case NumericFormat::NF_OCT:
               fs[o++] = 'o';
               break;
            case NumericFormat::NF_PTR:
               fs[o++] = 'p';
         }

         fs[o] = '\0';
         return fs;
      }
   }

}