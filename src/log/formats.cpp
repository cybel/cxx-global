#include "stdinc.h"

#include "head/log/levels.h"
#include "head/log/topics.h"
#include "head/log/scopes.h"
#include "head/log/message.h"
#include "head/log/formats.h"

namespace logging {
namespace format {
   using namespace ::config::logging::format;
   using boost::format;
   using logging::filter::toClassN;
   using logging::filter::toLevelN;
   using logging::filter::toClassS;
   using logging::filter::toLevelS;
   using logging::location::sourceNatureS;
   using logging::location::ctopicPtr;
   using logging::message::cmessageRef;
   using std::chrono::duration_cast;
   using std::chrono::microseconds;
   using std::chrono::system_clock;
   using std::ctime;
   using std::localtime;
   using std::put_time;
   using std::string;
   using std::stringstream;
   using std::time_t;

 
   /////////////////////////////////////////////////////////////////////
   ////// Formatter // (abstract base)
   //////////////////

   string Formatter::formatPart(FormatPart part, cmessageRef msg) const{
      stringstream o;

      switch (part) {
         case FormatPart::MCLASS:
            o << toClassS(msg.level);
         break;

         case FormatPart::MLEVEL:
            o << toLevelS(msg.level);
         break;

         case FormatPart::SOURCE:
            if (msg.origin.source)
               o << msg.origin.source->name;
            else
               o << "unknown-source";
         break;

         case FormatPart::SCOPE:
            if (msg.origin.path)
               o << msg.origin.path << ":" << sourceNatureS((uint8)msg.origin.nature);
            else
               o << "unknown-nature:unknown-path";
         break;

         case FormatPart::TOPIC: {
            uint8 i = FORMAT_TOPIC_CONTEXT;
            ctopicPtr p = msg.topic;
            if (!p) {
               o << "(unknown-topic)";
               break;
            }
            o << "(";
            while (p && i) {
               const string& s = *p;
               o << s;
               if ((p = p->parent) && --i)
                  o << " << ";
            }
            o << ")";
         } break;
         
         case FormatPart::TIME:
            o << duration_cast<microseconds>(msg.time - programStart).count();
         break;

         case FormatPart::WALLCLOCK: {
            time_t t = system_clock::to_time_t(programToReal(msg.time));
            o << put_time(localtime(&t), TIME_FORMAT);
         } break;

         case FormatPart::MESSAGE:
            o << "\t";
            for (msize i = 0; i < msg.lines; i++)
               o << msg.line[i] << (i+1 < msg.lines ? "\n\t" : "");
         break;

         case FormatPart::THREAD:
            o << "[" << std::hex << msg.threadId << " / " << std::hex << msg.cothread << "]";
         break;

         default:
            return "";
      }
      
      return o.str();
   }

   /////////////////////////////////////////////////////////////////////
   ////// Boost String Formatter //
   ///////////////////////////////

   boostFormatter_S::boostFormatter_S(const string& format) 
   : fmt(format)
   {}

}}