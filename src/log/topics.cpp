#include "stdinc.h"

#include "head/log/topics.h"

#include "head/util/temporal-attachment.h"

namespace logging {
namespace location {
   using namespace ::config::logging::location;

   using boost::shared_mutex;
   using boost::upgrade_lock;
   using boost::upgrade_to_unique_lock;
   using std::make_tuple;
   using std::map;
   using std::string;
   using std::tie;
   using std::tuple;
   using util::hash::murmur3_128;
   using util::relational::temporalAttachment;
   using util::relational::PolicyCoThreadId; 


   /////////////////////////////////////////////////////////////////////
   ////// Topic Collection State //
   ///////////////////////////////

   typedef tuple<uint64        , uint64> TopicAbsKey;    // id
   typedef tuple<const topic_S*, cstrc>  TopicRelKey;    // parent, name
   typedef tuple<rptr          , cstrc>  TopicHintKey;   // hint  , name

   constexpr_ TopicAbsKey nullkey = make_tuple(0LL, 0LL);

   typedef temporalAttachment<topicPtr, PolicyCoThreadId> TopicTemporalAttachment;

#  define collectionRead()  upgrade_lock<shared_mutex>           __readLock(state->stateAccess)
#  define collectionWrite() upgrade_to_unique_lock<shared_mutex> __writeLock(__readLock)

   struct topicCollectionState_S {
      shared_mutex stateAccess;

      map<TopicAbsKey,  topicPtr>    topicsById;
      map<TopicRelKey,  TopicAbsKey> topicIdsByParent;
      map<TopicHintKey, TopicAbsKey> topicIdsByHint;

      TopicTemporalAttachment        topicAttach;

      ~topicCollectionState_S() {
         for (auto topic: topicsById)
            delete topic.second;
      }

      alwaysinline
      tuple<bool, TopicAbsKey> findKnownPredecessor(cstr name) {
         // note: make sure this is thread safe for reads!!!
         ctopicPtr parent = topicAttach.current();

         if (parent) {
            auto i = topicIdsByParent.find(make_tuple(parent, name));
            if (i != topicIdsByParent.end()) 
               return make_tuple(true, i->second);  
         } 

         return make_tuple(false, nullkey);
      }

      alwaysinline
      tuple<bool, TopicAbsKey> findHint(cstr name, rptr hint) {
         auto i = topicIdsByHint.find(make_tuple(hint, name));
         if (i != topicIdsByHint.end())
            return make_tuple(true, i->second);
         return make_tuple(false, nullkey);
      }

      alwaysinline
      tuple<bool, TopicAbsKey> findRootTopic(cstr name) {
         auto i = topicIdsByParent.find(make_tuple(nullptr, name));
         if (i != topicIdsByParent.end()) 
               return make_tuple(true, i->second);  
         return make_tuple(false, nullkey);
      }

      alwaysinline
      tuple<bool, TopicAbsKey> findParent() {
         ctopicPtr parent = topicAttach.current();
         if (parent)
            return make_tuple(true, make_tuple(parent->id[0], parent->id[1]));
         else
            return make_tuple(false, nullkey);
      }

      alwaysinline
      topicPtr find(cstr name, rptr hint = nullptr) {
         assert(name);
         bool found = false;
         TopicAbsKey key;

         // try the hint
         if (hint)
            tie(found, key) = findHint(name, hint);

         // try temporal attachment
         if (!found)
            tie(found, key) = findKnownPredecessor(name);

         // try asserted parent
         if (!found)
            tie(found, key) = findRootTopic(name);

         if (found)
            return topicsById[key];
         else
            return nullptr;
      }

      alwaysinline
      topicPtr insert(cstr name, rptr hint = nullptr) {
         assert(name);
         TopicAbsKey myKey, parentKey;
         bool hasParent;
         topicPtr add, parent;

         tie(hasParent, parentKey) = findKnownPredecessor(name);
         
         // finally, just attach to parent
         if (!hasParent)
            tie(hasParent, parentKey) = findParent();

         parent = hasParent ? topicsById[parentKey] : nullptr;

         // ALLOCATION: deallocated by destructor
         add = new topic(name, parent);
         myKey = make_tuple(add->id[0], add->id[1]);

         topicIdsByParent[make_tuple(parent, name)] = myKey;

         if (hint)
            topicIdsByHint[make_tuple(hint, name)] = myKey;

         topicsById[myKey] = add;

         return add;
      }

      alwaysinline
      void begin(topicPtr what) {
         assert(what);
         topicAttach.create(what);
      }

      alwaysinline
      topicPtr conclude(topicPtr what) {
         assert(what);
         topicAttach.destroy(what);
         return what->parent;
      }

      alwaysinline
      topicPtr currently() {
         topicPtr now = topicAttach.current();

         return now;
      }

   };

   
   /////////////////////////////////////////////////////////////////////
   ////// Topic Collection //
   /////////////////////////

   topicCollection_S::topicCollection_S() {
      state = new topicCollectionState;
   }

   topicCollection_S::~topicCollection_S() {
      delete state;
   }

   topicPtr topicCollection_S::begin(cstr name, rptr hint) {
      topicPtr t = nullptr;
      collectionRead();
      t = state->find(name, hint);
      
      if (!t) {
         collectionWrite();
         t = state->insert(name, hint);
      }

      state->begin(t);
      return t;
   }

   topicPtr topicCollection_S::conclude(topicPtr topic) {
      collectionRead();
      return state->conclude(topic);
   }

   topicPtr topicCollection_S::currently() {
      topicPtr t = nullptr;
      collectionRead();
      t = state->currently();
      return t;
   }

   /////////////////////////////////////////////////////////////////////
   ////// Topic //
   //////////////

   topic_S::topic_S(cstr _name, topic_S* _parent) 
   : id {0,0}
   , name(_name)
   , parent(_parent)
   , muted(false)
   {
      murmur3_128( (rptr)_name
                 , strlen(_name)
                 , (rptr)&id
                 , parent 
                 ? (uint32)(parent->id[0] ^ parent->id[1])
                 : TOPIC_BASE
                 );
   }

   // same topic
   bool topic_S::operator==(const topic_S& other) const {
      return id[0] == other.id[0] 
          && id[1] == other.id[1]
           ;
   }

   bool topic_S::operator!=(const topic_S& other) const {
      return !(*this == other);
   }

   // hierarchy
   bool topic_S::operator>(const topic_S& other) const {
      const topic_S* walk = &other;
      while (walk && walk->parent != this)
         walk = walk->parent;
      return walk;
   }

   bool topic_S::operator<(const topic_S& other) const {
      return other > *this;
   }

   bool topic_S::silence(bool nowMute) {
      bool o = muted;
      muted = nowMute;
      return o;
   }

   // muted
   bool topic_S::mute() const {
      return muted || (parent ? parent->mute() : false);
   }

   topic_S::operator bool() const {
      return !mute();
   }

   topic_S::operator string() const {
      return (string) name;
   }

   topic_S::operator cstr() const {
      return name;
   }

}}