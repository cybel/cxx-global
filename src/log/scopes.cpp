#include "stdinc.h"

#include "head/log/scopes.h"

namespace logging {
namespace location {
   using boost::shared_mutex;
   using boost::unique_lock;
   using boost::upgrade_lock;
   using boost::upgrade_to_unique_lock;
   using std::atomic;
   using std::bind;
   using std::function;
   using std::make_tuple;
   using std::map;
   using std::string;
   using std::tuple;
   using namespace std::placeholders;


   /////////////////////////////////////////////////////////////////////
   ////// Source State // (opaque, internal)
   /////////////////////

   typedef tuple<SourceNature, cstrc> Scope;

   struct sourceState_S {
      map<SourceNature, set<cstr>> scopeLookup;
      set<Scope>                   scopeSet;
      shared_mutex                 stateAccess;

      static const set<cstr> EmptyNature;

      bool contains(SourceNature nature, cstr path) const {
         return scopeSet.find(make_tuple(nature, path)) != scopeSet.end();
      }

      bool contains(SourceNature nature) const {
         return scopeLookup.find(nature) != scopeLookup.end();
      }

      const set<cstr>& pathsFor(SourceNature nature) const {
         auto i = scopeLookup.find(nature);
         if (i == scopeLookup.end())
            return EmptyNature;
         return i->second;
      }

      bool enscope(SourceNature nature, cstr path) {
         if (!contains(nature, path)) {
            scopeLookup[nature].insert(path);
            scopeSet.insert(make_tuple(nature, path));
            return true;
         }
         return false;
      }
   };
   CompactTypes(sourceState,_S);

   const set<cstr> sourceState_S::EmptyNature;

#  define collectionRead()  upgrade_lock<shared_mutex>           __readLock(state->stateAccess)
#  define collectionWrite() upgrade_to_unique_lock<shared_mutex> __writeLock(__readLock)

   /////////////////////////////////////////////////////////////////////
   ////// Source  //
   ////////////////

   namespace {
      // use a counter instead of hashing since sources might have same
      // names and their specification (scopes) may grow.
      atomic<uint64> sourceIdCounter;
   }

   // ALLOCATION: strdup name. Destroyed with source (program exit
   // typically for per-file Sources). new sourceState, same.
   source_S::source_S(cstr _name, const EnscopeInitFunc init) 
   : id(++sourceIdCounter)
   , name(strdup(_name))
   , muted(false)
   {
      state = new sourceState;

      init(bind( &source::enscope, this
               , _1, _2
               ));
   }

   // DEALLOCATION: free name, delete sourceState.
   source_S::~source_S() {
      //unique_lock<shared_mutex> __deleteLock(stateAccess);
      if (name)
         free((void*)name);
      delete state;
   }   

   bool source_S::contains(SourceNature nature) const {
      collectionRead();
      return state->contains(nature);
   }

   bool source_S::contains(SourceNature nature, cstr path) const {
      collectionRead();
      return state->contains(nature, path);
   }

   const set<cstr>& source_S::pathsFor(SourceNature nature) const {
      collectionRead();
      return state->pathsFor(nature);
   }

   bool source_S::enscope(SourceNature nature, cstr path) {
      collectionRead();
      if (!state->contains(nature, path)) {
         collectionWrite();
         return state->enscope(nature, path);
      }
      return false;
   }

   // mute
   bool source_S::silence(bool nowMuted) {
      bool o = muted;
      muted = nowMuted;
      return o;
   } 

   // muted
   bool source_S::mute() const {
      return muted;
   }

   source_S::operator bool() const {
      return !mute();
   }

   // name
   source_S::operator string() const {
      return (string)name;
   }

   source_S::operator cstr() const {
      return name;
   }

}}