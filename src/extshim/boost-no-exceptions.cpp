#include "stdinc.h"

#include <boost/throw_exception.hpp>

using namespace ::config::os;

namespace boost {
#  ifdef BOOST_NO_EXCEPTIONS
      void throw_exception( std::exception const & e );
      void throw_exception( std::exception const & e ) {
         exit(EXIT_LIB_BOOST_EXCEPTION);
      }
#  else
      template <class E>
      void throw_exception( E const & e );
#  endif
}