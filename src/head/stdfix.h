#ifndef __STDFIX_H__
#define __STDFIX_H__

#include "head/platform.h"

#include <type_traits>

#if (E_DEBUG & DEBUG_INCONSEQUENTIAL) && (E_COMPILER == COMP_CLANG)
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wc++14-extensions"
#  pragma clang diagnostic ignored "-Wc++1z-extensions"
#endif

/*
 * global/src/head/stdfix.h:67:29: warning: variable templates are a C++14 extension [-Wc++14-extensions]
 *       inline constexpr bool is_invocable_v = std::is_invocable<Fn, ArgTypes...>::value;      
 *                             ^
 * global/src/head/stdfix.h:67:7: warning: inline variables are a C++1z extension [-Wc++1z-extensions]
 *       inline constexpr bool is_invocable_v = std::is_invocable<Fn, ArgTypes...>::value;      
 *       ^
 * global/src/head/stdfix.h:70:29: warning: variable templates are a C++14 extension [-Wc++14-extensions]
 *       inline constexpr bool is_invocable_r_v = std::is_invocable_r<R, Fn, ArgTypes...>::value;
 *                             ^
 * global/src/head/stdfix.h:70:7: warning: inline variables are a C++1z extension [-Wc++1z-extensions]
 *       inline constexpr bool is_invocable_r_v = std::is_invocable_r<R, Fn, ArgTypes...>::value;
 *       ^
 * global/src/head/stdfix.h:73:29: warning: variable templates are a C++14 extension [-Wc++14-extensions]
 *       inline constexpr bool is_nothrow_invocable_v = std::is_nothrow_invocable<Fn, ArgTypes...>::value;
 *                             ^
 * global/src/head/stdfix.h:73:7: warning: inline variables are a C++1z extension [-Wc++1z-extensions]
 *       inline constexpr bool is_nothrow_invocable_v = std::is_nothrow_invocable<Fn, ArgTypes...>::value;
 *       ^
 * global/src/head/stdfix.h:76:23: warning: variable templates are a C++14 extension [-Wc++14-extensions]
 * inline constexpr bool is_nothrow_invocable_r_v = std::is_nothrow_invocable_r<R, Fn, ArgTypes...>::value;
 *                       ^
 * global/src/head/stdfix.h:76:1: warning: inline variables are a C++1z extension [-Wc++1z-extensions]
 * inline constexpr bool is_nothrow_invocable_r_v = std::is_nothrow_invocable_r<R, Fn, ArgTypes...>::value;
 */

////////////////////////////////////////////////////////////////////////
////// < C++14 //
////////////////

#if ((E_COMPILER==COMP_GCC) || (E_COMPILER==COMP_CLANG)) && (E_LANGUAGE < LANG_14)
   namespace std {
      template <bool cond, typename a, typename b>
      struct conditional;

      template <bool cond, typename a>
      struct enable_if;

      template <bool cond, typename a, typename b = void>
      using conditional_t = typename conditional<cond,a,b>::type;

      template <bool cond, typename a = void>
      using enable_if_t = typename enable_if<cond,a>::type;
   }
#endif

////////////////////////////////////////////////////////////////////////
////// < C++17 //
////////////////

#if (E_LANGUAGE < LANG_17)
   namespace std {

#  if ((E_COMPILER==COMP_GCC) || (E_COMPILER==COMP_CLANG))
      template <typename T>
      using is_integral_v = typename is_integral<T>::value;
#  endif 

      // use the modern name (since C++17)

      template <class Fn, class... ArgTypes>
      struct is_callable;

      template <class R, class Fn, class... ArgTypes>
      struct is_callable_r;

      template <class Fn, class... ArgTypes>
      struct is_nothrow_callable;

      template <class R, class Fn, class... ArgTypes>
      struct is_nothrow_callable_r;      

      template <class Fn, class... ArgTypes>
      using is_invocable = is_callable<Fn, ArgTypes...>;

      template <class R, class Fn, class... ArgTypes>
      using is_invocable_r = is_callable_r<R, Fn, ArgTypes...>;

      template <class Fn, class... ArgTypes>
      using is_nothrow_invocable = is_nothrow_callable<Fn, ArgTypes...>;

      template <class R, class Fn, class... ArgTypes>
      using is_nothrow_invocable_r = is_nothrow_callable_r<R, Fn, ArgTypes...>;

      // helpers (since C++17)

      template <class Fn, class... ArgTypes>
      inline constexpr bool is_invocable_v = is_invocable<Fn, ArgTypes...>::value;      

      template <class R, class Fn, class... ArgTypes>
      inline constexpr bool is_invocable_r_v = is_invocable_r<R, Fn, ArgTypes...>::value;

      template <class Fn, class... ArgTypes>
      inline constexpr bool is_nothrow_invocable_v = is_nothrow_invocable<Fn, ArgTypes...>::value;

      template <class R, class Fn, class... ArgTypes>
      inline constexpr bool is_nothrow_invocable_r_v = is_nothrow_invocable_r<R, Fn, ArgTypes...>::value;

#     if (E_PLATFORM == PLAT_X64) || (E_PLATFORM == PLAT_ARM64)
         constexpr size_t hardware_destructive_interference_size  = sizeof(uint64_t);
         constexpr size_t hardware_constructive_interference_size = sizeof(uint64_t);
#     elif (E_PLATFORM == PLAT_X32) || (E_PLATFORM == PLAT_ARM32)
         constexpr size_t hardware_destructive_interference_size  = sizeof(uint32_t);
         constexpr size_t hardware_constructive_interference_size = sizeof(uint32_t);
#     endif
   }
#endif

#if E_COMPILER == COMP_CLANG
#  pragma clang diagnostic pop
#endif

#endif
