#ifndef __MEMORY_UTIL_H__
#define __MEMORY_UTIL_H__

template <typename T>
alwaysinline msize zeroMemory(T* m) {
   memset((void*)m, 0, sizeof(T));
   return sizeof(T);
}

template <typename T>
alwaysinline msize zeroMemory(T& m) {
   return zeroMemory(&m);   
}
#endif

