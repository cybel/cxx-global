#ifndef __EXTLIB_GOOGLE_V8_H__
#define __EXTLIB_GOOGLE_V8_H__

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER==COMP_MSVC
#     pragma warning(push)
#     pragma warning(disable:4100;disable:4127;disable:4191;disable:4193;disable:4365;disable:4512;disable:4623;disable:4625;disable:4626;disable:4668;disable:4946)
#  endif
#endif

#include <v8.h>

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER==COMP_MSVC
#     pragma warning(pop)
#  endif
#endif


#endif

