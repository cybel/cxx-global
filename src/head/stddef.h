#ifndef __STDDEF_H__
#define __STDDEF_H__

#include "head/platform.h"

#define DEBUG_CONSOLEOUT       (1ull<<63)
#define DEBUG_NO_SUPPRESS_WARN (1ull<<60)
#define DEBUG_INCONSEQUENTIAL  (1ull<<59) // mute inconsequential warnings
#define DEBUG_GUARDS           (1ull<<58)
#define DEBUG_XSYNCHRONIZE     (1ull<<59)


#define DEBUG_ALL (0xFFFFFFFFFFFFFFFFll)

// XXX: This is temporary. 
#define E_DEBUG (DEBUG_ALL & (~DEBUG_NO_SUPPRESS_WARN))
// XXX: Remove this and put it in the makefile!!!

#if E_DEBUG & DEBUG_GUARDS
#  define DEBUG_GUARD_STRING "<GUARD-GUARD-GUARD-GUARD-GUARD>"
#  define DEBUG_GUARD(x) chr __guard##x [32] = DEBUG_GUARD_STRING
#  define DEBUG_GUARD_ASSERT(x,n) assert(!strcmp((x).__guard##n, DEBUG_GUARD_STRING))
#else
#  define DEBUG_GUARD(x)
#  define DEBUG_GUARD_ASSERT(x,n)
#endif
 
///

#define UNUSED(x) (void)x

#if (E_COMPILER==COMP_MSVC) && (E_COMPILER_VER < 19)
#  define constexpr_
#  define static_assert_constexpr(...)
#  define alwaysinline inline __forceinline
#  define noinline     __declspec(noinline)
#elif (E_COMPILER==COMP_MSVC)
#  define constexpr_ constexpr
#  define static_assert_constexpr(...) static_assert(__VA_ARGS__)
#  define alwaysinline inline __forceinline
#  define neverinline  __declspec(noinline)
#elif (E_COMPILER==COMP_GCC) || (E_COMPILER==COMP_CLANG)
#  define constexpr_ constexpr
#  define static_assert_constexpr(...) static_assert(__VA_ARGS__)
#  define alwaysinline inline __attribute__((always_inline))
#  define noinline     __attribute__((noinline)) 
#else
#  define constexpr_ constexpr
#  define static_assert_constexpr(...) static_assert(__VA_ARGS__)
#  define alwaysinline
#  define noinline
#endif

#define interface =0

// http://stackoverflow.com/questions/2991927/how-to-force-inclusion-of-an-object-file-in-a-static-library-when-linking-into-e
#if (E_COMPILER == COMP_MSVC)
#  if (E_OS == OS_WINDOWS)
#     if (E_PLATFORM == PLAT_X64)
#        define FORCE_UNDEFINED_SYMBOL(x) __pragma(comment (linker, "/export:" #x))
#     elif (E_PLATFORM == PLAT_X32)
#        define FORCE_UNDEFINED_SYMBOL(x) __pragma(comment (linker, "/export:_" #x))
#     endif
#     define FORCE_UNDEFINED_SYMBOL_HARD(x) \
         extern "C" void x(void); \
         void (*__ ## x ## _fp)(void)=&x; \
         FORCE_UNDEFINED_SYMBOL(x)
#  endif
#elif (E_COMPILER == COMP_CLANG || E_COMPILER == COMP_GCC)
#  define FORCE_UNDEFINED_SYMBOL(x)
#  define FORCE_UNDEFINED_SYMBOL_HARD(x) \
         extern "C" void x(void); \
         void (*__ ## x ## _fp)(void)=&x
#else
#  warning "No FORCE_UNDEFINED_SYMBOL / FORCE_UNDEFINED_SYMBOL_HARD available for this compiler"
#endif

#if ((E_OS==OS_WIN32) || (E_OS==OS_WIN64)) && (E_COMPILER==COMP_MSVC)
#   define CONVENTION_STDCALL  __stdcall          // Callee cleans up
#   define CONVENTION_CDECL    __cdecl            // Caller cleans up
#   define CONVENTION_THISCALL __thiscall         
#   define CONVENTION_FASTCALL __fastcall
#   define CONVENTION_VECTOR   __vectorcall
#   define CONVENTION_DEFAULT
#   define CONVENTION_BASIC    CONVENTION_STDCALL // APIENTRY / WINAPI.
#elif (E_PLATFORM!=PLAT_X64) && ((E_COMPILER==COMP_GCC) || (E_COMPILER==COMP_CLANG))
#   define CONVENTION_STDCALL  __attribute__((stdcall))
#   define CONVENTION_CDECL    __attribute__((cdecl))
#   define CONVENTION_THISCALL __attribute__((thiscall))
#   define CONVENTION_FASTCALL __attribute__((fastcall))
#   define CONVENTION_VECTOR   
// TODO: detect BASIC based on OS
#   define CONVENTION_DEFAULT
#   define CONVENTION_BASIC    CONVENTION_STDCALL
#elif (E_PLATFORM==PLAT_X64) && ((E_COMPILER==COMP_GCC) || (E_COMPILER==COMP_CLANG))
#   define CONVENTION_STDCALL  __attribute__((stdcall))
#   define CONVENTION_CDECL    __attribute__((cdecl))
#   define CONVENTION_THISCALL __attribute__((thiscall))
#   define CONVENTION_FASTCALL __attribute__((fastcall))
#   define CONVENTION_VECTOR
#   define CONVENTION_DEFAULT
#   define CONVENTION_BASIC    CONVENTION_STDCALL
#else
#   define CONVENTION_STDCALL 
#   define CONVENTION_CDECL   
#   define CONVENTION_THISCALL
#   define CONVENTION_FASTCALL
#   define CONVENTION_VECTOR   
#   define CONVENTION_DEFAULT
#   define CONVENTION_BASIC   
#   if (E_PLATFORM==PLAT_X64)
#      error "Conventions are not known for this OS/Compiler!"
#   endif
#endif

#ifdef _
#  undef _
#endif

#if E_COMPILER == COMP_MSVC
#  pragma warning(push)
#  pragma warning(disable:4668) // eval to 0
#endif

#if defined(E_DEBUG) && E_DEBUG != 0
#  if defined(DEBUG)
#     if !DEBUG
#        ifdef DEBUG
#           undef DEBUG
#        endif
#        define DEBUG (1)
#     endif
#  endif
#  if defined(_DEBUG)
#     if !_DEBUG
#        ifdef _DEBUG
#           undef _DEBUG
#        endif
#        define _DEBUG (1)
#     endif
#  endif
#endif

#if E_COMPILER == COMP_MSVC
#  pragma warning(pop)
#endif

#if defined(UNICODE) || defined(_UNICODE)
#  if defined(UNICODE_WIDE)
#     define _(x) L##x
#  elif defined(UNICODE_UTF16)
#     define _(x) u16##x
#  else
#     define _(x) u8##x
#  endif
#else
#  define _(x) x
#endif

#if !defined(sgn)
#  define sgn(x) ((x<0)?(-1):((x>0)?(1):(0)))
#endif

#define CPP_EXPAND(...) __VA_ARGS__
#define CPP_STRING(x)   #x
#define CPP_JOIN(a,b)   a##b

#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1)
#define VA_NUM_ARGS_IMPL(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,N,...) N
#define VA_DISPATCH(func, ...) \
   VA_DISPATCH_(func, VA_NUM_ARGS(__VA_ARGS__))
#define VA_DISPATCH_(func, nargs) \
   VA_DISPATCH__(func, nargs)
#define VA_DISPATCH__(func, nargs) \
   func ## nargs
#define VA_EXPAND(...) \
   __VA_ARGS__



#endif

