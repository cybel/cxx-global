#ifndef __UTIL_RESOURCE_TRANSACTION_H__
#define __UTIL_RESOURCE_TRANSACTION_H__

#include <stack>
#include <vector>
#include <functional>

namespace util        {
namespace transaction {
   using namespace ::config::util::res;

   using std::function;
   using std::initializer_list;
   using std::stack;
   using std::vector;
   using std::pair;

   
   /* Transactions are used in order to have a reversible set of steps 
    * taken, which might be modified up until they're "committed". At
    * this point, the list may be "inverted" (optional), and 
    * subsequently rolled back. Transactions may not be modified after
    * commit/inversion to ensure continuity is strictly ensured. When an
    * actively stepped-into transaction is modified, it may be rolled 
    * back to where the modification takes place, requiring the user to
    * step it forward again. The final entry pre-inversion is *always*
    * a commit step added by the transaction itself, locking it. This
    * step is NOT optional, although the user could intentionally step
    * short of it for whatever awful reason they choose. 
    */
   struct simpleLinear {
      // count type
      typedef TransactionStepN StepN;

      // TransactionStep(step/undo, step) -> success
      typedef function<bool(bool, StepN)> TransactionStep;

      typedef pair<TransactionStep, TransactionStep> TransactionPair;

      // error handler
      // TransactionFail(stepOrUndo, step, offendingStep, offendingUndo) -> recovered
      typedef function<bool(bool, StepN, TransactionStep, TransactionStep)> TransactionFail;

    
      simpleLinear() = delete;

      // invertCommit  -> invert when completed (prevent auto rollback)
      // form == true  -> {{do, undo}, {do, undo}, ...}
      // form == false -> {do+undo, do+undo, do+undo, ...}
      simpleLinear( bool invertCommit = false
                  , TransactionFail stepFail = FailSilent
                  );
      simpleLinear( initializer_list<TransactionStep>
                  , bool invertCommit = false
                  , TransactionFail stepFail = FailSilent
                  , bool form = true
                  );

      ~simpleLinear();

      // transform list (index past end goes just prior to commit)
      // deletion must be valid, otherwise silently ignored.
      bool addStep(TransactionStep perform);
      bool addStep(TransactionStep perform, TransactionStep undo);
      bool addStep(StepN where, TransactionStep perform);
      bool addStep(StepN where, TransactionStep perform, TransactionStep undo);
      bool delStep(StepN where);

      // total steps 
      StepN steps() const;

      // currently at step
      StepN atStep() const;

      // step forward -> incomplete
      StepN step(StepN count = 1, TransactionFail fail = nullptr);
      StepN until(StepN until = TRANSACTION_STEP_N_MAX, TransactionFail fail = nullptr);

      // step back -> incomplete
      StepN undo(StepN count = 1, TransactionFail fail = nullptr);
      StepN unto(StepN until = 0, TransactionFail fail = nullptr);

      // perform a transaction or roll back (returns COMMIT)
      bool transact(TransactionFail fail = FailSilent);
      bool rollback(TransactionFail fail = FailSilent);

      bool completed() const;

      // get a transaction of undos (useful for resource release at a
      // later time).
      bool invert();
      bool inverted() const;

      // get an inverted child
      void invertTo(simpleLinear& child) const;

   private:
      const bool convert;
      bool isInverted;
      bool isComplete;

      int curStep = 0;

      vector<TransactionPair> transactionList;
      TransactionFail         stepFail;

      static const TransactionFail FailSilent;
      static const TransactionStep EmptyStep;

      void appendCommit();
      bool rewindModified(StepN to, TransactionFail fail);
   };


}}

#endif

