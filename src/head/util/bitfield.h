#ifndef __UTIL_BITFIELD_H__
#define __UTIL_BITFIELD_H__

#if E_COMPILER == COMP_CLANG
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wsign-compare"
#endif

/* Lock-Free Bitfield
 *    - Tailored to tracking tasks. Each field of N units is atomic, so
 *      all operations are pass/fail depending on intermittent change,
 *      and will resolve "eventually". 
 *
 *    - Interface is pretty simple:
 *
 *          field[N]    - state of bit N
 *          field--     - find unset bit starting at end
 *          --field     - find unset bit starting at beginning
 *          field == N  - bit status is true
 *          field != N  - bit status is false
 *          field >> N  - Draw (set) bit
 *          field << N  - Replace (reset) bit
 */

namespace util {
namespace bitfield {
   using std::atomic;
   using std::atomic_flag;
   using std::conditional_t;
   using std::enable_if;
   using std::hardware_destructive_interference_size;
   using std::integral_constant;
   using std::is_integral;
   using std::make_tuple;
   using std::numeric_limits;
   using std::tie;
   using std::tuple;

   template <unsigned Bits>
   using UnitType 
      = conditional_t< Bits <=  8, uint8
      , conditional_t< Bits <= 16, uint16
      , conditional_t< Bits <= 32, uint32
      , uint64
      >>>;

   template <unsigned Bits, template<unsigned B> class UnitT = UnitType>
   using UnitWidth = integral_constant<msize, sizeof(UnitT<Bits>)*8>; 

   template <unsigned Bits, template<unsigned B> class UnitT = UnitType>
   using FieldSize = integral_constant< msize
                                      , Bits 
                                      / UnitWidth<Bits, UnitT>::value 
                                      + ( Bits % UnitWidth<Bits, UnitT>::value
                                        ? 1
                                        : 0
                                        )>;

   template <unsigned Bits, template<unsigned B> class UnitT = UnitType>
   struct FieldEntry {
      alignas(hardware_destructive_interference_size)
      atomic<UnitT<Bits>> value;
   };

   template <unsigned Bits, template<unsigned B> class UnitT = UnitType>
   using FieldType = FieldEntry<Bits, UnitT>[FieldSize<Bits, UnitT>::value];

   template <unsigned Bits>
   struct bitfield {
      typedef bitfield<Bits>  SelfT;
      typedef msize           IndexT;
      typedef UnitType<Bits>  UnitT;
      typedef FieldType<Bits> FieldT;

      // max value for midpoint 
      static const msize  UnitMax    = numeric_limits<UnitT>::max();
      // max value for end
      static const UnitT  UnitLast   = UnitMax >> ( (sizeof(UnitT)*8) - Bits );

      // total bits covered ("end")
      static const IndexT BitTotal   = Bits;
      // invalid value
      static const IndexT BitInvalid = numeric_limits<IndexT>::max();

      static const msize  FieldLen   = FieldSize<Bits, UnitType>::value;

      static const msize  UnitLen    = UnitWidth<Bits, UnitType>::value;

      bitfield() {
         for (auto i = FieldLen; i; i--)
            field[i-1].value = i == FieldLen
                             ? UnitLast
                             : UnitMax
                             ;
      }

      // bit status
      alwaysinline 
      bool operator[](const int &x) const {
         msize index;
         msize elem;

         tie(index, elem) = idx(x);

         return !((field[index].value >> elem) & 1);
      }

      // bit not set
      alwaysinline 
      bool operator!=(const int &x) const {
         msize index;
         msize elem;

         tie(index, elem) = idx(x);

         return ((field[index].value >> elem) & 1);
      }

      // bit set
      alwaysinline 
      bool operator==(const int &x) const {
         msize index;
         msize elem;

         tie(index, elem) = idx(x);

         return !((field[index].value >> elem) & 1);
      }

      alwaysinline 
      bool empty() const {
         for (auto i = FieldLen; i; i--)
            // any true (0) become false (1)
            if (  field[i-1].value
               != ( i == FieldLen
                    ? UnitLast
                    : UnitMax
                  )
               )
               return false;
         return true;
      }

      alwaysinline 
      bool full() const {
         for (auto i = FieldLen; i; i--)
            // any true (0) become false (1)
            if (!field[i-1].value)
               return false;
         return true;
      }

      // bitfield empty/full
      // while (!field) { get-another }
      alwaysinline 
      bool operator!() const {
         return !full();
      }

      // has contents
      // while (field) { check for something }
      alwaysinline 
      operator bool() const {
         return !empty();
      }

      /******************/
      /* *** Modify *** */
      /******************/

      // withdraw bit
      // if (x >> n) // did i get my bit?
      template < typename T
               , typename = typename enable_if<is_integral<T>::value>::type
               >
      alwaysinline 
      bool draw(const T& x) {
         UnitT expect;
         UnitT unit;
         msize index;
         msize elem;

         tie(index, elem) = idx(x);

         do {
            expect = field[index].value;
            unit   = expect;
            unit  &= ( index == (FieldLen - 1)
                     ? UnitLast
                     : UnitMax
                     ) 
                   & ~(1 << elem)
                   ;
            // taken
            if (unit == expect)
               return false;

            //printf("%016llX %016llX % 16llX\n"
            //       "                -                 % 16llX\n"
            //       , unit, expect, 1<<elem, unit ^ expect
            //      );

            assert((unit ^ expect) == (1 << elem)); // knock out all ones
         } while (!atomic_compare_exchange_weak(&field[index].value, &expect, unit));

         return true;
      }

      // withdraw bit
      // if (x >> n) // did i get my bit?
      template < typename T
               , typename = typename enable_if<is_integral<T>::value>::type
               >
      alwaysinline 
      bool replace(const T& x) {
         UnitT expect;
         UnitT unit;
         msize index;
         msize elem;

         tie(index, elem) = idx(x);

         do {
            expect = field[index].value;
            unit   = expect;
            unit  |= (1 << elem);
            assert(unit != expect);
            if (unit == expect)
               return false;
            assert((unit ^ expect) == (1 << elem)); // knock out all ones
         } while (!atomic_compare_exchange_weak(&field[index].value, &expect, unit));

         return true;
      }

      // withdraw (beginning)
      alwaysinline 
      IndexT operator--() {
         for (auto i = 0; i < FieldLen; i++) {
            UnitT v = field[i].value;
            auto c = lowBit(v);
            if (c) {
               auto j = highBitPos(c);
               auto k = idx(i,j);
               if (!draw(k)) {
                  i--;
                  continue;
               }
               return k;
            }
         }
         return BitInvalid;
      }

      // withdraw (end)
      alwaysinline 
      IndexT operator--(int) {
         for (auto i = FieldLen; i; i--) {
            UnitT v = field[i-1].value;
            auto c = lowBit(v);
            if (c) {
               auto j = highBitPos(c);
               auto k = idx(i-1,j);
               if (!draw(k)) {
                  i++;
                  continue;
               }
               return k;
            }
         }
         return BitInvalid;
      }

      // withdraw bit
      // if (x >> n) // did i get my bit?
      template < typename T
               , typename = typename enable_if<is_integral<T>::value>::type
               >
      alwaysinline 
      bool operator>>(const T& x) {
         return draw(x);
      }

      // withdraw bit
      // if (x >> n) // did i get my bit?
      template < typename T
               , typename = typename enable_if<is_integral<T>::value>::type
               >
      alwaysinline 
      bool operator<<(const T& x) {
         return replace(x);
      }


   private:
      // note: field is inverted. helps searching!
      FieldT field;

      /*****************/
      /* *** Index *** */
      /*****************/

      template < typename T
               , typename = typename enable_if<is_integral<T>::value>::type
               >
      alwaysinline static
      tuple<msize,msize> idx(const T& x) {
         assert(x < Bits);

         msize index = x / UnitLen;
         msize elem  = x % UnitLen;

         assert(index < FieldLen);
         assert(elem  < UnitLen );

         return make_tuple(index, elem);
      }

      template < typename T
               , typename U
               , typename = typename enable_if<is_integral<T>::value>::type
               , typename = typename enable_if<is_integral<U>::value>::type
               >
      alwaysinline static
      msize idx(const T& x, const U& y) {
         return x * UnitLen + y;
      }

      /******************/
      /* *** Modify *** */
      /******************/

     

   };

}}

#if E_COMPILER == COMP_CLANG
#  pragma clang diagnostic pop
#endif

#endif

