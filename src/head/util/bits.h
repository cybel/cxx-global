#ifndef __UTIL_BITS_H__
#define __UTIL_BITS_H__

#include "head/platform.h"
#include "head/stdtyp.h"
#include "head/stddef.h"

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(push)
#     pragma warning(disable:4127) // static conditional
#  endif
#endif

namespace util {
   template <uint64 x>
   struct LowBit {
      static const uint64 value = (x & (-(sint64)x));
   };

   template <uint64 x, uint8 p = 0>
   struct LowBitPosImpl {
      static const uint64 value = LowBitPosImpl<(LowBit<x>::value >> 1), p + 1>::value;
   };

   template <uint64 p>
   struct LowBitPosImpl < 0, p > {
      static const uint64 value = p-1;
   };

   template <uint64 x>
   using LowBitPos = LowBitPosImpl<x>;

   template <uint64 x, uint8 p = 0>
   struct HighBitPos {
      static const uint64 value = HighBitPos<(x >> 1), p + 1>::value;
   };

   template <uint64 p>
   struct HighBitPos < 0, p > {
      static const uint64 value = p - 1;
   };

   template <uint64 p>
   struct HighBit {
      static const uint64 value =  1 << HighBitPos<p>::value;
   };

   template <typename T>
   alwaysinline
   T lowBit(T x) {
      return x & -x;
   }

   template<typename T>
   alwaysinline
   T lowBitPos(T x) {
      T l = lowBit(x);
      return (T) (log(l) / log(2));
   }

   // https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn
   template <typename T>
   T highBitPos32(T v) {
      int r;      // result goes here

      constexpr_ int MultiplyDeBruijnBitPosition[32] = 
      {
        0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
        8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
      };

      v |= v >> 1; // first round down to one less than a power of 2 
      v |= v >> 2;
      v |= v >> 4;
      v |= v >> 8;
      v |= v >> 16;

      r = MultiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27];

      return r;
   }

#  define G(y, z) if (S >= y) x |= (x >> z)
   template <typename T, size_t S = sizeof(T)*8> 
   alwaysinline 
   T saturateBits(T x) {
      G( 8,  1);
      G( 8,  2);
      G( 8,  4);
      G(16,  8);
      G(32, 16);
      G(64, 32);
      return x;
   }
#  undef G

   template <typename T, size_t S = sizeof(T)*8>
   alwaysinline 
   T highBit(T x) {
      x = saturateBits(x);
      //x &= ((x+1)>>1);
      x ^= x >> 1;
      return x;
   }

   template <typename T, size_t S = sizeof(T)*8>
   alwaysinline 
   T highBitPos(T x) {
      // O(ln(S))
      x = highBit(x);
      unsigned h = 0,g = 0;
      switch (S) {
      case 64: // 2 ops
         for (unsigned i = 0; i < S; i+=32)
            if (x & (0xFFFFFFFFull << i))
               h = i;
      case 32: // 2 ops
         for (unsigned i = h; i < h + 32; i+=16)
            if (x & (0xFFFFull << i))
               g = i;
      case 16: // 2 ops
         for (unsigned i = g; i < g + 16; i+=8)
            if (x & (0xFFull << i))
               h = i;
      case 8:  // 8 ops
         for (unsigned i = h; i < h + 8; i++)
            if (x & (1ull << i))
               g = i;
      default:
         break;
      }
      return g;
   }
}

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(pop)
#  endif
#endif

#endif 
