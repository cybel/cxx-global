namespace util {
namespace env  {
   using namespace ::config::util::env;
   using ::config::util::env::AppParseSpecSource;
   using std::string;
   using std::set;
   using std::map;
   using std::vector;
   

   // basic packed arguments reader
   // requires access to a global variable named "AppParseSpecSource"
   struct AppParseSpec
   : public EnvParseSpec {
      virtual set<string> candidates(uint8 type) const {
         set<string> out; 
         for (int i = 0; strcmp(AppParseSpecSource[i],"");i+=ENV_PACKED_WIDTH)
            if (strlen(AppParseSpecSource[i+type]))
               out.emplace(AppParseSpecSource[i+type]);
         return out;
      }

      virtual string longForm(string from) const {
         if (from.length() > 1)
            return from;
         for (int i = 0; strcmp(AppParseSpecSource[i],"");i+=ENV_PACKED_WIDTH)
            if (!strcmp(AppParseSpecSource[i+ENV_SWITCH_SHORT], from.c_str()))
               return AppParseSpecSource[i+ENV_SWITCH_LONG];
         return "";
      }

      virtual string shortForm(string from) const {
         if (from.length() == 1)
            return from;
         for (int i = 0; strcmp(AppParseSpecSource[i],"");i+=ENV_PACKED_WIDTH)
            if (!strcmp(AppParseSpecSource[i+ENV_SWITCH_LONG], from.c_str()))
               return AppParseSpecSource[i+ENV_SWITCH_SHORT];
         return "";
      }

      virtual string description(uint8 type, string from) const {
         for (int i = 0; strcmp(AppParseSpecSource[i],"");i+=ENV_PACKED_WIDTH)
            if (!strcmp(AppParseSpecSource[i+type], from.c_str())) {
               return AppParseSpecSource[i+ENV_SWITCH_DESCRIPTION];
            }
         return "Unknown";
      }

      virtual bool option(uint8 type, string from) const {
         for (int i = 0; strcmp(AppParseSpecSource[i],"");i+=ENV_PACKED_WIDTH)
            if (!strcmp(AppParseSpecSource[i+type], from.c_str()))
               return AppParseSpecSource[i+ENV_SWITCH_TYPE][0] == 'o';
         return false;
      }

      virtual bool accept(uint8 type, string candidate) const {
         set<string> s = candidates(type);
         return s.find(candidate) != s.end();
      }

      virtual bool reject(uint8 type, string candidate) const {
         set<string> s = candidates(type);
         return s.find(candidate) == s.end();
      }

   };

}}