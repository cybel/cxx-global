#ifndef __UTIL_FASTPOOL_H__
#define __UTIL_FASTPOOL_H__

#include "head/util/bitfield.h"

#define ALIGN_UNIQUE //alignas(hardware_destructive_interference_size)
#define ALIGN_SHARED ALIGN_UNIQUE uint8

//
//      /¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
//      v                                           v
//    Bank 0 <-> Bank 1 <-> Bank 2 <-> Bank 3 <-> Bank 4
//    (head)                  |
//                            |
//                            |
//     (Slow allocation)      |               (Locking)
//                            |
// - - - - - - - - - - - - - -|- - - - - - - - - - - - - - - -
//                            |
//     (Fase allocation)  [Bitfield]         (Lock-free)
//                         /  |  \
//                        /   |   \
//                  Message   |   Message
//                         Message
//
//    Banks can be grown without locking searches out since it will not
// create invalid links. Conversely, shrinking does require locking 
// since it can cause invalid links. 
// 

namespace util {
namespace fastpoolBits {
   using boost::mutex;
   using boost::shared_mutex;
   using boost::shared_lock;
   using boost::unique_lock;
   using util::bitfield::bitfield;
   using std::atomic;

   template <typename T, uint64 BankSize>
   struct bank_S;
   CompactTemplates(bank,_S, typename, T, uint64, BankSize);

   template <typename T, uint64 BankSize>
   struct bankItem {
      T value;       // must remain first, reinterpret_cast to bankItem

      bankPtr<T,BankSize> bank;
      msize bankItem;
   };

   template <typename T, uint64 BankSize>
   struct bank_S {
      typedef bitfield<BankSize>      CollectionBitfield;

      ALIGN_UNIQUE bitfield<BankSize>     occupancy;
      ALIGN_UNIQUE bankItem<T, BankSize>  item[BankSize];

      ALIGN_SHARED;
      atomic<bank_S*> next;
      atomic<bank_S*> prev;
   };
   CompactTemplates(bank,_S, typename, T, uint64, BankSize);

}}

#  define SEARCHING() shared_lock<shared_mutex> __sch_lock (shrinkLock)

#  define SHRINKING() unique_lock<shared_mutex> __shr_lock (shrinkLock)

#  define GROWING()   unique_lock<shared_mutex> __gro_lock (resizeLock); \
                      shared_lock<shared_mutex> __shr_lock (shrinkLock)

namespace util {
   using namespace util::fastpoolBits;
   using util::bitfield::bitfield;
   using math::sclamp;

   template < typename T
            , uint64 BankSize = 64
            , sint16 BIAS_MIN = -5
            , sint16 BIAS_MAX = 5
            , sint16 FLOW_SHRINK = -16
            , uint64 BankSizeTrue = (util::HighBit<BankSize>::value)
            >
   struct fastPool {
   protected:
      typedef bank<T, BankSizeTrue>       Bank;
      typedef bankCPtr<T, BankSizeTrue>   BankCPtr;
      typedef bankPtr<T, BankSizeTrue>    BankPtr;
      typedef bankPtrPtr<T, BankSizeTrue> BankPtrPtr;
      typedef bankItem<T, BankSizeTrue>   BankElemTag;
      typedef T                           Element;
      typedef T*                          ElementPtr;

   public:
      fastPool() {
         BankPtr nb = createBank();
         head = nb;
         nb->prev = nb;
         nb->next = nb;
         pressure = 1;
         findBias = 1;
         banks = 1;
      }

      ~fastPool() {
         //while (head)
         //   contractBank(&head);
      }

      ElementPtr acquire() {
         return bankAcquire();
      }

      void acquiesce(ElementPtr elem) {
         bankRelinquish(elem);
      }

   protected:
      auto createBank
      () -> BankPtr {
         BankPtr newBank = new Bank;
         return newBank;
      }

      auto destroyBank
      (BankPtrPtr b) -> void {
         delete *b;
      }

      auto expandBank
      () -> BankPtr {
         BankPtr nb = createBank();

         if (findBias < 0) {
            // before head (h->prev & h->prev->next)
            (*nb).next.exchange(head);
            (*nb).prev.exchange((*head).prev);
            (*(*head).prev).next.exchange(nb);
            (*head).prev.exchange(nb);
         } else {
            // after head
            (*nb).prev.exchange(head);
            (*nb).next.exchange((*head).next);
            (*(*head).next).prev.exchange(nb);
            (*head).next.exchange(nb);
         }

         banks++;

         return nb;
      }

      auto contractBank
      (BankPtrPtr at) -> void {
         (*(**at).prev).next.exchange((**at).next);
         (*(**at).next).prev.exchange((**at).prev);
         banks--;
         destroyBank(at);
      }

      auto bankAcquire
      () -> ElementPtr {
         SEARCHING();

         BankPtr i = head;
         msize index = 0;
         bool step = findBias < 0;
         sint16 stepF = 0;

         if (!head) {
            GROWING();
            i = expandBank();
         }

         // search based on findBias
         do {
            stepF++;
            if ((index = i->occupancy--) != Bank::CollectionBitfield::BitInvalid)
               break;
            if (step)
               i = i->prev;
            else
               i = i->next;
         } while (i != head);

         if (index == Bank::CollectionBitfield::BitInvalid) {
            GROWING();
            i = expandBank();
            index = i->occupancy--;
         }

         // build the tag
         for (msize e = 0; e < BankSizeTrue; e++) {
            i->item[e].bank = i;
            i->item[e].bankItem = e;
         }

         findBias += sclamp(step ? stepF : -stepF, BIAS_MIN, BIAS_MAX);
         pressure++;

         return &(i->item[index].value);
      }

      auto bankRelinquish
      (ElementPtr what) -> void {
         // DON'T "bankRead"! This is an atomic operation!
         // also, it'll deadlock bankContract!
         //assert(!what->line);
         //assert(!what->lines);

         pressure--;

         // get the element tag
         BankElemTag* tag = reinterpret_cast<BankElemTag*>(what);

         BankPtr bank = tag->bank;
         bank->occupancy << tag->bankItem;
         if (  pressure < FLOW_SHRINK 
            && bank->occupancy.empty()
            && (bank->next != bank) // keep head (SEGV)
            ) {
            // contraction requires nobody enter the bit being removed
            SHRINKING();
            findBias += -FLOW_SHRINK;
            contractBank(&bank);
         }
      }

   private:
      atomic<msize>   banks;
      atomic<BankPtr> head;
      shared_mutex    resizeLock;
      shared_mutex    shrinkLock;

      sint64 pressure;
      sint64 findBias;
   };

}

#undef ALIGN_UNIQUE
#undef ALIGN_SHARED

#undef SEARCHING
#undef GROWING
#undef SHRINKING

#endif
