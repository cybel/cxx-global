#ifndef __UTIL_STATICMATH_H__
#define __UTIL_STATICMATH_H__

#include "head/stdtyp.h"
#include "head/stddef.h"

namespace util {
namespace math {

   template <uint64 B, uint64 P>
   struct Pow {
      static const uint64 value = B * Pow<B, P - 1>::value;   
   };

   template <uint64 B>
   struct Pow<B, 0> {
      static const uint64 value = 1;
   };

   template < typename T
            , typename U
            , typename V
            >
   alwaysinline constexpr_ T sclamp(T value, U low, V high) {
      return (value < low)  ? (low)
           : (value > high) ? (high)
           : (value)
           ;
   }
   

}}


#endif
