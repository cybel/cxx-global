#ifndef __UTIL_BASIC_FACTORY_H__
#define __UTIL_BASIC_FACTORY_H__

#if E_COMPILER == COMP_CLANG && !(E_DEBUG & DEBUG_NO_SUPPRESS_WARN)
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wundefined-var-template"
#endif

namespace util {
namespace factories {
namespace basic {
   using std::function;
   using std::map;
   using std::string;

   /////////////////////////////////////////////////////////////////////

#  define M(x) auto x
#  define SM(x) static auto x
#  define VM(x) virtual auto x


   /////////////////////////////////////////////////////////////////////

   struct FactoryBase;

   template<typename BaseT> 
   struct factory;


   /////////////////////////////////////////////////////////////////////

   template<typename BaseT>
   struct FactoryEntryBase {
      cstrc name;

      FactoryEntryBase(cstrc _name)
      : name (_name)
      {
         factory<BaseT>::addEntry(this);
      }

      VM(create)
      (rptr = nullptr) -> BaseT* 
      interface;
   };


   /////////////////////////////////////////////////////////////////////

   template<typename BaseT, typename ActualT>
   struct factoryEntry 
   : public FactoryEntryBase<BaseT> {

      factoryEntry(cstrc _name) 
      : FactoryEntryBase<BaseT> (_name)
      {}

      VM(create)
      (rptr where = nullptr) -> BaseT* {
         BaseT* obj = nullptr;
         if (!where) {
            obj = new ActualT;
         } else {
            obj = new (where) ActualT;
         }

         return obj;
      }
   };


   /////////////////////////////////////////////////////////////////////

   template <typename BaseT>
   struct factory {
      
      typedef map<std::string, FactoryEntryBase<BaseT>*> FactoryMap;
      typedef typename FactoryMap::const_iterator  iterator;

      SM(addEntry)
      (FactoryEntryBase<BaseT>* what) -> void {
         assert(what);

         factory& instance = getFactory();

         instance.factoryMap[what->name] = what;
      }

      SM(getEntry)
      (const std::string& name) -> FactoryEntryBase<BaseT>* {
         factory& instance = getFactory();

         return instance.factoryMap[name];
      }

      SM(create)
      (const std::string& name) -> BaseT* {
         FactoryEntryBase<BaseT>* f = getEntry(name);

         //assert(f);

         if (!f) 
            return nullptr;

         return f->create();
      }

      SM(create)
      (const iterator& i) -> BaseT* {
         if (i == end())
            return nullptr; 

         FactoryEntryBase<BaseT>* f = i->second;

         return f->create();
      }

      SM(begin)
      () -> iterator {
         factory& instance = getFactory();

         return instance.factoryMap.cbegin();
      }

      SM(end)
      () -> iterator {
         factory& instance = getFactory();

         return instance.factoryMap.cend();
      }


   private:
      SM(getFactory)
      () -> factory& {
         static factory<BaseT> instance;
         return instance;
      }

      FactoryMap factoryMap;
   };


   /////////////////////////////////////////////////////////////////////

#  undef M
#  undef SM
#  undef VM

}}}

#if E_COMPILER == COMP_CLANG && !(E_DEBUG & DEBUG_NO_SUPPRESS_WARN)
#  pragma clang diagnostic pop
#endif


#endif
