#ifndef __UTIL_TMP_H__
#define __UTIL_TMP_H__

namespace tmp {

   template <typename T, T FIRST, T ...REST>
   struct CountArgsImpl {
      enum {
         value = CountArgsImpl<T, REST...>::value + 1
      };
   };

   template <typename T, T ONLY>
   struct CountArgsImpl<T, ONLY> {
      enum {
         value = 1
      };
   };

   template <typename T, T ...ALL>
   using CountArgs = CountArgsImpl<T, ALL...>;

   /////////////////////////////////////////////////////////////////////

   template <typename FIRST, typename ...REST>
   struct CountTypesImpl {
      enum {
         value = CountTypesImpl<REST...>::value + 1
      };
   };

   template <typename ONLY>
   struct CountTypesImpl<ONLY> {
      enum {
         value = 1
      };
   };

   template <typename ...ALL>
   using CountTypes = CountTypesImpl<ALL...>;

   /////////////////////////////////////////////////////////////////////

   


}

#endif