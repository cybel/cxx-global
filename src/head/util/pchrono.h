#ifndef __PCHRONO_H__
#define __PCHRONO_H__

typedef std::chrono::steady_clock ProgramClock;
typedef std::chrono::time_point<std::chrono::steady_clock> ProgramMoment;

typedef std::chrono::system_clock::time_point RealMoment;

extern RealMoment    realStart;
extern ProgramMoment programStart;


alwaysinline
RealMoment programToReal(const ProgramMoment& moment) {
   RealMoment m = realStart + (moment - programStart);
   return m;
}

#endif
