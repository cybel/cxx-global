#ifndef __UTIL_FACTORIES_H__
#define __UTIL_FACTORIES_H__

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
// you can't tell the "potatoe potatoe" joke in text.
#     pragma warning(push)
#     pragma warning(disable:4127)
#  elif E_COMPILER == COMP_CLANG
// we're aware of the oddness that factories entails:
//    instantiation of variable 'FactoryBase<562361691>::factories' 
//    required here, but no definition is available
//    add an explicit instantiation declaration to suppress this warning
//    if 'FactoryBase<562361691>::factories' is explicitly instantiated
//    in another translation unit.
// this is done by the SIMPLE_FACTORY_DEFINE() macro, which creates an
// instance of the variables in the area chosen by the user to serve as
// a hub for a factory's products.
#     pragma clang diagnostic push
#     pragma clang diagnostic ignored "-Wundefined-var-template"
#  endif
#endif

#include "head/stdtyp.h"
#include "head/stddef.h"
#include "head/stdlibs.h"

//
// TYPESET_{WHAT} . Take the macro name, feed it into sha1sum 
// and take the LSB 8 digits to produce the macro value.
//
// Example: NAMESPACE_MEMORY_SOURCE (0x2184f55b)
//

#if E_COMPILER == COMP_MSVC
#  define SIMPLE_FACTORY_DEFINE(NamespaceV) \
      template<> \
      std::map< std::string \
              , ::util::pattern::factory::simple::FactoryBase<NamespaceV>* \
              > \
         (::util::pattern::factory::simple::FactoryBase<NamespaceV>::factories); \
      template<> \
      std::set<std::string> \
         (::util::pattern::factory::simple::FactoryBase<NamespaceV>::typesKnown)
#else
#  define SIMPLE_FACTORY_DEFINE(NamespaceV) \
      namespace util    { \
      namespace pattern { \
      namespace factory { \
      namespace simple  { \
         template<> \
         std::map< std::string \
                 , ::util::pattern::factory::simple::FactoryBase<NamespaceV>* \
                 >* \
            FactoryBase<NamespaceV>::factories = nullptr; \
         template<> \
         std::set<std::string>* \
            FactoryBase<NamespaceV>::typesKnown = nullptr; \
      }}}}

#endif

#define SIMPLE_FACTORY_ITEM_DECLARE(NamespaceV, Type) \
   public: \
      static const std::string                                                     _StaticFactory_name; \
      static const ::util::pattern::factory::simple::factoryItem<NamespaceV, Type> _StaticFactory_registration

#define SIMPLE_FACTORY_ITEM_DEFINE(NamespaceV, Type) \
   const std::string                                               Type::_StaticFactory_name = #Type; \
   const ::util::pattern::factory::simple::factoryItem<NamespaceV, Type> Type::_StaticFactory_registration

#define SIMPLE_FACTORY_FOR(NamespaceV) \
   ::util::pattern::factory::simple::FactoryBase<NamespaceV>

namespace util    {
namespace pattern {
namespace factory {
namespace simple  {

   // --- FactoryBase ----------------------------------------------------------

   template <unsigned Namespace_V>
   class FactoryBase {
   private:
      static std::map<std::string, FactoryBase*>* factories;
      static std::set<std::string>*               typesKnown;

      virtual crptr create(crptr at = nullptr) = 0;
      virtual void  destruct(crptr what) = 0;
      virtual void  destroy(crptr what) = 0;
      virtual msize size() = 0;

   protected:
      FactoryBase(const std::string& name_)
      : name(name_) {
         if (!factories)
            factories = new std::map< std::string
                                    , FactoryBase<Namespace_V>* 
                                    >;
         (*factories)[name_] = this;

         if (!typesKnown)
            typesKnown = new std::set<std::string>;
         typesKnown->insert(name);
      }

      FactoryBase(const FactoryBase&) = delete;
      const FactoryBase& operator=(const FactoryBase&) = delete;

   public:
      const std::string name;
      static const unsigned Namespace = Namespace_V;

      virtual ~FactoryBase() 
      {}

      const decltype(typesKnown)& types() const {
         return typesKnown;
      }

      template <typename T>
      static T *const create(crptr at = nullptr) {
         if (Namespace_V != T::_StaticFactory_registration.Namespace)
            return nullptr;
         if (!factories)
            return nullptr;
         auto i = (*factories)[T::_StaticFactory_registration.name];
         if (!i)
            return nullptr;
         return static_cast<T*>(i->create(at));
      }

      template <typename T>
      static T *const create(const std::string& name, crptr at = nullptr) {
         if (!factories)
            return nullptr;
         auto i = (*factories)[name];
         if (!i)
            return nullptr;
         if (i->Namespace != Namespace_V)
            return nullptr;
         return static_cast<T*>(i->create(at));
      }

      template <typename T>
      static T *const create(cstrc name, crptr at = nullptr) {
         if (!factories)
            return nullptr;
         auto i = (*factories)[name];
         if (!i)
            return nullptr;
         if (i->Namespace != Namespace_V)
            return nullptr;
         return static_cast<T*>(i->create(at));
      }

      template <typename T>
      static void destroy(T* what) {
         assert(what);
         if (!factories)
            return;
         auto i = (*factories)[T::_StaticFactory_registration.name];
         if (!i)
            return;
         i->destroy((rptr)what);
      }

      template <typename T>
      static void release(T *const what) {
         assert(what);
         delete what;
      }
   };

   // --- factoryItem ----------------------------------------------------------

   template <unsigned Namespace_V, typename T>
   class factoryItem
   : public FactoryBase<Namespace_V> {
   private:
      virtual crptr create(crptr at = nullptr) {
         if (at)
            return new (at) T;
         else
            return new T;
      }

      virtual void destruct(crptr what) {
         assert(what);
         ((T*)what)->~T();
      }

      virtual void destroy(crptr what) {
         assert(what);
         delete (T*)what;
      }

      virtual msize size() {
         return sizeof(T);
      }

   public:
      factoryItem()
      : FactoryBase<Namespace_V>(T::_StaticFactory_name)
      {}

      factoryItem(cstrc name_)
      : FactoryBase<Namespace_V>(name_)
      {}

      virtual ~factoryItem()
      {}

      factoryItem(const factoryItem&) = delete;
      const factoryItem& operator=(const factoryItem&) = delete;

      // implement accessors

      static const FactoryBase<Namespace_V>& factory() {
         return T::_StaticFactory_registration;
      }

      static unsigned getNamespace() {
         return FactoryBase<Namespace_V>::Namespace;
      }
   };

}}}}

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(pop)
#  elif E_COMPILER == COMP_CLANG
#     pragma clang diagnostic pop
#  endif
#endif

#endif

