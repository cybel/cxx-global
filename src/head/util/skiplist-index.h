#ifndef __UTIL_SKIPLISTINDEX_H__
#define __UTIL_SKIPLISTINDEX_H__

#include "head/platform.h"

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(push)
#     pragma warning(disable:4706) 
#     pragma warning(disable:4365) 
#  endif
#endif

#include "head/stdtyp.h"
#include "head/stdlibs.h"

#include "head/util/math.h"
#include "head/util/static-math.h"

namespace util  {
namespace index {
   /* SkiplistIndex
    * 
    * Template Parameters:
    *    IndexT      - A numeric type for the index (access by)
    *    IndexWidthV - Width of the index
    *    ContentT    - An entry value
    *    NextF       - Next entry. Return false if passed in is end
    *    PrevF       - Previous entry. Return false if passed in is first
    *    WidthF      - Total number of entries.
    *    CompareF    - Compare two entries. -1 First < Second, +1 First > Second
    *    SearchF     - Compare entry to search value. Same result as CompareF
    */
   namespace {
      template < typename ReferenceT
               , typename IndexT
               , typename ContentT
               , typename SearchT
               >
      struct SkiplistIndexCallbacks {
         typedef bool   (*NextF)   (const ReferenceT&, IndexT* const, const ContentT** const);
         typedef bool   (*PrevF)   (const ReferenceT&, IndexT* const, const ContentT** const);
         typedef IndexT (*WidthF)  (const ReferenceT&);
         typedef sint8  (*CompareF)(const ReferenceT&, const ContentT* const, const ContentT* const);
         typedef sint8  (*SearchF) (const ReferenceT&, const ContentT* const, const SearchT);
      };
   }

   using namespace ::util::math;
#  define FTYPE(x) SkiplistIndexCallbacks<ReferenceT,IndexT,ContentT,SearchT>::x x
   template < typename ReferenceT
            , typename IndexT       // index access type (numeric)
            , IndexT   IndexWidthV  // index width
            , typename ContentT     // type of content indexed
            , typename SearchT      // search for type
            , typename FTYPE(NextF)
            , typename FTYPE(PrevF)
            , typename FTYPE(WidthF)
            , typename FTYPE(CompareF)
            , typename FTYPE(SearchF)
            >
#  undef FTYPE
   class SkiplistIndex {
   private:
      /* Index in a pure ideal form (as created by rebuildIndex).
       *    IndexWidthV = 4 (5 allocations, zero is for FIRST element always)
       *    WidthF      = 14
       *    StepWidth   = 14/4 = 3
       *    
       *    A - Underlying data
       *    B - Skiplist index
       *     A | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 |
       *       | ^ | ..... | ^ | ..... | ^ | ..... | ^ | ....... |  ^ |
       *     B |[0]|       | 1 |       | 2 |       | 3 |         | *4 |
       *                                                          /
       *                *4 = Extra element in index created by   /
       *                     first element being reserved.      /
       *             j          i                              /
       *       index[0] = value[0]                            /
       *       j += 1   | i += 3                             /
       *       index[1] = value[3]                          /
       *       ...                                         /
       *       index[4] = value[12] ----------------------/
       *       
       *       ** Terminate: i > IndexWidthV (<= means continue)
       *
       * Over time, this can become *severely* distorted. Furthermore, if the
       * underlying values are too few, there will be nulls. Nulls are ignored
       * during a search.
       */

      const ContentT* index[IndexWidthV+1];
      IndexT          indices[IndexWidthV+1];

      ReferenceT& ref;

   protected:
      static const IndexT IndexSize = IndexWidthV+1;

      // do a straight-up linear search starting at a specific index
      alwaysinline const ContentT* const linearSearch(const SearchT value, IndexT at = 0, const sint8 direction = 1) const {
         assert(at < IndexSize);
         const ContentT* i = index[at];
         IndexT idx = indices[at]; // preserve indices which can be destroyed by search
         sint8 sv;

         assert(direction);
         assert(i);
         assert(idx < WidthF(ref));

         while ( (direction > 0) 
               ? (NextF(ref, &idx, &i)) 
               : (PrevF(ref, &idx, &i))
               ) 
            if ((sv = SearchF(ref, i, value)) == direction)
               continue;
            else if (sv == -direction)
               return nullptr;
            else
               return i;
         return nullptr;
      }

      // find value using first a binary search (degenerates into a linear)
      alwaysinline const ContentT* const binarySearch(const SearchT value, IndexT at = IndexSize >> 1, const unsigned depth = 1) const {
         assert(depth > 0);

         // find an index 
         at = binarySearchIndex(value, at, depth);
         sint8 sv = SearchF(ref, index[at], value);

         // terminate with a linear search or the value
         if (sv)
            return linearSearch(value, at, sgn(sv));
         else
            return index[at];
      }

      // find nearest index value with a binary search
      alwaysinline IndexT binarySearchIndex(const SearchT value, IndexT at = IndexSize>>1, const unsigned depth = 1) const {
         assert(depth > 0);

         // constrain
         at = clamp<IndexT, 0, IndexWidthV>(at);
         sint8 sv = SearchF(ref, index[at], value);

         // terminate with a linear search
         if (sv && !(IndexSize >> depth))
            return at;

         // step neg or pos, or found depending on value.
         switch (sgn(sv)) {
         case -1:
            return binarySearchIndex(value, at - (IndexSize >> depth), depth+1); // 4365
         break;
         case 0:
            return at;
         break;
         case 1:
            return binarySearchIndex(value, at + (IndexSize >> depth), depth+1); // 4365
         break;
         default:
            assert(false);
         }

         assert(false);
         return IndexSize;
      }

      // find the indice in the index
      alwaysinline IndexT binarySearchIndices(const IndexT value, IndexT at = IndexSize>>1, const unsigned depth = 1) const {
         assert(depth > 0);
         assert(value >= 0);
         assert(value < IndexSize);

         // constrain
         at = clamp<IndexT, 0, IndexWidthV>(at);
         sint8 sv = (at > value)?(-1)
                  : (at < value)?( 1)
                  : (0)
                  ;

         // terminate with a linear search
         if (sv && !(IndexSize >> depth))
            return at;

         // step neg or pos, or found depending on value.
         switch (sgn(sv)) {
         case -1:
            return binarySearchIndices(value, at - (IndexSize >> depth), depth+1);
         break;
         case 0:
            return at;
         break;
         case 1:
            return binarySearchIndices(value, at + (IndexSize >> depth), depth+1);
         break;
         default:
            assert(false);
         }

         assert(false);
         return IndexSize;
      }

   public:
      SkiplistIndex(ReferenceT& ref_, ContentT* firstElement = nullptr) 
      : ref(ref_) {
         if (firstElement)
            rebuildIndex(firstElement);
         else
            purgeIndex();
      }

      SkiplistIndex(const SkiplistIndex& other) 
      : ref(other.ref) 
      , index(other.index)
      , indices(other.indices) 
      {}

      ~SkiplistIndex() {
      
      }

      SkiplistIndex& operator=(const SkiplistIndex&) = delete;
      
      alwaysinline IndexT indexWidth() const {
         return IndexSize;
      }

      alwaysinline void purgeIndex() {
         for (IndexT i = 0; i < IndexSize; i++)
            index[i] = nullptr;
      }

      void rebuildIndex(const ContentT* firstElement) {
         assert(firstElement);

         IndexT          i       = 0;
         IndexT          j       = 1;
         IndexT          width   = WidthF(ref);
         IndexT          step    = (IndexT) max(1u, (IndexT) (width / IndexSize));
         const ContentT* current = firstElement;
         bool            more    = true;
         
         assert(width);

         // purge old index contents
         for (IndexT idx = 1; idx < IndexSize; idx++) {
            indices[idx] = width;
            index[idx] = nullptr;
         }
         index[0] = current;
         indices[0] = 0;

         // while there are entries, skip based on step
         while (i < width && j < IndexSize) {
            while ((more = NextF(ref, &i, &current)) && (i % step));
            if (!more)
               break;
            assert(current);
            assert(!(i % step));
            assert(i / step == j);
            
            // store i'th element, which is (i/step)
            indices[j] = i;
            index[j++] = current;
         }
      }

      const ContentT* const find(const SearchT value) const {
         if (WidthF(ref) < IndexSize)
            return linearSearch(value);
         else
            return binarySearch(value);
      }

      /*const ContentT* const find(const IndexT at) const {
         
      }*/

      // call before removing to ensure de-indexed
      alwaysinline void hintRemove(const SearchT value) {
         const std::function<bool (const ReferenceT&, IndexT* const, const ContentT** const)> f[2] = {PrevF, NextF};
         IndexT at = binarySearchIndex(value);

         /* shift the indexed value if it is hit toward the middle. if there
          * are many deletions, the index could become focused in the middle.
          */
         if (!SearchF(ref, index[at], value))
             if (!f[(at > IndexSize>>1)?(0):(1)](ref, &indices[at], &index[at]))
                f[(at > IndexSize>>1)?(1):(0)](ref, &indices[at], &index[at]);
      }

      // call after insert to consider for indexing
      alwaysinline void hintInsert(const SearchT value) {
         //const std::function<bool (const ReferenceT&, IndexT* const, const ContentT** const)> f[2] = {PrevF, NextF};
         IndexT at = binarySearchIndex(value);
         
         // Indexing should move away from middle
         switch (SearchF(ref, index[at], value)) {
         case -1:
            // if new value is less then index, and index is less then middle, move.
            if (at < IndexSize>>1)
               PrevF(ref, &indices[at], &index[at]);
         break;
         case 1:
            // if new value is more then index, and index is more then middle, move.
            if (at > IndexSize>>1)
               NextF(ref, &indices[at], &index[at]);
         break;
         default: 
            return;
         }
      }

      /* O(n) . Underlying indices might not exist, 
       * so we are responsible to ensure consistency
       */
      void hintUpdateIndices() {
         IndexT at = 0; // index in underlying data
         IndexT idx = 0; // index in index
         const ContentT* elem = index[idx];

         // find the true first element
         while (PrevF(ref, &at, &elem))
            at = 0;

         // ensure it is the root
         index[idx]   = elem;
         indices[idx] = at;

         // when you hit a match, update the indices
         do if (!CompareF(ref, index[idx], elem)) {
            indices[idx] = at;
            idx++;
         } while (NextF(ref, &at, &elem) && idx < IndexSize);

      }

      // TODO: hintRebalanceIndex (move index points to reasonable positions)

   };

}}

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(pop)
#  endif
#endif

#endif

