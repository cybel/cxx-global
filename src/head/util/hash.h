#ifndef __UTIL_HASH_H__
#define __UTIL_HASH_H__

#include "head/stdtyp.h"

namespace util {
namespace hash {
//#  ifdef __cplusplus
//   extern "C" {
//#  endif
   void murmur3_32 (rptr in, size_t length, rptr out);
   void murmur3_128(rptr in, size_t length, rptr out);

   void murmur3_32 (rptr in, size_t length, rptr out, uint32 offset);
   void murmur3_128(rptr in, size_t length, rptr out, uint32 offset);
//#  ifdef __cplusplus
//   }
//#  endif
}}

#endif
