#ifndef __UTIL_BASIC_LOOKUP_H__
#define __UTIL_BASIC_LOOKUP_H__

#if E_COMPILER == COMP_CLANG && !(E_DEBUG & DEBUG_NO_SUPPRESS_WARN)
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wundefined-var-template"
#endif

namespace util {
namespace lookup {
namespace basic {
   using std::function;
   using std::map;
   using std::string;

   /////////////////////////////////////////////////////////////////////

#  define M(x) auto x
#  define SM(x) static auto x
#  define VM(x) virtual auto x


   /////////////////////////////////////////////////////////////////////

   struct LookupBase;

   template<typename BaseT> 
   struct lookup;


   /////////////////////////////////////////////////////////////////////

   template<typename BaseT>
   struct LookupEntryBase {
      cstrc  name;
      BaseT& lvalue;

      LookupEntryBase(cstrc _name, BaseT& _value)
      : name   (_name)
      , lvalue (_value)
      {
         lookup<BaseT>::addEntry(this);
      }

      M(value) 
      () -> BaseT& {
         return lvalue;
      }
   };


   /////////////////////////////////////////////////////////////////////

   template<typename BaseT, typename ActualT>
   struct lookupEntry 
   : public LookupEntryBase<BaseT> {

      lookupEntry(cstrc _name, ActualT &_value) 
      : LookupEntryBase<BaseT> (_name, _value)
      {}

      M(valueTyped) 
      () -> ActualT& {
         return static_cast<ActualT&>(LookupEntryBase<BaseT>::value());
      }

   };


   /////////////////////////////////////////////////////////////////////

   template <typename BaseT>
   struct lookup {
      
      typedef map<std::string, LookupEntryBase<BaseT>*> LookupMap;
      typedef typename LookupMap::const_iterator        iterator;

      SM(addEntry)
      (LookupEntryBase<BaseT>* what) -> void {
         assert(what);

         lookup& instance = getLookup();

         instance.lookupMap[what->name] = what;
      }

      SM(getEntry)
      (const std::string& name) -> LookupEntryBase<BaseT>* {
         lookup& instance = getLookup();

         return instance.lookupMap[name];
      }

      SM(get)
      (const std::string& name) -> BaseT* {
         LookupEntryBase<BaseT>* f = getEntry(name);

         assert(f);

         if (!f) 
            return nullptr;

         return &f->value();
      }

      SM(get)
      (const iterator& i) -> BaseT* {
         if (i == end())
            return nullptr; 

         LookupEntryBase<BaseT>* f = i->second;

         return &f->value();
      }

      SM(begin)
      () -> iterator {
         lookup& instance = getLookup();

         return instance.lookupMap.cbegin();
      }

      SM(end)
      () -> iterator {
         lookup& instance = getLookup();

         return instance.lookupMap.cend();
      }


   private:
      SM(getLookup)
      () -> lookup& {
         static lookup<BaseT> instance;

         return instance;
      }

      LookupMap lookupMap;

   };

   /////////////////////////////////////////////////////////////////////

#  undef M
#  undef SM
#  undef VM

}}}

#if E_COMPILER == COMP_CLANG && !(E_DEBUG & DEBUG_NO_SUPPRESS_WARN)
#  pragma clang diagnostic pop
#endif


#endif
