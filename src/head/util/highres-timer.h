#pragma once
#ifndef __UTIL_HIGHRESTIMER_H__
#define __UTIL_HIGHRESTIMER_H__

#include "head/common.h"

namespace util{
   // exp(10,-x) == val
   enum class HighresTimerResolution : uint8
   {  DECI  = 1
   ,  CENTI = 2
   ,  MILLI = 3
   ,  MICRO = 6
   ,  NANO  = 9
   ,  PICO  = 12
   };

   class HighresTimerBase {
   public:
      virtual ~HighresTimerBase() = 0;

      // return drift
      virtual uint64 update() = 0;
      // return now
      virtual uint64 now() = 0;
      virtual uint64 lastUpdate() const = 0;

      // absolute
      virtual uint64 markAbs() const = 0;
      // relative to now
      virtual sint64 markRel() = 0; 
      // set mark to now
      virtual void   setMark() = 0;
      // set mark to absolute
      virtual void   setMark(uint64 when) = 0;
      // set mark to relative
      virtual void   setMark(sint64 when) = 0;
      // tell if mark is past
      virtual bool   markPast() = 0;
      // tell if mark is future
      virtual bool   markFuture() {
         return !markPast();
      }

      // resolution
      virtual HighresTimerResolution resolution() const = 0;

      virtual uint64 frequency() const = 0;
   };

}

#  if E_OS==OS_WINDOWS
#     include "head/os/win/util/highresTimer.h"
#  endif

#endif 
