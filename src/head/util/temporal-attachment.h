#ifndef __UTIL_TEMPORAL_PRECURSOR_H__
#define __UTIL_TEMPORAL_PRECURSOR_H__

#include "head/libco.h"

namespace util       {
namespace relational {
   using std::thread;
   using std::vector;
   using std::map;
   using std::any_of;
   using std::thread;
   using std::find;
   using std::make_pair;
   using std::this_thread::get_id;

   template < typename FRAME_T
            , typename FAMILY_T = vector<FRAME_T>
            >
   struct PolicyThreadId {
      typedef FRAME_T                             FrameT;
      typedef FAMILY_T                            FamilyT;
      typedef typename FAMILY_T::iterator         FamilyIterator;
      typedef typename FAMILY_T::reverse_iterator FamilyRIterator;

      typedef map<thread::id, FAMILY_T*>          FamiliesMap;
      typedef typename FamiliesMap::iterator      FamiliesIterator;

      PolicyThreadId() {

      }
      
      FamilyT* getFamily() {
         auto i = get_id();
         const FamiliesIterator& j = families.find(i);

         if (j == families.end()) {
            families[i] = new FAMILY_T;
            assert(families.find(i) != families.end());
            return families[i];
         }

         return j->second;
      }   

      void terminate(FamilyT* family) {
         auto i = get_id();
         const FamiliesIterator& j = families.find(i);

         assert(j->first  == i);
         assert(j->second == family);

         delete j->second;
         families.erase(j);
      }

   private:
      FamiliesMap families;
   };

   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////

   template < typename FRAME_T
            , typename FAMILY_T = vector<FRAME_T>
            >
   struct PolicyCoThreadId {
      typedef FRAME_T                             FrameT;
      typedef FAMILY_T                            FamilyT;
      typedef typename FAMILY_T::iterator         FamilyIterator;
      typedef typename FAMILY_T::reverse_iterator FamilyRIterator;

      typedef map<cothread_t, FAMILY_T*>          FamiliesMap;
      typedef typename FamiliesMap::iterator      FamiliesIterator;

      PolicyCoThreadId() {

      }

      ~PolicyCoThreadId() {
         for (auto family: families) 
            delete family.second;
      }
      
      FamilyT* getFamily() {
         cothread_t i = co_active();
         const FamiliesIterator& j = families.find(i);

         if (j == families.end()) {
            families[i] = new FAMILY_T;
            assert(families.find(i) != families.end());
            return families[i];
         }

         return j->second;
      }   

      void terminate(FamilyT* family) {
         cothread_t i = co_active();
         const FamiliesIterator& j = families.find(i);

         assert(j->first  == i);
         assert(j->second == family);

         delete j->second;
         families.erase(j);
      }

   private:
      FamiliesMap families;
   };

   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////

   template < typename T
            , template < class FRAME_T  = T
                       , class FAMILY_T = vector<FRAME_T>
                       > class POLICY   = PolicyThreadId
            >
   struct temporalAttachment {
      typedef POLICY<T>                           PolicyT;
      typedef typename POLICY<T>::FrameT          FrameT;
      typedef typename POLICY<T>::FamilyT         FamilyT;
      typedef typename POLICY<T>::FamilyIterator  FamilyIterator;
      typedef typename POLICY<T>::FamilyRIterator FamilyRIterator;

      temporalAttachment() {}
      ~temporalAttachment() {}

      void create(const T& what) {
         FamilyT *const family = policy.getFamily();
         
         family->push_back(what);
      }

      bool destroy(const T& what) {
         FamilyT *const family = policy.getFamily();
         FamilyRIterator i = find(family->rbegin(), family->rend(), what);

         assert(family->size());
         assert(i != family->rend());

         if (i == family->rend()) 
            return false;

         family->erase(--family->rbegin().base(), i.base());
         
         if (!family->size()) 
            policy.terminate(family);

         return true;
      }

      FrameT current() {
         FamilyT *const family = policy.getFamily();

         return family->empty() ? nullptr : family->back();
      }


      FamilyT& family() {
         return *policy.getFamily();
      }

   private:
      PolicyT policy;

   };
   
}}

#endif

