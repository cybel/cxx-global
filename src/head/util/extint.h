#ifndef __UTIL_Varint_H__
#define __UTIL_Varint_H__

#include "head/stdtyp.h"
#include "head/stddef.h"

// the general idea is simply do as much as possible with the compiler

namespace util {
   enum class VarintRepSize : uint8 
   {  U8  = 1
   ,  U16 = 2
   ,  U32 = 4
   ,  U64 = 8
   ,  S8  = 16
   ,  S16 = 32
   ,  S32 = 64
   ,  S64 = 128
   };

   inline size_t VarintRepSizeBytes(const VarintRepSize &r) {
      return ((size_t)r) | (((size_t)r) >> 4);
   }

   // enough information to select signed/unsigned interface
   // technically, we could give a generic (unsigned) interface
   // here, and then delete it for the unsigned case...
   class VarintBase {
   public:
      inline virtual unsigned      bits() const = 0;
      inline virtual bool          isSigned() const = 0;
      inline virtual VarintRepSize representSize() const = 0;
   };

   class VarintUnsigned : public VarintBase {
   public:
      typedef uintMax RepType;

      inline virtual bool isSigned() const {
         return false;
      }

      inline virtual operator RepType() const = 0;
      inline virtual RepType operator ++(int) = 0;
      inline virtual RepType operator --(int) = 0;
      inline virtual RepType operator ++() = 0;
      inline virtual RepType operator --() = 0;
      inline virtual RepType operator  =(const RepType i) = 0;
      inline virtual RepType operator |=(const RepType i) = 0;
      inline virtual RepType operator ^=(const RepType i) = 0;
      inline virtual RepType operator &=(const RepType i) = 0;
      inline virtual RepType operator<<=(const RepType i) = 0;
      inline virtual RepType operator>>=(const RepType i) = 0;
      inline virtual RepType operator +=(const RepType i) = 0;
      inline virtual RepType operator -=(const RepType i) = 0;
      inline virtual RepType operator *=(const RepType i) = 0;
      inline virtual RepType operator /=(const RepType i) = 0;
      inline virtual RepType operator %=(const RepType i) = 0;
   };

   class VarintSigned : public VarintBase {
   public:
      typedef sintMax RepType;

      inline virtual bool isSigned() const {
         return true;
      }

      inline virtual operator RepType() const = 0;
      inline virtual RepType operator ++(int) = 0;
      inline virtual RepType operator --(int) = 0;
      inline virtual RepType operator ++() = 0;
      inline virtual RepType operator --() = 0;
      inline virtual RepType operator  =(const RepType i) = 0;
      inline virtual RepType operator |=(const RepType i) = 0;
      inline virtual RepType operator ^=(const RepType i) = 0;
      inline virtual RepType operator &=(const RepType i) = 0;
      inline virtual RepType operator<<=(const RepType i) = 0;
      inline virtual RepType operator>>=(const RepType i) = 0;
      inline virtual RepType operator +=(const RepType i) = 0;
      inline virtual RepType operator -=(const RepType i) = 0;
      inline virtual RepType operator *=(const RepType i) = 0;
      inline virtual RepType operator /=(const RepType i) = 0;
      inline virtual RepType operator %=(const RepType i) = 0;
   };


   template <bool Signed, unsigned Bits>
   class Varint 
      : public std::conditional_t<Signed, VarintSigned, VarintUnsigned> {
   public:
      typedef std::conditional_t<Signed, VarintSigned, VarintUnsigned> Parent;
      typedef typename Parent::RepType RepType;

   private:
      typedef std::conditional_t< Bits <=  8, uint8
            , std::conditional_t< Bits <= 16, uint16
            , std::conditional_t< Bits <= 32, uint32
            , std::enable_if_t  < Bits <= 64, uint64
            >> >> internalRepUnsigned;

      typedef std::conditional_t< Bits <=  8, sint8
            , std::conditional_t< Bits <= 16, sint16
            , std::conditional_t< Bits <= 32, sint32
            , std::enable_if_t  < Bits <= 64, sint64
            >> >> internalRepSigned;

      typedef typename std::conditional_t<Signed, internalRepSigned, internalRepUnsigned> internalRep;

      internalRep value;

      enum : internalRep
      { BitMax  = ((util::Popcount<Bits>::value==1 && Bits>4) ?                   (0) : (1ull << (Bits - 1)))
      , BitMask = ((util::Popcount<Bits>::value==1 && Bits>4) ? ((internalRep)(-1ll)) : ((BitMax << 1) - 1))
      };

      template <typename T>
      alwaysinline internalRep clip(T x) {
#        if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#           if E_COMPILER==COMP_MSVC
#              pragma warning(push)
#              pragma warning(disable:4127)
#           endif
#        endif
         if (util::Popcount<Bits>::value==1 && Bits>4) 
            return (internalRep) x; // we don't need to do anything!
         else
            if (Signed) 
               return (internalRep) (((x & BitMask) ^ BitMax) - BitMax);
            else
               return (internalRep) (x & BitMask);
#        if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#           if E_COMPILER==COMP_MSVC
#              pragma warning(pop)
#           endif
#        endif
      }

   public:
      Varint() 
         : value(0) {}
      Varint(const RepType x)
         : value(clip(x)) {}

      inline virtual unsigned bits() const {
         return Bits;
      }

      inline virtual VarintRepSize  representSize() const {
#        if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#           if E_COMPILER==COMP_MSVC
#              pragma warning(push)
#              pragma warning(disable:4127)
#           endif
#        endif

         if (Signed)
            return (VarintRepSize)(sizeof(internalRep)<<4);
         else
            return (VarintRepSize)(sizeof(internalRep));

#        if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#           if E_COMPILER==COMP_MSVC
#              pragma warning(pop)
#           endif
#        endif
      }

      //inline virtual operator internalRep() const {
      //   return value;
      //}

      inline virtual operator RepType() const {
         return (RepType)value;
      }

      inline virtual RepType operator ++(int) {
         internalRep v = value;
         value = clip(value + 1);
         return (RepType)v;
      }

      inline virtual RepType operator --(int) {
         internalRep v = value;
         value = clip(value - (internalRep)1);
         return v;
      }

      inline virtual RepType operator ++() {
         return value = clip(value + 1);
      }

      inline virtual RepType operator --() {
         return value = clip(value - 1);
      }

      inline virtual RepType operator  =(const RepType i) {
         return value = clip(i);
      }

      inline virtual RepType operator |=(const RepType i) {
         return value = clip(value | i);
      }

      inline virtual RepType operator ^=(const RepType i) {
         return value = clip(value ^ i);
      }

      inline virtual RepType operator &=(const RepType i) {
         return value = clip(value & i);
      }

      inline virtual RepType operator<<=(const RepType i) {
         return value = clip(value << i);
      }

      inline virtual RepType operator>>=(const RepType i) {
         return value = clip(value >> i);
      }

      inline virtual RepType operator +=(const RepType i) {
         return value = clip(value + i);
      }

      inline virtual RepType operator -=(const RepType i) {
         return value = clip(value - i);
      }

      inline virtual RepType operator *=(const RepType i) {
         return value = clip(value * i);
      }

      inline virtual RepType operator /=(const RepType i) {
         return value = clip(value / i);
      }

      inline virtual RepType operator %=(const RepType i) {
         return value = clip(value % i);
      }

      template <unsigned O>
      inline RepType operator=(const Varint<Signed, O> &o) {
         return value = clip((RepType)o);
      }

      template <unsigned O>
      inline RepType operator=(const Varint<!Signed, O> &o) {
         return value = clip((RepType)o);
      }
   };

   template <unsigned Bits>
   using UVarint = Varint<false, Bits>;

   template <unsigned Bits>
   using SVarint = Varint<true, Bits>;

#if E_COMPILER==COMP_CLANG
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wmissing-declarations"
#endif

#  define i(b) template Varint<false,b>;         \
               template Varint<true,b>;          \
               typedef  Varint<false,b> uint##b; \
               typedef  Varint<true,b>  sint##b
#  define j(b) template Varint<false,b>;            \
               template Varint<true,b>;             \
               typedef  Varint<false,b> uint##b##v; \
               typedef  Varint<true,b>  sint##b##v

   // multiples of 4 are "universal"

   i(4);
   j(8);
   i(12);
   j(16);
   i(20);
   i(24);
   i(28);
   j(32);
   i(36);
   i(40);
   i(44);
   i(48);
   i(52);
   i(56);
   i(60);
   j(64);

#  undef i
#  undef j

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER==COMP_CLANG
#     pragma clang diagnostic pop
#  endif
#endif
}

#endif

