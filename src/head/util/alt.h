#ifndef __UTIL_ALT_H__
#define __UTIL_ALT_H__

#include "head/common.h"

namespace alt {
   // forward various std:: replacements
   // or standins to their appropriate locations here.

   // basically, alias everything using our own guts...

   //template <typename T, typename A = memory::Allocator<T>>
   //using vector = std::vector<T, A>;
}

#endif
