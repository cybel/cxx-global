#ifndef __UTIL_THREADED_H__
#define __UTIL_THREADED_H__

#include "head/common.h"

namespace util {
   typedef std::function<intptr_t(void*)> ThreadFunction;

   class ThreadBase {
   public:
      virtual void start() = 0;
      virtual void stop() = 0;

      virtual void suspend() = 0;
      virtual void resume() = 0;

      virtual void join() = 0;

      virtual bool   complete() = 0;
      virtual bool   running() = 0;
      virtual bool   dead() = 0;
      virtual bool   suspended() = 0;
      virtual uint64 id() = 0;
   };
   
   class Thread;

   class Threaded {
   public:
      virtual const ThreadFunction entry() = 0;
      virtual       void           thread(Thread &t) = 0;
   };

}

#if E_OS==OS_WIN32 || E_OS==OS_WIN64
#  include "head/os/win/util/threaded.h"
#elif !(E_DEBUG & DEBUG_INCONSEQUENTIAL)
#  warning "No thread implementation exists for this platform"
#endif

#endif

