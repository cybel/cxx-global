#ifndef __UTIL_MATH_H__
#define __UTIL_MATH_H__

#include "head/platform.h"

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
// only you can prevent forest fires.
#     pragma warning(push)
#     pragma warning(disable:4018)
#  elif E_COMPILER == COMP_CLANG
#     pragma clang diagnostic push
#     pragma clang diagnostic ignored "-Wsign-compare"
#  endif
#endif

namespace util {
namespace math {

template <typename T, typename U>
alwaysinline auto max(T a, U b) -> decltype(a+b) {
   return (a > b) ? (a) : (b);
}

template <typename T, typename U>
alwaysinline auto min(T a, U b) -> decltype(a+b) {
   return (a < b) ? (a) : (b);
}

//template <typename T, typename U>
//alwaysinline auto clamp(T x, U mn, U mx) -> decltype(x+min+max) {
//   return min(max(x, mn), mx);
//}

}}

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER == COMP_MSVC
#     pragma warning(pop)
#  elif E_COMPILER == COMP_CLANG
#     pragma clang diagnostic pop
#  endif
#endif

#endif

