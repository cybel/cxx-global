#ifndef __UTIL_INVOKE_PARSE_H__
#define __UTIL_INVOKE_PARSE_H__

#include <string>
#include <set>
#include <map>
#include <vector>

#include "head/util/math.h"
#include "head/util/transaction/simple-linear.h"
#include "head/opengl/opengl.h"

namespace util {
namespace env  {
   using namespace ::config::util::env;
   using std::string;
   using std::set;
   using std::map;
   using std::vector;
   
   enum : uint8 
   {  ENV_SWITCH_TYPE = 0                       // packed only
   ,  ENV_SWITCH_LONG                           // valid
   ,  ENV_SWITCH_SHORT                          // valid
   ,  ENV_SWITCH_DESCRIPTION                    // valid
   ,  ENV_PACKED_WIDTH                          // packed only
   ,  ENV_ENVIRONMENT                           // special
   };

   // parse spec gives an interface by which we might be able to tell
   // parser what we actually want, and fail in specific cases (but not
   // necessarily for an unkown option).
   struct EnvParseSpec_S {
      virtual set<string> candidates(uint8 type) const = 0;
      
      virtual string longForm(string from)  const = 0;
      virtual string shortForm(string from) const = 0;

      virtual string description(uint8 type, string candidate) const = 0;
      virtual bool   option(uint8 type, string candidate)      const = 0;

      virtual bool accept(uint8 type, string candidate) const = 0;
      virtual bool reject(uint8 type, string candidate) const = 0;
   };
   CompactTypes(EnvParseSpec,_S); 

   struct EnvironmentBase_S {
      virtual ~EnvironmentBase_S();

      virtual const string banner(const bool colour = (TERM_ANSI && TERM_COLOUR)) const = 0;
      virtual const string help(const bool colour = (TERM_ANSI && TERM_COLOUR)) const = 0;

      virtual const vector<string>& getErrors() const = 0;

      virtual bool getSwitch(const string& what) const = 0;
      virtual bool optionExists(const string& what) const = 0;
      virtual const string& getOption(const string& what) const= 0;
      virtual bool variableExists(const string& what) const = 0;
      virtual const string& getVariable(const string& what) const = 0;
      virtual const vector<string>& getStrings() const = 0; 

      virtual const string getOS() const = 0;
      virtual const string getOSVersion() const= 0;
      virtual const string getGLVersion() const= 0;
   };
   CompactAbstractTypes(EnvironmentBase,_S);

   // Invocation parser. Handles switches, options, and environmental 
   // variables. 
   template <typename SPEC_T>
   class environmentInfo_C:
   public EnvironmentBase_S
   {
      typedef SPEC_T        Specification;
      
      string                invocation;

      set<string>           switches;
      map<string, string>   options;
      vector<string>        strings;

      map<string, string>   variables;

      vector<string>        errors;

      const string emptyString = "";

      void parseArgv(int argc, char** argv) {
         Specification spec;
         char ebuf[256] = {0};
         bool furtherOptions = true;

         invocation = argv[0];
         for (int i = 1; i < argc; i++) {
            cstr  c = argv[i];
            msize s = strlen(c);
            if (s == 1)
               strings.push_back(c);
            else if (s == 2) {
               if (!strcmp(c, "--")) // no more options (GNU convention)
                  furtherOptions = false;
               else if (furtherOptions && c[0] == '-') { // switch -or- short option
                  if (   spec.accept(ENV_SWITCH_SHORT, c+1) 
                     || !spec.reject(ENV_SWITCH_SHORT, c+1)
                     ) {
                     if (spec.option(ENV_SWITCH_SHORT, c+1) && (i+1) < argc){
                        options[spec.longForm(c+1)] = argv[i+1];
                        i++; // consume next
                     } else if (!spec.option(ENV_SWITCH_SHORT, c+1))
                        switches.emplace(spec.longForm( c+1));
                     else {
                        sprintf(ebuf, "Environment Error: Switch missing value - %s", c+1);
                        errors.push_back(ebuf);
                     }
                  } else if (spec.reject(ENV_SWITCH_SHORT, c+1)) {
                     sprintf(ebuf, "Environment Error: Unrecognized & Rejected switch - %s", c+1);
                     errors.push_back(ebuf);
                  }
               }
            } else if (furtherOptions && s > 2) {
               // extensive form options and switches

               cstr  value       = strchr(c+2, '=');
               msize lim         = 0;
               char  option[128] = {0};
               bool  consumeNext = false;

               if (!value && (i+1) < argc) { // value subsequent
                  value = argv[i+1];
                  lim = strlen(c+2);
                  consumeNext = true;
               } else if (value) // value embedded
                  lim = value - (c+2);
               else
                  lim = strlen(c+2);
               strncpy(option, c+2, lim);

               if (!strncmp(c, "--", 2)) {  // switch -or- long option
                  if (  spec.accept(ENV_SWITCH_LONG, option)
                     || !spec.reject(ENV_SWITCH_LONG, option)
                     ) {
                     if (spec.option(ENV_SWITCH_LONG, option) && value) {
                        options[option] = value+1;
                        if (consumeNext)
                           i++;
                     }
                     else if (!spec.option(ENV_SWITCH_LONG, option))
                        switches.emplace(option);
                     else {
                        sprintf(ebuf, "Environment Error: Option missing required value - %s", option);
                        errors.push_back(ebuf);
                     }

                  } else if (spec.reject(ENV_SWITCH_LONG, option)) {
                     sprintf(ebuf, "Environment Error: Unrecognized & Rejected long option - %s", option);
                     errors.push_back(ebuf);
                  }
               } else if (!strncmp(c, "-", 1)) { // extensive form switch
                  chr sw[2] = {*(c+1), 0};

                  if (!value || (value && value[0] != '=')) {
                     value = c+2;
                     lim = strlen(value);
                     consumeNext = false;
                  }

                  if (  spec.accept(ENV_SWITCH_SHORT, sw)
                     || !spec.reject(ENV_SWITCH_SHORT, sw)
                     ) {
                     if (spec.option(ENV_SWITCH_SHORT, sw) && value) {
                        options[spec.longForm(sw)] = value;
                        if (consumeNext)
                           i++;
                     } else if (spec.option(ENV_SWITCH_SHORT, sw)) {
                        sprintf(ebuf, "Environment Error: Switch missing required value - %s", sw);
                        errors.push_back(ebuf);
                     }
                  } else if (spec.reject(ENV_SWITCH_SHORT, sw)) {
                     sprintf(ebuf, "Environment Error: Unrecognized & Rejected switch - %s", sw);
                     errors.push_back(ebuf);
                  }
               } else {
                  strings.push_back(c);
               }
            } else if (!furtherOptions)
               strings.push_back(c);
         }
      }

      void parseEnv(char** env) {
         // TODO: Implement
         UNUSED(env);
      }

   public:
      environmentInfo_C(int argc, char** argv, char** env) {
         if (argc && argv)
            parseArgv(argc, argv);
         if (env && *env)
            parseEnv(env);
      }

      environmentInfo_C(int argc, char** argv) {
         if (argc && argv)
            parseArgv(argc, argv);
      }

#     define col(x) ((colour) ? (TERM_ANSI_CMDS[ANSI_##x]) : (""))
      const string banner(const bool colour = (TERM_ANSI && TERM_COLOUR)) const {
         using namespace std;

         stringstream        htxt;
         int                 termLength  = 80; // TODO: consult ENV_TERM_WIDTH var 
         string              desc;
         
         htxt << col(GREEN_BRIGHT);
         for (int i = 0; i < termLength; i++)
            htxt << "=";
         htxt << "\n";
         
         // invocation
         if ((termLength - invocation.length()) / 2 > 0)
            for (int i = 0; i < (termLength - (int)invocation.length()) / 2; i++)
               htxt << " ";
         htxt << col(BLUE_BRIGHT) << invocation << "\n";
         
         // os string
         desc = "OS: " + getOSVersion();
         if ((termLength - desc.length()) / 2 > 0)
            for (int i = 0; i < (termLength - (int)desc.length()) / 2; i++)
               htxt << " ";
         htxt << desc;

#        if E_OS == OS_LINUX
            // GLX string
            desc = "GLX Version: " + getGLXVersion();
            if ((termLength - desc.length()) / 2 > 0)
               for (int i = 0; i < (termLength - (int)desc.length()) / 2; i++)
                  htxt << " ";
            htxt << desc << "\n";
#        endif

         // GL string
         desc = "GL Version: " + getGLVersion();
         if ((termLength - desc.length()) / 2 > 0)
            for (int i = 0; i < (termLength - (int)desc.length()) / 2; i++)
               htxt << " ";
         htxt << desc << "\n";

         // build version
         desc = "Version: " E_BUILD_VERSION;
         if ((termLength - desc.length()) / 2 > 0)
            for (int i = 0; i < (termLength - (int)desc.length()) / 2; i++)
               htxt << " ";
         htxt << desc << "\n";

         // build date
         desc = "Built: " E_BUILD_DATE;
         if ((termLength - desc.length()) / 2 > 0)
            for (int i = 0; i < (termLength - (int)desc.length()) / 2; i++)
               htxt << " ";
         htxt << desc;


         // build version/date

         htxt << "\n" << col(GREEN_BRIGHT);
         for (int i = 0; i < termLength; i++)
            htxt << "=";

         htxt << col(RESET);

         return htxt.str();
      }
#     undef col

#     define col(x) ((colour) ? (TERM_ANSI_CMDS[ANSI_##x]) : (""))
      const string help(const bool colour = (TERM_ANSI && TERM_COLOUR)) const {
         using namespace std;

         Specification       spec;
         stringstream        htxt;
         int                 lenSwitches = 8;  // 8 for header
         const int           lenSplit    = 3;
         int                 termLength  = 80; // TODO: consult ENV_TERM_WIDTH var 
         map<string, string> form;
         set<string>         echoed;
         string              desc;
         int                 descLines;
         vector<string>      descBroken;

         // find length of left column (switches/options)
         for (auto i: spec.candidates(ENV_SWITCH_LONG))
            lenSwitches = ::util::math::max(lenSwitches, (int)(i.length() + lenSplit));

         // for each switch, get the opposite form and stash
         for (auto i: spec.candidates(ENV_SWITCH_LONG)) {
            string c = spec.shortForm(i);
            if (!c.compare(""))
               form[i] = i;
            else
               form[i] = c;
         }
         for (auto i: spec.candidates(ENV_SWITCH_SHORT)) {
            string c = spec.longForm(i);
            if (!c.compare(""))
               form[i] = i;
            else
               form[i] = c;
         }

         // Header
         htxt << right
              << string(col(GREEN_BRIGHT))
              << setw(lenSwitches) 
              << "Switches"
              << " | Description\n"
         ;
         for (int i = 0; i < termLength; i++)
            htxt << "-";

         // arguments
         for (auto i: form) {
            const string& j = i.first;

            if (echoed.find(j) != echoed.end())
               continue;
            echoed.emplace(i.first);
            if (i.first != i.second)
               echoed.emplace(i.second);

            htxt << "\n" << left 
                 << string(col(BLUE_BRIGHT))
                 << setw(lenSwitches) 
                 << ( ( (i.first != i.second)
                      ? (i.first + ", " + i.second)
                      : (i.first)
                      )
                    )
                 << string(col(GREEN_BRIGHT))
                 << " | "
            ;

            desc = spec.description(j.length() == 1 ? ENV_SWITCH_SHORT : ENV_SWITCH_LONG, j);
            descLines = desc.length() / (termLength - (lenSwitches + lenSplit));
            descBroken.clear();

            if (descLines > 1)
               while (desc.length() > 0) {
                  auto p = desc.begin();
                  p += (termLength - (lenSwitches + lenSplit));
                  if (p != desc.end()) {
                     while (!isspace(*p) && p != desc.begin())
                        p--;
                     if (p == desc.begin()) {
                        p += (termLength - (lenSwitches + lenSplit));
                        if (p >= desc.end())
                           p = desc.begin() 
                             + ::util::math::min( (int)(desc.length() + 1)
                                                , (termLength - (lenSwitches + lenSplit))
                                                );
                     }
                  }

                  descBroken.push_back(string(desc.begin(), p));
                  if (desc.end() != p)
                     desc.erase(desc.begin(), p > desc.end() ? desc.end() : p);
                  while (isspace(*desc.begin()))
                     desc.erase(desc.begin());
               }
            else
               descBroken.push_back(desc);

            msize c = 0;
            for (auto s: descBroken) {
               c++;
               htxt << string(col(YELLOW_BRIGHT))
                    << s 
                    ;
               if (c < descBroken.size())
                  htxt << string(col(RESET)) << "\n" 
                       << string(col(GREEN_BRIGHT)) 
                       << left << setw(lenSwitches) << "" << " | "
               ;
            }

            if (spec.option( i.first.length() == 1 
                           ? ENV_SWITCH_SHORT 
                           : ENV_SWITCH_LONG
                           , i.first.length() == 1
                           ? i.first
                           : i.second
                           ))
               htxt << string(col(RESET)) << "\n"
                    << left << setw(lenSwitches) << "" << " | "
                    << string(col(CYAN_BRIGHT))
                    << "(Requires value)"
                    ;
        }
        
        htxt << string(col(RESET));
        desc = htxt.str();

        return desc;
      }
#     undef col

      const vector<string>& getErrors() const {
         return errors;
      }

      bool getSwitch(const string& what) const {
         return switches.find(what) != switches.end();
      }

      bool optionExists(const string& what) const {
         return options.find(what) != options.end();
      }

      const string& getOption(const string& what) const {
         if (optionExists(what))
            return options.find(what)->second;
         return emptyString;
      }

      bool variableExists(const string& what) const {
         return variables.find(what) != options.end();
      }

      const string& getVariable(const string& what) const {
         if (variableExists(what))
            return variables.find(what)->second;
         return emptyString;
      }

      const vector<string>& getStrings() const {
         return strings;
      }

      const string getOS() const {
         switch (E_OS) {
         case OS_LINUX:
            return "linux";
         case OS_WINDOWS:
            return "windows";
         case OS_OSX:
            return "osx";
         default:
            return "unknown";
         }
      }

      const string getOSVersion() const {
         string out = "";
#        if E_OS == OS_LINUX
            char output[1024] = {0};
            int count = 0;
            FILE* proc = popen("uname -nrmo", "r");
            while ((count = read(fileno(proc), output, sizeof(output))) > 0)
               out += output;
            if (count <= 0) 
               pclose(proc);
            if (count < 0)
               out = "[Error  #[popen uname] IO]";
#        else
#           error "No OS version string available. Please implement!"
#        endif
         return out;
      }

#     if E_OS == OS_LINUX
      const string getGLXVersion() const {
         string out = "";
         int major, minor;
         char output[128];
         Display* dpy = XOpenDisplay(getenv("DISPLAY"));
         glXQueryVersion(dpy, &major, &minor);
         XCloseDisplay(dpy);
         sprintf(output, "%d.%d", major, minor);
         out += output;
         return out;
      }
#     endif

      const string getGLVersion() const {
         using ::config::opengl::GL_ATTRIB_MIN; 
         using ::config::opengl::GL_ATTRIB_MIN_SIZE; 


         string out = "";
#        if E_OS == OS_LINUX
            using ::config::opengl::VISUAL_ATTRIB_MIN; 
            using ::config::opengl::VISUAL_ATTRIB_MIN_SIZE; 
            using util::transaction::simpleLinear;
            
            typedef GLXContext (*glXCreateContextAttribsARBFunc)
                               ( Display*
                               , GLXFBConfig
                               , GLXContext
                               , Bool
                               , const int*
                               );

            custr verString = nullptr;
            simpleLinear tx(true, []( bool do_undo
                                    , simpleLinear::StepN step
                                    , simpleLinear::TransactionStep d
                                    , simpleLinear::TransactionStep u
                                    ) -> bool {
               UNUSED(d);
               UNUSED(u);
               fprintf( stderr
                      , "[Error getGLVersion #simpleLinear ~#[%s %d]]\n"
                      , do_undo ? "step" : "undo"
                      , step
                      );
               return true;
            });
            
            Display* dpy                 = nullptr; 
            int scrnum                   = 0;
            int elemc                    = 0;
            Window root                  = 0;
            Window tester                = 0;
            XVisualInfo* visinfo         = nullptr;
            GLXFBConfig* fbconfig        = nullptr;
            XSetWindowAttributes winattr;
            unsigned long mask           = 0;
            glXCreateContextAttribsARBFunc glXCreateContextAttribsARB = nullptr;
            GLXContext ctx               = 0;

            int vattr[VISUAL_ATTRIB_MIN_SIZE] = {0};
            int glattr[GL_ATTRIB_MIN_SIZE]    = {0};

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               memcpy(vattr , VISUAL_ATTRIB_MIN, VISUAL_ATTRIB_MIN_SIZE);
               memcpy(glattr, GL_ATTRIB_MIN    , GL_ATTRIB_MIN_SIZE);
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool { 
               dpy = XOpenDisplay(getenv("DISPLAY"));
               if (!dpy)
                  return false;
               scrnum = DefaultScreen(dpy);
               root = RootWindow(dpy, scrnum);
               if (!dpy || !root)
                  return false;
               return true;

            }, [&](bool,simpleLinear::StepN)->bool {
               if (dpy)
                  XCloseDisplay(dpy);
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               //assert(dpy && scrnum);
               fbconfig = glXChooseFBConfig(dpy, scrnum, vattr, &elemc);
               if (!fbconfig || !elemc) {
                  out += "[Error getGLVersion #glXChooseFBConfig nullptr]";
                  return false;
               }
               return true;

            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               assert(dpy && fbconfig);
               visinfo = glXGetVisualFromFBConfig(dpy, fbconfig[0]);
               if (!visinfo) {
                  out += "[Error getGLVersion #glXChooseVisual nullptr]";
                  return false;
               }
               return true;

            }, [&](bool,simpleLinear::StepN)->bool {
               if (visinfo)
                  XFree(visinfo);
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               assert(dpy && root && visinfo->visual);
               winattr.colormap = XCreateColormap(dpy, root, visinfo->visual, AllocNone);
               if (!winattr.colormap) {
                  out += "[Error getGLVersion #XCreateColormap nullptr]";
                  return false;
               }

               winattr.background_pixel = 0;
               winattr.border_pixel = 0;
               winattr.event_mask = StructureNotifyMask | ExposureMask | KeyPressMask;
               mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;
               return true;

            }, [&](bool,simpleLinear::StepN)->bool {
               if (winattr.colormap)
                  XFreeColormap(dpy, winattr.colormap);
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               assert(dpy && root && visinfo->depth && visinfo->visual && mask);
               tester = XCreateWindow(dpy, root
                                     , -1, -1, 640, 480
                                     , 0, visinfo->depth
                                     , InputOutput
                                     , visinfo->visual, mask, &winattr
                                     );
               if (!tester) {
                  out += "[Error getGLVersion #XCreateWindow nullptr]";
                  return false;
               }
               return true;

            }, [&](bool,simpleLinear::StepN)->bool {
               if (tester) 
                  XDestroyWindow(dpy, tester);
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               glXCreateContextAttribsARB = (glXCreateContextAttribsARBFunc)
                  glXGetProcAddressARB((custr)"glXCreateContextAttribsARB");
               if (!glXCreateContextAttribsARB) {
                  out += "[Error getGLVersion #glXGetProcAddressARB nullptr]";
                  return false;
               }
               return true;
            });

            tx.addStep([&](bool,simpleLinear::StepN)->bool {
               ctx = glXCreateContextAttribsARB(dpy, fbconfig[0], NULL, true, glattr);
               if (!ctx) {
                  out += "[Error getGLVersion #glCreateContextAttribsARB nullptr]";
                  return false;
               }
               glXMakeCurrent(dpy, tester, ctx);
               return true;
            }, [&](bool,simpleLinear::StepN)->bool {
               if (ctx)
                  glXDestroyContext(dpy, ctx);
               return true;
            });

            if (tx.transact()) {
               verString = glGetString(GL_VERSION);
               if (verString) {
                  out += (cstr)verString;
                  out += glXIsDirect(dpy, ctx) ? " (Direct Rendering)"
                                               : " (No Direct Rendering)"
                                               ;
               }
               else
                  out += "[Error getGLVersion #glGetString nullptr]";
               assert(tx.inverted());
               tx.transact();
            }
#        else 
#           error "No GL version string available. Please implement!"
#        endif
         return out;
      }

   };
   //template<typename SPEC_T> using EnvironmentParse = EnvironmentParse_C<SPEC_T>&;
   CompactTemplates(environmentInfo, _C, typename, SPEC_T);


}}

#endif

