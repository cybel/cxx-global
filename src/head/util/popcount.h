#ifndef __UTIL_POPCOUNT_H__
#define __UTIL_POPCOUNT_H__

#include "head/stdtyp.h"

namespace util {
   alwaysinline constexpr_ uint8 popcount(uint64 i) {
#     define PC_A (i - ((i >> 1) & 0x55555555))
#     define PC_B ((PC_A & 0x33333333) + ((PC_A >> 2) & 0x33333333))
#     define PC_C ((((PC_B + (PC_B >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24)
      //i = i - ((i >> 1) & 0x55555555);
      //i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
      //return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
      return (uint8) PC_C;
#     undef PC_A
#     undef PC_B
#     undef PC_C
   }

   template<uint64 x>
   struct Popcount {
      static const uint64 value = (x & 1) + Popcount<(x >> 1)>::value;
   };

   template<>
   struct Popcount < 0 > {
      static const uint64 value = 0;
   };

}

#endif
