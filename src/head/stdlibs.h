#ifndef __COMMON_STDHEAD_H__
#define __COMMON_STDHEAD_H__

#include "head/platform.h"

#include <cstddef>
#include <cassert>
#include <cstdarg>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdio>

#if E_COMPILER==COMP_MSVC
#  include <intrin.h>
#endif

#include <limits>

#include <type_traits>
#include <memory>
#include <new>
#include <functional>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <utility>

#include <locale>
#include <codecvt>

#if E_COMPILER == COMP_MSVC
#  pragma warning(push)
#  pragma warning(disable:4265)
#endif
#include <mutex>
#include <shared_mutex>
#if E_COMPILER == COMP_MSVC
#  pragma warning(pop)
#endif
#include <thread>
#include <atomic>

#include <array>
#include <tuple>
#include <vector>
#include <deque>
#include <tuple>
#include <list>
#include <forward_list>
#include <initializer_list>
#include <map>
#include <set>
#include <unordered_set>

#include <queue>
#include <stack>

#include <algorithm>
#include <iterator>

#include <experimental/filesystem>

#include <random>

#ifdef min
#  undef min
#endif
#ifdef max
#  undef max
#endif

#endif
