#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <limits>

#include <chrono>

#include "head/platform.h"
#include "head/stddef.h"
#include "head/stdtyp.h"

#include "head/opengl/opengl.h"

#define EXPOSE_T(x)    CompactTypes(x,T)
#define EXPOSE_N(t,x)  constexpr t x  = x##N
#define EXPOSE_CT(t,x) constexpr t &x = x##_C

namespace config {
   /////////////////////////////////////////////////////////////////////
   ////// Platform Conventions //
   /////////////////////////////

   constexpr cstr NEWLINE = "\n";

   namespace os {
      enum : int
      {  EXIT_OK                  =    0
      ,  EXIT_INT                 =    1
      ,  EXIT_INIT_EXCEPTION      =  -10
      ,  EXIT_INIT_EARLY          =  -20
      ,  EXIT_INIT_LATE           =  -30
      ,  EXIT_LIB_EXCEPTION       =  -40
      ,  EXIT_LIB_BOOST_EXCEPTION =  -41
      ,  EXIT_COTHREAD_RETURNED   =  -50
      };
   }

   namespace linux {

   }

   /////////////////////////////////////////////////////////////////////
   ////// Threading config //
   /////////////////////////

   namespace threads {
   namespace bits {
      // 512K
      constexpr unsigned int CO_HEAPSIZE_DEFAULT = (1024)*512;
   }}

   namespace threads {
      using std::literals::chrono_literals::operator""us;
      constexpr std::chrono::microseconds SLEEP_BASE     = 100us;
      constexpr real32                    SLEEP_GROW     = 3.f;
      constexpr std::chrono::microseconds SLEEP_CAP      = (std::chrono::seconds)2;
      constexpr std::chrono::microseconds SLEEP_STARVING = (std::chrono::milliseconds)500;
      constexpr unsigned int CO_HEAPSIZE_RUNNER = (1024)*32;
   }

   /////////////////////////////////////////////////////////////////////
   ////// Logging Config //
   ///////////////////////

   namespace logging {
      constexpr uint16 LEVEL_DEFAULT = ((7<<13) | (3<<11));
      constexpr std::chrono::microseconds PUMP_PERIOD_DEFAULT = std::chrono::microseconds(500);
      constexpr msize PUMP_QUEUE_COUNT = 16;
   }

   namespace logging {
   namespace message {
      constexpr msize  MESSAGE_COLLECTION_SIZE = 64;
      constexpr sint16 BIAS_MIN = -5;
      constexpr sint16 BIAS_MAX = 5;
      constexpr sint16 FLOW_SHRINK = -16;
      constexpr msize  MESSAGE_LINES = 10;
      constexpr msize  MESSAGE_LINE_BUFFER = 256;
      constexpr msize  MESSAGE_LINE_LENGTH = 255;
   }}

   namespace logging {
   namespace filter  {
      constexpr cstr CLASS_NAMES[] =
      { "(unknown)"
      , "error"      
      , "warning"
      , "notification"
      , "debug"
      , "(user-5)"
      , "(user-6)"
      , "(user-7)"
      };

      constexpr cstr LEVEL_NAMES[] =
      { "(unknown)"
      , "critical"
      , "error"
      , "exception"  // Error
      , "warning"
      , "warning"
      , "warning"    // Warning
      , "note"
      , "info"
      , "verbose"    // Notification
      , "debug"
      , "trace"
      , "trace"      // Debug
      , "(user-13)"
      , "(user-14)"
      , "(user-15)"  // User-5
      , "(user-16)"
      , "(user-17)"
      , "(user-18)"  // User-6
      , "(user-19)"
      , "(user-20)"
      , "(user-21)"  // User-7
      };
   }}

   namespace logging {
   namespace location {
      // import random; hex(random.randint(0,2**64))
      constexpr uint32 TOPIC_BASE = 0x2942c934LL;

      constexpr cstr NATURE_NAMES[] =
      {  "unknown"
      ,  "internal"
      ,  "extension"
      ,  "program"
      ,  "file-source"
      ,  "file-header"
      ,  "group-owner"
      ,  "group-member"
      ,  "namespace-named"
      ,  "namespace-anon"
      ,  "class"
      ,  "struct"
      ,  "union"      
      ,  "function-c"
      ,  "function-c++" 
      ,  "function-c-callback"
      ,  "function-c++-callback"
      ,  "function-bare"   
      ,  "function-anon"
      ,  "function-lambda"
      ,  "function-static"
      ,  "function-member"
      ,  "function-other"
      ,  "library-injected"
      ,  "library-edge"
      ,  "system-os"
      ,  "system-gl"
      ,  "system-other"
      ,  "object-singleton"
      ,  "object-instance"
      ,  "object-flyweight-fly"    // unique 
      ,  "object-flyweight-weight" // common
      };
   }}

   namespace logging {
   namespace format {
      constexpr uint8 FORMAT_TOPIC_CONTEXT = 3;
      /*
       * 1 - Class         p(MCLASS); 
       * 2 - Level         p(MLEVEL);
       * 3 - Source        p(SOURCE); 
       * 4 - Scope         p(SCOPE);
       * 5 - Topic         p(TOPIC); 
       * 6 - Time          p(TIME); 
       * 7 - Wall clock    p(WALLCLOCK);
       * 8 - Message       p(MESSAGE);
       * 9 - Thread ID     p(THREAD);
       */
      constexpr cstr DEFAULT_FORMAT = ">>> @%6%/{%7%} %2%:%1%\n>>> [#[%3% %4%]: %5% @%9%]\n%8%";
      constexpr cstr TIME_FORMAT = "%d/%m/%y %T";
   }}


   /////////////////////////////////////////////////////////////////////
   ////// OpenGL Config //
   //////////////////////

   namespace util {
   namespace res  {
      typedef sint16 TransactionStepN;
      constexpr TransactionStepN TRANSACTION_STEP_N_MAX = std::numeric_limits<TransactionStepN>::max();
   }}

   namespace opengl {
#  if E_OS == OS_LINUX
      constexpr int VISUAL_ATTRIB_MIN[] = 
      {  GLX_RED_SIZE     , 7 // want 24-bit with alpha minimum!
      ,  GLX_GREEN_SIZE   , 7
      ,  GLX_BLUE_SIZE    , 7
      ,  GLX_ALPHA_SIZE   , 3
      ,  GLX_DEPTH_SIZE   , 24
      ,  GLX_STENCIL_SIZE , 8
      ,  GLX_X_RENDERABLE , True             // must be renderable
      ,  GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT   // render to window
      ,  GLX_RENDER_TYPE  , GLX_RGBA_BIT     // not index
      ,  GLX_CONFIG_CAVEAT, GLX_NONE         // no caveats
      ,  GLX_DOUBLEBUFFER , True             // double buffering
      ,  None
      };
      constexpr msize VISUAL_ATTRIB_MIN_SIZE = sizeof(VISUAL_ATTRIB_MIN);
#   endif

      // https://www.khronos.org/opengl/wiki/Version#OpenGL_3.3_.282010.29
      constexpr int GL_ATTRIB_MIN[] = 
         {  GLX_CONTEXT_MAJOR_VERSION_ARB, 4
         ,  GLX_CONTEXT_MINOR_VERSION_ARB, 3
         ,  GLX_CONTEXT_PROFILE_MASK_ARB , GLX_CONTEXT_CORE_PROFILE_BIT_ARB 
                                         
         ,  GLX_CONTEXT_FLAGS_ARB        , GLX_CONTEXT_DEBUG_BIT_ARB  
                                         | GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 
                                         | GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB
         ,  None
         };
      constexpr msize GL_ATTRIB_MIN_SIZE = sizeof(GL_ATTRIB_MIN);
   }

   /////////////////////////////////////////////////////////////////////
   ////// environment-info.h //
   ///////////////////////////

   namespace util {
   namespace env  {
      constexpr bool TERM_ANSI   = true;
      constexpr bool TERM_COLOUR = true;

#     define ESC "\x1B["
      constexpr cstr TERM_ANSI_CMDS[] = 
#     define COL "m"
      // Foreground
      { ESC  "0"   COL // 0
      , ESC  "1"   COL
      , ESC  "2"   COL
      , ESC  "3"   COL
      , ESC  "4"   COL
      , ESC  "5"   COL
      , ESC  "6"   COL
      , ESC  "7"   COL
      , ESC  "8"   COL
      , ESC "30"   COL // 9
      , ESC "31"   COL
      , ESC "32"   COL
      , ESC "33"   COL
      , ESC "34"   COL
      , ESC "35"   COL
      , ESC "36"   COL
      , ESC "37"   COL
      , ESC "30;1" COL
      , ESC "31;1" COL
      , ESC "32;1" COL
      , ESC "33;1" COL
      , ESC "34;1" COL
      , ESC "35;1" COL
      , ESC "36;1" COL
      , ESC "37;1" COL
      // Background
      , ESC "40"   COL
      , ESC "41"   COL
      , ESC "42"   COL
      , ESC "43"   COL
      , ESC "44"   COL
      , ESC "45"   COL
      , ESC "46"   COL
      , ESC "47"   COL
      , ESC "100"  COL
      , ESC "101"  COL
      , ESC "102"  COL
      , ESC "103"  COL
      , ESC "104"  COL
      , ESC "105"  COL
      , ESC "106"  COL
      , ESC "107"  COL
#     undef COL
      };
#     undef ESC


      enum : uint16
      // Attributes
      {  ANSI_RESET = 0
      ,  ANSI_BRIGHT 
      ,  ANSI_DIM
      ,  ANSI_UNDERSCORE
      ,  ANSI_BLINK
      ,  ANSI_REVERSE
      ,  ANSI_HIDDEN
      // Foreground
      ,  ANSI_BLACK = 9
      ,  ANSI_RED
      ,  ANSI_GREEN
      ,  ANSI_YELLOW
      ,  ANSI_BLUE
      ,  ANSI_MAGENTA
      ,  ANSI_CYAN
      ,  ANSI_WHITE
      ,  ANSI_BLACK_BRIGHT
      ,  ANSI_RED_BRIGHT
      ,  ANSI_GREEN_BRIGHT
      ,  ANSI_YELLOW_BRIGHT
      ,  ANSI_BLUE_BRIGHT
      ,  ANSI_MAGENTA_BRIGHT
      ,  ANSI_CYAN_BRIGHT
      ,  ANSI_WHITE_BRIGHT
      // Background
      ,  ANSI_BLACK_BG
      ,  ANSI_RED_BG
      ,  ANSI_GREEN_BG
      ,  ANSI_YELLOW_BG
      ,  ANSI_BLUE_BG
      ,  ANSI_MAGENTA_BG
      ,  ANSI_CYAN_BG
      ,  ANSI_WHITE_BG
      ,  ANSI_BLACK_BRIGHT_BG
      ,  ANSI_RED_BRIGHT_BG
      ,  ANSI_GREEN_BRIGHT_BG
      ,  ANSI_YELLOW_BRIGHT_BG
      ,  ANSI_BLUE_BRIGHT_BG
      ,  ANSI_MAGENTA_BRIGHT_BG
      ,  ANSI_CYAN_BRIGHT_BG
      ,  ANSI_WHITE_BRIGHT_BG
      };
   }}

}

#endif
