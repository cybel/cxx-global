#ifndef __LOG_LOGGER_H__
#define __LOG_LOGGER_H__

#include "head/log/topics.h"
#include "head/log/scopes.h"
#include "head/log/levels.h"

#include "head/log/destination.h"
#include "head/log/message.h"
#include "head/log/formats.h"

namespace logging {
   using namespace ::config::logging;
   using destination::DestinationPtr;
   using destination::DestinationRef;
   using std::atomic;
   using std::chrono::microseconds;
   using std::conditional_t;
   using std::enable_if_t;
   using std::function;
   using std::is_floating_point;
   using std::is_integral;
   using std::is_signed;
   using std::is_unsigned;
   using std::stack;
   using std::thread;
   using std::queue;
   using std::string;
   using namespace logging::destination;
   using namespace logging::filter;
   using namespace logging::format;
   using namespace logging::location;
   using namespace logging::message;

   /////////////////////////////////////////////////////////////////////
   ////// Logger Modes // 
   /////////////////////

   enum struct LogMode : uint8
   {  ROOT_LEVEL
   ,  FILE_LEVEL
   ,  LEAF_LEVEL
   };


   /////////////////////////////////////////////////////////////////////
   ////// Logger State // (opaque)
   /////////////////////

   struct loggerState_S;
   CompactTypes(loggerState,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Logger // (forward)
   ///////////////

   struct logger_S;   
   CompactTypes(logger,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Numerics Base //
   //////////////////////

   enum NumericFormat: uint8
   {  NF_DEC
   ,  NF_SCI
   ,  NF_HEX
   ,  NF_PTR
   ,  NF_BIN
   ,  NF_OCT
   };


   namespace bits {
#     define ISA(x, y)                        \
         template <>                          \
         struct IntSizeArg<x> {       \
            static constexpr_ cstr value = y; \
         }
      // helpers
      template < typename T
               , typename = conditional_t<is_integral      <T>::value, T
                          , conditional_t<is_floating_point<T>::value, T
                          , void>>
               >
      struct IntSizeArg {};

      ISA(uint8 , "hh");
      ISA(sint8 , "hh");
      ISA(uint16, "h" );
      ISA(sint16, "h" );
      ISA(uint32, ""  );
      ISA(sint32, ""  );
      ISA(uint64, "ll");
      ISA(sint64, "ll");
      ISA(real32, ""  );
      ISA(real64, "l" );
      ISA(rptr,   ""  );
//      ISA(msize,  "z" );
//      ISA(dptr,   "t" );
#     undef ISA

      cstr numericFormatStrBase( str           fs
                               , cstrc         sizeMod
                               , bool          Signed
                               , bool          Floating
                               , NumericFormat fmt          
                               , uint8         width        
                               , sint8         precision    
                               , bool          zeroPad      
                               , bool          positiveSign 
                               , bool          spacedSign   
                               , bool          leftJustify  
                               );

      template < typename T
               , typename      = enable_if_t<  is_integral<T>::value
                                            || is_floating_point<T>::value
                                            >
               , bool Floating =  is_floating_point<T>::value 
               , bool Signed   =  is_floating_point<T>::value || is_signed<T>::value
               , bool Unsigned = !is_floating_point<T>::value && is_unsigned<T>::value
               >
      alwaysinline
      cstr numericFormatStr( str           fs
                           , NumericFormat fmt          
                           , uint8         width        
                           , sint8         precision    
                           , bool          zeroPad      
                           , bool          positiveSign 
                           , bool          spacedSign   
                           , bool          leftJustify  
                           ) {
            return numericFormatStrBase( fs
                                       , bits::IntSizeArg<T>::value
                                       , Signed
                                       , Floating
                                       , fmt
                                       , width, precision, zeroPad
                                       , positiveSign, spacedSign
                                       , leftJustify
                                       );

         return fs;
      }
      
      template < NumericFormat fmt          = NF_DEC
               , uint8         width        = 0
               , sint8         precision    = -1
               , bool          zeroPad      = false
               , bool          positiveSign = false
               , bool          spacedSign   = false
               , bool          leftJustify  = false
               , typename T
               , typename      = enable_if_t<  is_integral<T>::value
                                            || is_floating_point<T>::value
                                            >
               , bool Floating =  is_floating_point<T>::value 
               , bool Signed   =  is_floating_point<T>::value || is_signed<T>::value
               , bool Unsigned = !is_floating_point<T>::value && is_unsigned<T>::value
               >
      cstrc numericFormat(str buff, msize buffLen, const T& v) {
         static chr fs[16] = {0};
         
         if (fs[0] != '%') {
            numericFormatStr<T>( fs
                               , fmt
                               , width, precision, zeroPad
                               , positiveSign, spacedSign
                               , leftJustify
                               );
         }

         snprintf(buff, buffLen, fs, v);
         return buff;
      }

   }

   /////////////////////////////////////////////////////////////////////
   ////// Logger // 
   ///////////////

   struct logger_S {
      // Mode: Root
      logger_S( DestinationRef dest
              , FormatterRef format
              , cstr programName
              , cstr programPath
              , MLevelNT level = LEVEL_DEFAULT
              , microseconds pumpPeriod = PUMP_PERIOD_DEFAULT
              );

      // Mode: File
      logger_S( logger_S& root
              , sourceRef source, SourceNature nature, cstr filePath
              , MLevelNT level = (MLevelNT)MLevel::MAX_VALUE__
              );

      // Mode: Leaf
      logger_S( logger_S& file
              , SourceNature nature, cstr path
              , MLevelNT level = (MLevelNT)MLevel::MAX_VALUE__
              );

      virtual ~logger_S();

      // get info
      LogMode    getMode()   const;
      loggerPtr  getParent() const;
      MLevelNT   getLevel()  const;
      sourceRef  getSource();
      cscopeRef  getScope()  const;
      topicPtr   getTopic();

      // modes
      loggerRef numericFormat(NumericFormat nf = NF_DEC);
      loggerRef numericWidth(uint8 w = 0);
      loggerRef numericPrecision(uint8 p = -1);
      loggerRef numericZeroPad(bool p = false);
      loggerRef numericSpacePad(bool p = false);
      loggerRef numericPositiveSign(bool s = false);
      loggerRef numericJustifyLeft(bool l = false);

      // topics
      loggerRef begin(cstrc name);
      loggerRef conclude();

      // scopes
      loggerRef enter(SourceNature nature, cstrc path);
      loggerRef exit();

      // levels
      loggerRef setLevel(MLevel level);

      // push
      loggerRef log(cstr msg); 
      loggerRef log(chr c);
      loggerRef log();  // flush line

      alwaysinline
      loggerRef log(const string& msg) {
         return log(msg.c_str());
      }

      template < NumericFormat fmt
               , uint8         width        = 0
               , sint8         precision    = -1
               , bool          zeroPad      = false
               , bool          positiveSign = false
               , bool          spacedSign   = false
               , bool          leftJustify  = false
               , typename T
               , typename      = enable_if_t<  is_integral<T>::value
                                            || is_floating_point<T>::value
                                            >
               , bool Floating =  is_floating_point<T>::value 
               , bool Signed   =  is_floating_point<T>::value || is_signed<T>::value
               , bool Unsigned = !is_floating_point<T>::value && is_unsigned<T>::value
               >
      alwaysinline
      loggerRef log(T v) {
         chr buff[70] = {0};
         bits::numericFormat< fmt
                            , width, precision, zeroPad
                            , positiveSign, spacedSign
                            , leftJustify
                            >(buff, sizeof(buff), v);
         return log(buff);
      }

      template < typename T
               , typename      = enable_if_t<  is_integral<T>::value
                                            || is_floating_point<T>::value
                                            >
               , bool Floating =  is_floating_point<T>::value 
               , bool Signed   =  is_floating_point<T>::value || is_signed<T>::value
               , bool Unsigned = !is_floating_point<T>::value && is_unsigned<T>::value
               >
      alwaysinline
      loggerRef log(T v) {
         chr buff[70] = {0};
         numericModeActualize( bits::IntSizeArg<T>::value
                             , is_floating_point<T>::value || is_signed<T>::value
                             , is_floating_point<T>::value 
                             );
         snprintf(buff, sizeof(buff), nFormat, v);
         return log(buff);
      }

      template <typename T>
      alwaysinline
      loggerRef log(const T* x) {
         NumericFormat onf = nFmt;
         uint8 ow = nWidth;
         bool oz = nZeroPad;

         numericFormat(NF_HEX);
         numericWidth(sizeof(void*)*2);
         numericZeroPad(true);
         log("0x");
         log((uptr)x);
         numericFormat(onf);
         numericWidth(ow);
         numericZeroPad(oz);

         return *this;
      }

   private:
      topicCollectionPtr getTopics();

      // immediate mode
      bool logImmediateBegin();
      void logImmediateFlush();

      void numericModeInvalidate();
      void numericModeActualize(cstrc sizeMod, bool Signed, bool Floating);

      bool filterMessage(cmessagePtr msg);
      void logPush(messagePtr msg);
      void logPump(microseconds pumpPeriod);


      //////////////////////////////////////////////////////////////////
      ////// Relational //
      ///////////////////
   
      LogMode   mode;

      // Root ONLY
      loggerStatePtr state;

      // Non-root only
      loggerPtr parent;
     
      //////////////////////////////////////////////////////////////////
      ////// Log State & Location //
      /////////////////////////////

      // location
      sourcePtr       src;
      stack<scope>    scp;
      stack<topicPtr> tpc;
      bool topicInherit;
      
      // filtering
      MLevelNT  level;

      // message
      messagePtr active;


      //////////////////////////////////////////////////////////////////
      ////// Direct Invocation Controls //
      ///////////////////////////////////

      // formatting
      chr           nFormat[16]   = {0};
      chr           nSizeMod[3]   = {0};
      NumericFormat nFmt          = NF_DEC;
      bool          nSigned       = false;
      bool          nFloating     = false;
      uint8         nWidth        = 0;
      sint8         nPrecision    = -1;
      bool          nZeroPad      = false;
      bool          nSpacedSign   = false;
      bool          nPositiveSign = false;
      bool          nLeftJustify  = false;
   };


   /////////////////////////////////////////////////////////////////////
   ////// Logger Inserters //
   /////////////////////////

   typedef loggerRef(*LoggerManipulator)(loggerRef);
   typedef function<loggerRef(loggerRef)> LoggerManipulatorFunc;

   alwaysinline
   loggerRef operator<<(loggerRef l, cstr s) {
      return l.log(s);
   }

   alwaysinline
   loggerRef operator<<(loggerRef l, chr c) {
      return l.log(c);
   }

   alwaysinline
   loggerRef operator<<(loggerRef l, const string& s) {
      return l.log(s.c_str());
   }

   template <typename T>
   alwaysinline
   loggerRef operator<<(loggerRef l, const T& x) {
      return l.log(x);
   }

   template <typename T>
   alwaysinline
   loggerRef operator<<(loggerRef l, const T* x) {
      return l.log(x);
   }

   alwaysinline
   loggerRef operator<<(loggerRef l, LoggerManipulator func) {
      return func(l);
   }

   alwaysinline
   loggerRef operator<<(loggerRef l, LoggerManipulatorFunc func) {
      return func(l);
   }

   alwaysinline
   loggerRef operator<<(loggerRef l, filter::MLevel level) {
      return l.setLevel(level);
   }

   alwaysinline
   loggerRef endm(loggerRef l) {
      return l.log();
   }

#  define M(name, atype, def, calls)            \
      alwaysinline                              \
      LoggerManipulatorFunc name(atype v = def) {     \
         return [v](loggerRef l) -> loggerRef { \
            l.calls(v);                         \
            return l;                           \
         };                                     \
      }

   M(nformat      , NumericFormat, NF_DEC, numericFormat      )
   M(width        , uint8        , -1    , numericWidth       )
   M(precision    , uint8        ,  0    , numericPrecision   )
   M(padzero      , bool         , false , numericZeroPad     )
   M(padspace     , bool         , false , numericSpacePad    )
   M(possign      , bool         , false , numericPositiveSign)
   M(ljustify     , bool         , false , numericJustifyLeft )

#  undef M

   /////////////////////////////////////////////////////////////////////
   ////// Logger Utilities //
   /////////////////////////

#  define defineRootLogger(_outputter, _formatter, name, path)        \
      namespace __logging {                                           \
         using namespace logging;                                     \
         using namespace logging::destination;                        \
         using namespace logging::format;                             \
         _outputter __rootProgramOutput;                              \
         _formatter __rootProgramFormat;                              \
         logger __rootLogger( __rootProgramOutput, __rootProgramFormat\
                            , name, path                              \
                            );                                        \
         DestinationRef rootProgramOutputRef(__rootProgramOutput);    \
         FormatterRef   rootProgramFormatRef(__rootProgramFormat);    \
         loggerRef      rootLoggerRef(__rootLogger);                  \
      }

#  define defineFileLogger(source)                                \
      namespace __logging {                                       \
         using namespace logging;                                 \
         extern logger __rootLogger;                              \
      }                                                           \
      namespace {                                                 \
         using namespace logging;                                 \
         logger __log_file( __logging::__rootLogger               \
                          , source                                \
                          , SourceNature::FILE_SOURCE, __FILE__   \
                          );                                      \
      }

#  define referenceRootLog()                         \
      namespace __logging {                          \
         using namespace logging;                    \
         using namespace logging::destination;       \
         using namespace logging::format;            \
         extern DestinationRef rootProgramOutputRef; \
         extern FormatterRef   rootProgramFormatRef; \
         extern loggerRef      rootLoggerRef;        \
      }

#  define rootLoggerDestOpen(target, what)               \
      DestinationTargetedPtr<target> rout = dynamic_cast \
         <DestinationTargetedPtr<target>>(&rootOutput());\
      if (rout)                                          \
         rout->open(what)

#  define fileLog()    __log_file
#  define rootOutput() __logging::rootProgramOutputRef
#  define rootFormat() __logging::rootProgramFormatRef
#  define rootLogger() __logging::rootLoggerRef
#  define funcLog(...) \
      logging::logger lout(fileLog(), __VA_ARGS__)
}



#endif
