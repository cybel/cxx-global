#ifndef __LOG_MESSAGE_H__
#define __LOG_MESSAGE_H__

#include "head/util/bitfield.h"

#include "head/log/levels.h"
#include "head/log/scopes.h"
#include "head/log/topics.h"

#include "head/libco.h"

namespace logging {
namespace message {
   using namespace ::config::logging::message;

   using logging::filter::MLevel;
   using logging::filter::MLevelNT;
   using logging::location::sourcePtr;
   using logging::location::SourceNature;
   using logging::location::ctopicPtr;
   using std::queue;
   using std::string;
   using std::thread;
   using namespace util::bitfield;

   struct messageBank_S;
   CompactTypes(messageBank,_S);

   struct message_S {
      ProgramMoment time;

      thread::id threadId;
      cothread_t cothread;
      
      union {
         MLevelNT levelN;
         MLevel   level;
      };

      struct {
         sourcePtr    source;
         SourceNature nature;
         cstr         path;
      } origin;

      ctopicPtr topic;

      msize lines;
      chr   line[MESSAGE_LINES][MESSAGE_LINE_BUFFER];

      DEBUG_GUARD(0);
      messageBankPtr bank;
      msize          bankItem;
      DEBUG_GUARD(1);
   };
   CompactTypes(message,_S);

   messagePtr getMessage();
   void returnMessage(messageCPtr msg);

}}

#endif
