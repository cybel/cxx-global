#ifndef __LOG_SCOPES_H__
#define __LOG_SCOPES_H__

namespace logging {
namespace location {
   using namespace ::config::logging::location;
   using boost::shared_mutex;
   using std::function;
   using std::set;
   using std::string;
   using util::math::sclamp;


   /////////////////////////////////////////////////////////////////////
   ////// Source natures //
   ///////////////////////

   enum struct SourceNature: uint8
   {  UNKNOWN = 0
   ,  INTERNAL
   ,  EXTENSION

   ,  PROGRAM
   ,  FILE_SOURCE 
   ,  FILE_HEADER
   ,  GROUP_OWNER
   ,  GROUP_CHILD
   ,  NAMESPACE_NAMED
   ,  NAMESPACE_ANON
   ,  CLASS
   ,  STRUCT      
   ,  UNION      
   ,  FUNC_C
   ,  FUNC_CXX 
   ,  FUNC_C_CALLBACK
   ,  FUNC_CXX_CALLBACK
   ,  FUNC_BARE   
   ,  FUNC_ANON
   ,  FUNC_LAMBDA
   ,  FUNC_STATIC
   ,  FUNC_MEMBER
   ,  FUNC_OTHER
   ,  LIBRARY_INJECTED
   ,  LIBRARY_EDGE
   ,  SYSTEM_OS
   ,  SYSTEM_GL
   ,  SYSTEM_OTHER
   ,  OBJECT_SINGLETON
   ,  OBJECT_INSTANCE
   ,  OBJECT_FLYWEIGHT_FLY    // unique 
   ,  OBJECT_FLYWEIGHT_WEIGHT // common
   };

   template<typename T>
   alwaysinline 
   cstr sourceNatureS(T nature) {
      nature = sclamp(nature, 0, sizeof(NATURE_NAMES) / sizeof(cstr));
      return NATURE_NAMES[nature];
   }

   /////////////////////////////////////////////////////////////////////
   ////// Source scope //
   /////////////////////

   struct scope_S {
      SourceNature   nature;
      cstr           path;
   };
   CompactTypes(scope,_S);

   /////////////////////////////////////////////////////////////////////
   ////// Source state // (opaque)
   /////////////////////

   struct sourceState_S;
   CompactTypes(sourceState,_S);

   /////////////////////////////////////////////////////////////////////
   ////// Source  //
   ////////////////

   typedef function<void(SourceNature, cstr)> EnscopeFunc;
   typedef function<void(const EnscopeFunc)>  EnscopeInitFunc;

   struct source_S {
      const uint64 id;
      cstrc        name;
      
      source_S(cstr name, const EnscopeInitFunc init);
      ~source_S();

      bool             contains(SourceNature nature) const;
      bool             contains(SourceNature nature, cstr path) const;
      const set<cstr>& pathsFor(SourceNature nature) const;

      bool enscope(SourceNature nature, cstr path);

      // mute
      bool silence(bool nowMuted = true);

      // muted
      bool     mute() const;
      operator bool() const;

      // name
      operator string() const;
      operator cstr() const;

   private:
      bool           muted;
      sourceStatePtr state;

   };
   CompactTypes(source,_S);

#  define scoped(nature, path) \
      ::logging::location::SourceNature::nature, path

// define one scope
#  define enscopeSrc(nature, path) \
      __enscope(SourceNature::nature, path)

// define
#  define fileScopes(name, ...)                    \
      namespace {                                  \
         using ::logging::location::source;        \
         using ::logging::location::EnscopeFunc;   \
         using ::logging::location::SourceNature;  \
         source __log_source_file                  \
            (#name, [](EnscopeFunc __enscope) {    \
               enscopeSrc(FILE_SOURCE, __FILE__)   \
               __VA_ARGS__;                        \
            });                                    \
      }                                            \
      namespace __log_scopes {                     \
         using ::logging::location::sourcePtr;     \
         sourcePtr getScopes_##name() {            \
            return &__log_source_file;             \
         }                                         \
      }
         

// use
#  define fileScope() \
      __log_source_file

// define

#  define groupScopesRegisterBody(group, name, ...)\
      [](EnscopeFunc __enscope) {                  \
         enscopeSrc(GROUP_OWNER, __FILE__);        \
         enscopeSrc(FILE_SOURCE, __FILE__)         \
         __VA_ARGS__;                              \
      }  

#  define groupScopesRegister(group, name, ...)                    \
      struct __log_source_##group##_##name##_register {            \
      __log_source_##group##_##name##_register() {                 \
         if (!__log_source_##group)                                \
            __log_source_##group = new source(#group,              \
               (groupScopesRegisterBody(group, name, __VA_ARGS__)) \
            );                                                     \
         else                                                      \
            (groupScopesRegisterBody(group, name, __VA_ARGS__))    \
            ([](SourceNature src, cstr path) {                     \
               __log_source_##group->enscope(src,path);            \
            });                                                    \
      }} __log_source_##group##_##name##_register_inst  
      

#  define groupScopesOwner(group, name, ...)                \
      namespace __log_scopes {                              \
         using ::logging::location::source;                 \
         using ::logging::location::sourcePtr;              \
         using ::logging::location::EnscopeFunc;            \
         using ::logging::location::SourceNature;           \
         sourcePtr __log_source_file = nullptr;             \
         sourcePtr __log_source_##group() {                 \
            static source groupOwner( #group                \
                                    , (groupScopesRegisterBody(group, name, __VA_ARGS__)) \
                                    );                      \
            __log_source_file = &groupOwner;                \
            return &groupOwner;                             \
         }                                                  \
         struct __log_source_##group##_##name##_register {  \
            __log_source_##group##_##name##_register() {    \
               __log_source_##group();                      \
            }                                               \
         } __log_source_##group##_##name##_register_inst;   \
      }

#  define groupScopesMember(group, name, ...)                      \
      namespace __log_scopes {                                     \
         using ::logging::location::source;                        \
         using ::logging::location::sourcePtr;                     \
         using ::logging::location::EnscopeFunc;                   \
         using ::logging::location::SourceNature;                  \
         sourcePtr __log_source_##group();                         \
         struct __log_source_##group##_##name##_register {         \
            __log_source_##group##_##name##_register() {           \
               (groupScopesRegisterBody(group, name, __VA_ARGS__)) \
               ([](SourceNature src, cstr path) {                  \
                  __log_source_##group()->enscope(src,path);       \
               });                                                 \
            }                                                      \
         } __log_source_##group##_##name##_register_inst;          \
      }                                                              


// join
/*
#  define groupScopesMember(group, name, ...)                              \
      namespace __log_scopes {                                             \
         using ::logging::location::SourceNature;                          \
         using ::logging::location::source;                                \
         using ::logging::location::EnscopeFunc;                           \
         using ::logging::location::SourceNature;                          \
         extern source __log_source_##group;                               \
         struct __member_register_##group##_##name {                       \
            __member_register_##group##_##name() {                         \
               [](EnscopeFunc __enscope) {                                 \
                  enscopeSrc(GROUP_CHILD, __FILE__);                       \
                  enscopeSrc(FILE_SOURCE, __FILE__)                        \
                  __VA_ARGS__;                                             \
               }([](SourceNature nature, cstr path) { \
                  __log_source_##group.enscope(nature, path);              \
               });                                                         \
            }                                                              \
         } __member_register_##group##_##name##_inst;                      \
      }
*/

// use
#  define groupScope(group, name) \
      (*__log_scopes::__log_source_##group())

}}


#endif

