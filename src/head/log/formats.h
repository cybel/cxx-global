#ifndef __LOG_FORMAT_H__
#define __LOG_FORMAT_H__

namespace logging {
namespace format {
   using namespace ::config::logging::format;
   using boost::format;
   using logging::message::cmessageRef;
   using logging::message::cmessagePtr;
   using std::transform;
   using std::bind;
   using std::function;
   using std::string;
   using std::stringstream;
   using std::vector;
   using namespace std::placeholders;


   /////////////////////////////////////////////////////////////////////
   ////// Formatter // (abstract base)
   //////////////////

   enum struct FormatPart: uint8 
   {  __first = 0
   ,  MCLASS  = __first
   ,  MLEVEL
   ,  SOURCE
   ,  SCOPE
   ,  TOPIC
   ,  TIME
   ,  WALLCLOCK
   ,  MESSAGE
   ,  THREAD
   ,  __last
   };

   typedef function<string(FormatPart, cmessageRef)>
      FormatPartFunc;

   struct Formatter_S {
      // format a message
      alwaysinline
      string format(cmessageRef msg, FormatPartFunc pull = nullptr) const {
         if (!pull)
            pull = bind( &Formatter_S::formatPart, this
                       , _1, _2
                       );
         return formatImpl(msg, pull);
      }

      // format a set of messages
      template <class InputIt, class OutputVec>
      alwaysinline
      void format( InputIt        first
                 , InputIt        last
                 , OutputVec&     dest
                 , FormatPartFunc pull = nullptr
                 ) const {
         if (!pull)
            pull = bind( &Formatter_S::formatPart, this
                       , _1, _2
                       );

         for (;first != last; first++)
            dest.push_back(this->format(**first, pull));
      }

   protected:
      virtual string formatPart(FormatPart part, cmessageRef msg) const;
      virtual string formatImpl(cmessageRef msg, FormatPartFunc pull) const = 0;

   };
   CompactAbstractTypes(Formatter,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Boost String Formatter //
   ///////////////////////////////

   struct boostFormatter_S 
   : public Formatter_S 
   {
      boostFormatter_S(const string& format = DEFAULT_FORMAT);

   protected:
      alwaysinline
      virtual string formatImpl( cmessageRef msg
                               , FormatPartFunc pull
                               ) const {
         assert(pull);
         boost::format f(fmt);

         for ( uint8 i = (uint8)FormatPart::__first
             ; i < (uint8)FormatPart::__last
             ; i++
             )
            f % pull((FormatPart)i, msg);

         return f.str();
      }

   private:
      boost::format fmt;
   };
   CompactTypes(boostFormatter,_S);
}}

#endif

