#ifndef __LOG_LEVELS_H__
#define __LOG_LEVELS_H__

namespace logging {
namespace filter {
   using namespace ::config::logging::filter;
   using ::util::math::sclamp;

   typedef uint16 MClassNT;
   typedef uint16 MLevelNT;

   //                           Message Spec 
   //  Class (<<13)            /  (11 bits)
   //   |                     /
   // /¯¯¯\     /¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
   // C C C L L n n n n n n n n n n n
   //       \_/
   //        |
   //      Level (<<11)

   enum struct MClass: MClassNT
   {  UNKNOWN        =     0
   ,  ERROR          =  1<<13
   ,  WARNING        =  2<<13
   ,  NOTIFICATION   =  3<<13
   ,  DEBUG          =  4<<13

   ,  MAX_VALUE__    =  5<<13
   //                  65535
   };

#  define x(c, l) ((c<<13)+(l<<11))
   enum struct MLevel: MLevelNT
   {  UNKNOWN     =      0
   ,  CRITICAL    =  x(1,1) // 0, 1
   ,  ERROR       =  x(1,2) // 0, 2
   ,  EXCEPTION   =  x(1,3) // 0, 3

   ,  WARNING     =  x(2,1)

   ,  NOTE        =  x(3,1) // 2, 1
   ,  INFO        =  x(3,2)
   ,  VERBOSE     =  x(3,3)

   ,  DEBUG       =  x(4,1) // 3, 1
   ,  TRACE       =  x(4,2)

   ,  MAX_VALUE__ =  x(5,0) 
   };
#  undef x

#  define level(x) (::logging::filter::MLevel::x)

   // Enumeration -> Numeric

   alwaysinline 
   MClassNT toClassN(const MLevel level) {
      return (MClassNT) (((MLevelNT)level) & (7 << 13));
   }

   template <typename T>
   alwaysinline
   cstr toClassS(const T cls) {
      MClassNT x = (((MClassNT)cls >> 13) & 7);
      return CLASS_NAMES[x];
   }

   alwaysinline 
   MLevelNT toLevelN(const MLevel level) {
      return (MLevelNT) level;
   }

   template <typename T>
   alwaysinline
   cstr toLevelS(const T level) {
      MClassNT x = (((MLevelNT)level >> 13) & 7);
      MLevelNT y = (((MClassNT)level >> 11) & 3);
      
      return LEVEL_NAMES[sclamp(x-1,0,7) * 3 + sclamp(y,0,3)];
   }

   // Numeric -> Enumeration

   alwaysinline 
   MLevel toLevel(const MLevelNT level) {
      return (MLevel) level;
   }

   alwaysinline 
   MClass toClass(const MLevelNT level) {
      return (MClass) toClassN(toLevel(level));
   }

   // Enumeration -> Enumeration

   alwaysinline 
   MClass toClass (const MLevel level) {
      return (MClass) toClassN(level);
   }

   alwaysinline 
   bool accept(const MLevel given, const MLevel message) {
      return message <= given;
   }

   alwaysinline 
   bool accept(const MClass given, const MClass message) {
      return message <= given;
   }

   alwaysinline 
   bool accept(const MLevelNT given, const MLevelNT message) {
      return message <= given;
   }

}}


#endif
