#ifndef __LOG_TOPICS_H__
#define __LOG_TOPICS_H__

namespace logging {
namespace location {
   using std::string;
   using std::tuple;


   /////////////////////////////////////////////////////////////////////
   ////// Topic //
   //////////////

   struct topic_S {
      const uint64   id[2];
      cstrc          name;
      topic_S *const parent;

      topic_S(cstr name, topic_S* parent = nullptr);

      // same topic
      bool operator==(const topic_S& other) const;
      bool operator!=(const topic_S& other) const;

      // hierarchy (parent > child)
      bool operator> (const topic_S& other) const;
      bool operator< (const topic_S& other) const;

      // mute
      bool silence(bool nowMuted = true);

      // muted
      bool     mute() const;
      operator bool() const;

      // name
      operator string() const;
      operator cstr() const;

   private:
      bool muted;
   };
   CompactTypes(topic,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Topic Collection State // (opaque)
   ///////////////////////////////

   struct topicCollectionState_S;
   CompactTypes(topicCollectionState,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Topic //
   //////////////

   struct topicCollection_S {
      topicCollection_S();
      ~topicCollection_S();

      topicPtr begin(cstr name, rptr hint = nullptr);
      topicPtr conclude(topicPtr topic);

      topicPtr currently();

   private:
      topicCollectionStatePtr state;
   };
   CompactTypes(topicCollection,_S);
}}

#endif
