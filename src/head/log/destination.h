#ifndef __LOG_DESTINATION_H__
#define __LOG_DESTINATION_H__

namespace logging {
namespace destination {
   using std::numeric_limits;
   using std::queue;
   using std::string;
 
   /////////////////////////////////////////////////////////////////////
   ////// Destination // (abstract)
   ////////////////////

   struct Destination_S {
      template <class InputIt>
      alwaysinline
      void write(InputIt begin, InputIt end, dptr count = numeric_limits<dptr>::max()) {
         for (;begin != end && count-- > 0; begin++)
            writeImpl(begin->c_str());
      }

   protected:
      virtual void writeImpl(cstrc msg) = 0;
   };
   CompactAbstractTypes(Destination,_S);


   /////////////////////////////////////////////////////////////////////
   ////// Destination Targeted // (abstract)
   /////////////////////////////

   template <typename TARGET>
   struct DestinationTargeted_S 
   : public virtual Destination
   {
      alwaysinline 
      bool ready() const {
         return readyImpl();
      }

      alwaysinline 
      bool open(const TARGET& what) {
         return openImpl(what);
      }

      alwaysinline 
      bool close() {
         return closeImpl();
      }

   protected:
      virtual bool readyImpl() const =0;
      virtual bool openImpl(const TARGET& what) =0;
      virtual bool closeImpl() =0;
   };
   CompactAbstractTemplates( DestinationTargeted,_S
                           , typename,TARGET
                           );

}}

#endif
