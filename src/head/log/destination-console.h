#ifndef __LOG_DESTINATION_CONSOLE_H__
#define __LOG_DESTINATION_CONSOLE_H__

#include "head/log/destination.h"

namespace logging {
namespace destination {
   using std::ostream;

   /////////////////////////////////////////////////////////////////////
   ////// Console Destination //
   ////////////////////////////

   enum struct ConsoleOutput : uint8
   {  STDOUT
   ,  STDERR
   ,  SPAWN
   ,  NOTHING
   };

   struct console_S 
   : public         DestinationTargeted<ConsoleOutput>
   , public virtual Destination
   {
   protected:
      alwaysinline 
      bool readyImpl() const {
         return dest;
      }

      alwaysinline 
      bool openImpl(const ConsoleOutput& what) {
         switch (what) {
         case ConsoleOutput::STDOUT:
            dest = &std::cout;
         break;
         case ConsoleOutput::STDERR:
            dest = &std::cerr;
         break;
         case ConsoleOutput::SPAWN:
            assert(false);
         break;
         case ConsoleOutput::NOTHING:
            assert(false);
         break;
         }

         return dest;
      }

      alwaysinline 
      bool closeImpl() {
         dest = nullptr;
         return true;
      }

      alwaysinline 
      void writeImpl(cstrc line) {
         if (dest)
            (*dest) << line << "\n";
      }
 
   private:
      ostream* dest = nullptr;
   };
   CompactTypes(console,_S);

}}

#endif
