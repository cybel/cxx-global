#ifndef __OS_WIN_UTIL_HIRESTIMER_H__
#define __OS_WIN_UTIL_HIRESTIMER_H__

#include "head/util/highresTimer.h"

#if !(E_OS==OS_WIN32 || E_OS==OS_WIN64)
#  error This implementation of highresTimer is meant for Windows 32/64-bit only
#endif

namespace util {
   class HighresTimer : public HighresTimerBase {
   protected:
      uint64 freq;
      uint64 val;
      uint64 mrk;
      uint64 begin;
   public:
      HighresTimer() {
         LARGE_INTEGER i;
         
         ZeroMemory(&i, sizeof(LARGE_INTEGER));
         QueryPerformanceFrequency(&i);
         freq = (uint64) i.QuadPart;

         ZeroMemory(&i, sizeof(LARGE_INTEGER));
         QueryPerformanceCounter(&i);
         val = mrk = begin = (uint64) i.QuadPart;
      }

      // return drift
      virtual uint64 update() {
         LARGE_INTEGER i;
         uint64 j;
         ZeroMemory(&i, sizeof(LARGE_INTEGER));
         QueryPerformanceCounter(&i);

         i.QuadPart *= 1000000;
         i.QuadPart /= freq;

         j = val;
         val = (uint64) i.QuadPart;

         return val - j;
      }

      // return now
      virtual uint64 now() {
         update();
         return val;
      }

      virtual uint64 lastUpdate() const  {
         return val;
      }

      // absolute
      virtual uint64 markAbs() const {
         return mrk;
      }

      // relative to now
      virtual sint64 markRel() {
         update();
         return (sint64)mrk - (sint64)val;
      }
      
      // set mark to now
      virtual void   setMark() {
         update();
         mrk = val;
      }

      // set mark to absolute
      virtual void   setMark(uint64 when) {
         mrk = when;
      }

      // set mark to relative
      virtual void   setMark(sint64 when) {
         update();
         mrk = val + when;
      }

      virtual bool markPast() {
         update();
         return mrk < val;
      }

      // resolution
      virtual HighresTimerResolution resolution() const {
         return HighresTimerResolution::MICRO;
      }

      virtual uint64 frequency() const {
         return freq;
      }
   };
}

#endif

