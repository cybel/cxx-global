#ifndef __OS_WIN_UTIL_THREADED_H__
#define __OS_WIN_UTIL_THREADED_H__

namespace util {
   class Thread;
   extern DWORD WINAPI __threadEntry(LPVOID threadObj); 

   class Thread : public ThreadBase {
   protected:
      Threaded *threaded;
      HANDLE    threadhndl;
      DWORD     threadId;
      bool      started;
      bool      suspension;
      bool      died;
      bool      completed;
      void     *argument;

      void createThread(bool prealloc) {
         HANDLE h = CreateThread(nullptr
                                , 0
                                , __threadEntry
                                , (LPVOID)this
                                , (prealloc) ? (CREATE_SUSPENDED) : (0u)
                                , &(this->threadId)
                                );
         if (h)
            threadhndl = h;
      }
   public:
      Thread() = delete;

      Thread(Threaded &threaded_, void *arg = nullptr, bool prealloc = true) 
         : threaded(&threaded_)
         , argument(arg)
         , threadhndl(nullptr)
         , threadId(MAXDWORD)
         , started(false)
         , suspension(prealloc) 
         , died(false) 
         , completed(false) {
         if (prealloc)
            createThread(prealloc);
         threaded->thread(*this);
      }

      intptr_t __entry() {
         intptr_t o = threaded->entry()(argument);
         started = false;
         completed = true;
         return o;
      }

      virtual void start() {
         if (!started && suspension) {
            started = true;
            resume();
         } else if (started)
            return;

         started = true;
         completed = false;
         died = false;
         createThread(false);
      }

      virtual void stop() {
         if (!TerminateThread(threadhndl, ERROR_DBG_TERMINATE_THREAD)) {
            completed = false;
            started = false;
            died = true;
         }
      }

      virtual void suspend() {
         if (started && !suspension)
            SuspendThread(threadhndl);
      }

      virtual void resume() {
         if (started && suspension)
            ResumeThread(threadhndl);
      }

      virtual void join() {
         if (died)
            return;
         died = true;
         WaitForSingleObject(threadhndl, INFINITE);
         CloseHandle(threadhndl);
      }

      virtual uint64 id() {
         return (uint64) threadId;
      }

      virtual bool complete() {
         return completed;
      }

      virtual bool   running() {
         return started;
      }

      virtual bool   dead() {
         return died;
      }
      
      virtual bool   suspended() {
         return suspension;
      }
   };

   
}

#endif

