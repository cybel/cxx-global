#ifndef __OS_LIN_OPENGL_H__
#define __OS_LIN_OPENGL_H__

extern "C" {
#  include <X11/X.h>
#  include <X11/Xlib.h>
#  include <X11/Xatom.h>

#  include <GL/glx.h>
}

#endif

