#ifndef __OS_LIN_THREADING_PTHREAD_CONTROLS_H__
#define __OS_LIN_THREADING_PTHREAD_CONTROLS_H__

#include "head/threads/types.h"

namespace threads {
namespace bits {
   using namespace threads;
   using std::thread;

   auto getThreadAffinity (thread* t)               -> Affinity;
   auto setThreadAffinity (thread* t, Affinity aff) -> void;

   auto getAffinityMask () -> Affinity;

}}

#endif
