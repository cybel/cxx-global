#ifndef __THREADS_THREAD_H__
#define __THREADS_THREAD_H__

#include "head/threads/types.h"

namespace threads {
   using namespace ::config::threads::bits;
   using boost::mutex;
   using std::atomic;
   using std::function;
   using std::tuple;

#  define M(x) auto x
   struct cothread_S {
      cothread_S(msize heapsize = CO_HEAPSIZE_DEFAULT);
      ~cothread_S();

      //// Attach/detach to/from a function ////

      M(canAttach)
      () const -> bool;

      M(canDetach)
      () const -> bool;

      M(attach)
      (CothreadEntry entry, rptr arg) -> bool;

      M(detach)
      () -> tuple<CothreadEntry, rptr>;

      //// Accepting CPU time ////

      M(canEnter)
      () const -> bool;

      M(enter)
      () -> void;

      //// Shutdown ////

      M(signalShutdown)
      () -> bool;

      M(canDelete)
      () -> bool;

   private:
      atomic<bool>  isOccupied;  // currently has CPU time
      atomic<bool>  isAvailable; // currently CAN be used/destroyed
      atomic<bool>  isRunning;   // currently entry is active
      atomic<bool>  isWanting;   // Wants further time
      atomic<bool>  detachSoon;  // conclude explicitly called

      mutex         modifyEntry;
      CothreadEntry entry;
      rptr          arg;

      atomic<cothread_t> coReturn;
      atomic<cothread_t> handle;      

      M(_main)
      () -> void;

      M(_start)
      () -> bool;

   };
#  undef M

}

#endif
