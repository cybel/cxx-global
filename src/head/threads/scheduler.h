#ifndef __THREADS_SCHEDULER_H__
#define __THREADS_SCHEDULER_H__

#include "head/log/log.h"

#include "types.h"
#include "cothread.h"

namespace threads {
   using boost::mutex;
   using boost::unique_lock;
   using logging::loggerRef;
   using std::deque;
   using std::function;
   using std::map;
   using std::pair;
   using std::thread;
   using std::set;
   using std::vector;

#  define M(x) auto x
   struct scheduler_S {
      scheduler_S();
      ~scheduler_S();

      M(registerThread)
      (thread&, Affinity = AffinityAll) -> void;
      
      M(scheduleCothread)
      (cothreadCPtr, Affinity = AffinityAll) -> void;

      M(start)
      (Affinity = AffinityAny) -> void;

      M(halt)
      (Affinity = AffinityAny) -> void;

   private:
      struct runnerContext {
         Affinity    cores;
         Affinity    schedule;
      };

      M(runnerEntry)
      (runnerContext*) -> bool;

      M(runnerMain)
      (runnerContext&) -> bool;

      static const Affinity AffinityAll;
      
      atomic<Affinity> cores;

      mutex schedulesMutex;
      map<Affinity, pair<mutex, deque<cothreadPtr>>> schedules;

      mutex threadMutex;
      vector<thread*>  runners;
      vector<thread*>  allocations;
   };
#  undef M
   CompactTypes(scheduler,_S);

}

#endif
