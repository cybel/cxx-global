#ifndef __THREADS_TYPES_H__
#define __THREADS_TYPES_H__

typedef void* cothread_t;

namespace threads {
   using std::function;

   typedef uint64 Affinity;
   constexpr Affinity AffinityAny = 0xFFFFFFFFFFFFFFFFLL;

   struct cothread_S;
   CompactTypes(cothread,_S);

   typedef function<void()>            YieldFunc;
   typedef function<void(cothreadRef)> PassFunc;
   typedef function<void(void)>        ConcludeFunc;

   struct cothreadEnv_S {
      rptr arg;

      YieldFunc    yield;
      PassFunc     pass;
      ConcludeFunc conclude;
   };
   CompactTypes(cothreadEnv,_S);

   typedef function<bool(ccothreadEnvRef)> CothreadEntry;
}

#endif
