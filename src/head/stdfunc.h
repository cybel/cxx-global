#ifndef __STDFUNC_H__
#define __STDFUNC_H__

#include "head/stdtyp.h"

#include <vector>
#include <string>

template <typename T>
alwaysinline rptr rptrAdd(const rptr& i, const T j) {
   return reinterpret_cast<rptr>(reinterpret_cast<decltype(j)>(i) + j);
}

template <typename T>
alwaysinline rptr rptrSub(const rptr& i, const T j) {
   return reinterpret_cast<rptr>(reinterpret_cast<decltype(j)>(i) - j);
}

template <typename T> 
alwaysinline T nvl(T v, T d) {
   if (!v)
      return d;
   return v;
}

template <template <class TYPE, class ...Args> class CONTAINER = std::vector>
alwaysinline CONTAINER<std::string> split(const std::string& src, chr on) {
   CONTAINER<std::string> c;
   msize left = -1;
   msize right;
   do {
      left++;
      right = src.find_first_of(on, left);

      if (right == std::string::npos)
         c.push_back(src.substr(left));
      else 
         c.push_back(src.substr(left, right-left));

      left = right;
   } while (left != std::string::npos);

   return c;

}


#endif

