#ifndef __COMMON_H__
#define __COMMON_H__

#include "head/config.h"

#include "head/platform.h"

#if E_COMPILER==COMP_MSVC
#  pragma warning(push)
#  pragma warning(disable:4350) // behavior change
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include "head/stddef.h"
#include "head/stdtyp.h"
#include "head/stdfix.h"
#include "head/stdfunc.h"
#include "head/stdlibs.h"

#if E_COMPILER==COMP_MSVC
#  pragma warning(pop)
#endif

#include "head/syslibs.h"
#include "head/systyp.h"
#include "head/extlibs.h"

#include "head/util/pchrono.h"              // program clocks

#include "head/util/environment-info.h"    // util::env:: // CLI Parsing etc.

#include "head/util/hash.h"            // util::hash::

#include "head/util/popcount.h"        // util::     // Population count, compile and run time
#include "head/util/bits.h"            // util::     // Bit tricks

#include "head/util/math.h"
#include "head/util/static-math.h"

#include "head/util/extint.h"          // util::     // Flexible/variable-width integers
#include "head/util/highres-timer.h"   // util::     // High resolution timers/counters
//#include "head/util/threaded.h"        // util::     // Flexible platform-independent C++ threading

//#include "head/util/factories.h"       // util::pattern::factory::
#include "head/util/basic-factory.h"   // util::factories::basic::
#include "head/util/basic-lookup.h"

#include "head/util/transaction/simple-linear.h" // util::res::transaction

#include "head/util/temporal-attachment.h"

#include "head/memory/util.h"

//#include "head/log/levels.h"

#include "head/util/alt.h"

#endif 
