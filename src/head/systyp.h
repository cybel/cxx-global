#ifndef __SYSTYP_H__
#define __SYSTYP_H__

#include "head/platform.h"
#include "head/stdtyp.h"
#include "head/syslibs.h"

#if E_OS==OS_WINDOWS
   typedef HANDLE osFile;
#elif E_OS==OS_LINUX
   typedef int    osFile;
#endif

#endif

