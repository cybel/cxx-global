#ifndef __EXTLIB_BOOST_MPL_H__
#define __EXTLIB_BOOST_MPL_H__

#include "head/platform.h"

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER==COMP_MSVC
#     pragma warning(push)
#     pragma warning(disable:4668)
#  endif
#endif

#include <boost/mpl/map.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/mpl/at.hpp>

#if (E_DEBUG & DEBUG_NO_SUPPRESS_WARN) == 0
#  if E_COMPILER==COMP_MSVC
#     pragma warning(pop)
#  endif
#endif

#endif

