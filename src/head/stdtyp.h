#ifndef __COMMON_STDTYP_H__
#define __COMMON_STDTYP_H__

#include "head/platform.h"

#if E_LANG==LANGUAGE_C
#  include <cstdint>
#  include <climits>
#else
#  include <stdint.h>
#  include <limits.h>
#endif

#include <typeinfo>
#include <type_traits>
#include <limits>

#include <functional>
#include <string>

typedef float                          real32  ;
typedef double                         real64  ;
          
typedef uint8_t                        uint8   ;
typedef  int8_t                        sint8   ;
typedef uint16_t                       uint16  ;
typedef  int16_t                       sint16  ;
typedef uint32_t                       uint32  ;
typedef  int32_t                       sint32  ;
typedef uint64_t                       uint64  ;
typedef  int64_t                       sint64  ;
typedef uintmax_t                      uintMax ;
typedef  intmax_t                      sintMax ;
typedef char                           chr     , *str  , *const strc   ;
typedef unsigned char                  uchr    , *ustr , *const ustrc  ;
typedef const unsigned char            cuchr   , *custr, *const custrc ;
typedef const char          *          cstr    ;
typedef const char          *const     cstrc   ;

typedef intptr_t                       sptr    ;
typedef uintptr_t                      uptr    ;
typedef std::ptrdiff_t                 dptr    ;
typedef std::nullptr_t                 nptr    ;
typedef void                *          rptr    ;
typedef void *const                    crptr   ;
typedef size_t                         msize   ;

typedef std::u16string                 lstring;

constexpr real32 R32_EPSILON = std::numeric_limits<real32>::epsilon();
constexpr real64 R64_EPSILON = std::numeric_limits<real64>::epsilon();

constexpr real32 PI = 3.1415926535897932384626433832f;

// fit
template <unsigned Bits>
using ufit
   = std::conditional_t< Bits <=  8, uint8
   , std::conditional_t< Bits <= 16, uint16
   , std::conditional_t< Bits <= 32, uint32
   , std::enable_if_t  < Bits <= 64, uint64
   >>>>;

template <unsigned Bits>
using sfit
   = std::conditional_t< Bits <=  8, sint8
   , std::conditional_t< Bits <= 16, sint16
   , std::conditional_t< Bits <= 32, sint32
   , std::enable_if_t  < Bits <= 64, sint64
   >>>>;

// predicates
template <typename T>
using UnaryPredicate = std::function<bool (const T&)>;

// type modifiers

template<typename T> using ref              =       T &            ;
template<typename T> using ptr              =       T *            ;
template<typename T> using cptr             =       T *const       ;
template<typename T> using ptrptr           =       T *      *     ;
template<typename T> using cptrptr          =       T *const *     ;
template<typename T> using ptrcptr          =       T *      *const;
template<typename T> using cptrcptr         =       T *const *const;

template<typename T> using cref             = const T &            ;
template<typename T> using ctptr            = const T *            ;
template<typename T> using ctcptr           = const T *const       ;
template<typename T> using ctptrptr         = const T *      *     ;
template<typename T> using ctcptrptr        = const T *const *     ;
template<typename T> using ctptrcptr        = const T *      *const;
template<typename T> using ctcptrcptr       = const T *const *const;

// Compact types (no template, simple)

#define CompactAbstractTypes(x,y) \
             typedef x##y                         x             \
                        ,           &             x##Ref        \
                        ,           *             x##Ptr        \
                        ,           *const        x##CPtr       \
                        ,           *      *      x##PtrPtr     \
                        ,           *const *      x##CPtrPtr    \
                        ,           *      *const x##PtrCPtr    \
                        ,           *const *const x##CPtrCPtr   \
                        ; typedef const x##y      C##x          \
                        ,           &             C##x##Ref     \
                        ,           *             C##x##Ptr     \
                        ,           *const        C##x##CPtr    \
                        ,           *      *      C##x##PtrPtr  \
                        ,           *const *      C##x##CPtrPtr \
                        ,           *      *const C##x##PtrCPtr \
                        ,           *const *const C##x##CPtrCPtr

#define CompactTypes(x,y) typedef x##y            x             \
                        ,           &             x##Ref        \
                        ,           *             x##Ptr        \
                        ,           *const        x##CPtr       \
                        ,           *      *      x##PtrPtr     \
                        ,           *const *      x##CPtrPtr    \
                        ,           *      *const x##PtrCPtr    \
                        ,           *const *const x##CPtrCPtr   \
                        ; typedef const x##y      c##x          \
                        ,           &             c##x##Ref     \
                        ,           *             c##x##Ptr     \
                        ,           *const        c##x##CPtr    \
                        ,           *      *      c##x##PtrPtr  \
                        ,           *const *      c##x##CPtrPtr \
                        ,           *      *const c##x##PtrCPtr \
                        ,           *const *const c##x##CPtrCPtr      

// Compact types (with template, less simple)
// Todo: Verify MSVC likes this. It likely won't due to being a flaming
// garbage dumpster that happens to output object code.

#define __CPCT_TMPL_A(...)            __CPCT_TMPL_A_IMPL(__VA_ARGS__)
#define __CPCT_TMPL_B(...)            __CPCT_TMPL_B_IMPL(__VA_ARGS__)
#define __CPCT_TMPL_A_IMPL(...)       VA_DISPATCH(__CPCT_TMPL_a_, __VA_ARGS__)(__VA_ARGS__)
#define __CPCT_TMPL_B_IMPL(...)       VA_DISPATCH(__CPCT_TMPL_b_, __VA_ARGS__)(__VA_ARGS__)
#define __CPCT_TMPL_LEFT(b,x,p,...)   template <__CPCT_TMPL_A(__VA_ARGS__)> using b##x##p =
#define __CPCT_TMPL_RIGHT(x,y,...)    x##y<__CPCT_TMPL_B(__VA_ARGS__)> 

#define __CPCT_TMPL_a_2(a,b)               a b
#define __CPCT_TMPL_b_2(a,b)                 b
#define __CPCT_TMPL_a_4(a,b,c,d)           a b, c d
#define __CPCT_TMPL_b_4(a,b,c,d)             b,   d   
#define __CPCT_TMPL_a_6(a,b,c,d,e,f)       a b, c d, e f
#define __CPCT_TMPL_b_6(a,b,c,d,e,f)         b,   d,   f
#define __CPCT_TMPL_a_8(a,b,c,d,e,f,g,h)   a b, c d, e f, g h
#define __CPCT_TMPL_b_8(a,b,c,d,e,f,g,h)     b,   d,   f,   h

#define __CPCT_TMPL_a_10(a,b,c,d,e,f,g,h,i,j)               a b, c d, e f, g h, i j
#define __CPCT_TMPL_b_10(a,b,c,d,e,f,g,h,i,j)                 b,   d,   f,   h,   j
#define __CPCT_TMPL_a_12(a,b,c,d,e,f,g,h,i,j,k,l)           a b, c d, e f, g h, i j, k l
#define __CPCT_TMPL_b_12(a,b,c,d,e,f,g,h,i,j,k,l)             b,   d,   f,   h,   j,   l
#define __CPCT_TMPL_a_14(a,b,c,d,e,f,g,h,i,j,k,l,m,n)       a b, c d, e f, g h, i j, k l, m n
#define __CPCT_TMPL_b_14(a,b,c,d,e,f,g,h,i,j,k,l,m,n)         b,   d,   f,   h,   j,   l,   n
#define __CPCT_TMPL_a_16(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)   a b, c d, e f, g h, i j, k l, m n, o p
#define __CPCT_TMPL_b_16(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)     b,   d,   f,   h,   j,   l,   n,   p

#define __CPCT_TMPL_a_18(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r)                   a b, c d, e f, g h, i j, k l, m n, o p, q r
#define __CPCT_TMPL_b_18(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r)                     b,   d,   f,   h,   j,   l,   n,   p,   r
#define __CPCT_TMPL_a_20(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t)               a b, c d, e f, g h, i j, k l, m n, o p, q r, s t
#define __CPCT_TMPL_b_20(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t)                 b,   d,   f,   h,   j,   l,   n,   p,   r,   t
#define __CPCT_TMPL_a_22(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v)           a b, c d, e f, g h, i j, k l, m n, o p, q r, s t, u v
#define __CPCT_TMPL_b_22(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v)             b,   d,   f,   h,   j,   l,   n,   p,   r,   t,   v
#define __CPCT_TMPL_a_24(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x)       a b, c d, e f, g h, i j, k l, m n, o p, q r, s t, u v, w x
#define __CPCT_TMPL_b_24(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x)         b,   d,   f,   h,   j,   l,   n,   p,   r,   t,   v,   x
#define __CPCT_TMPL_a_26(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)   a b, c d, e f, g h, i j, k l, m n, o p, q r, s t, u v, w x, y z
#define __CPCT_TMPL_b_26(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z)     b,   d,   f,   h,   j,   l,   n,   p,   r,   t,   v,   x,   z

#define CompactTemplates(x,y,...) \
   __CPCT_TMPL_LEFT( ,x,         ,__VA_ARGS__)             __CPCT_TMPL_RIGHT(x,y,__VA_ARGS__); \
   __CPCT_TMPL_LEFT( ,x,Ref      ,__VA_ARGS__)  ref       <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,Ptr      ,__VA_ARGS__)  ptr       <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtr     ,__VA_ARGS__)  cptr      <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,PtrPtr   ,__VA_ARGS__)  ptrptr    <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtrPtr  ,__VA_ARGS__)  cptrptr   <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,PtrCPtr  ,__VA_ARGS__)  ptrcptr   <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtrCPtr ,__VA_ARGS__)  cptrcptr  <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
                                                                                              \
   __CPCT_TMPL_LEFT(c,x,         ,__VA_ARGS__)  const      __CPCT_TMPL_RIGHT(x,y,__VA_ARGS__); \
   __CPCT_TMPL_LEFT(c,x,Ref      ,__VA_ARGS__)  cref      <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,Ptr      ,__VA_ARGS__)  ctptr     <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,CPtr     ,__VA_ARGS__)  ctcptr    <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,PtrPtr   ,__VA_ARGS__)  ctptrptr  <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,CPtrPtr  ,__VA_ARGS__)  ctcptrptr <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,PtrCPtr  ,__VA_ARGS__)  ctptrcptr <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(c,x,CPtrCPtr ,__VA_ARGS__)  ctcptrcptr<__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>

#define CompactAbstractTemplates(x,y,...) \
   __CPCT_TMPL_LEFT( ,x,         ,__VA_ARGS__)             __CPCT_TMPL_RIGHT(x,y,__VA_ARGS__); \
   __CPCT_TMPL_LEFT( ,x,Ref      ,__VA_ARGS__)  ref       <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,Ptr      ,__VA_ARGS__)  ptr       <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtr     ,__VA_ARGS__)  cptr      <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,PtrPtr   ,__VA_ARGS__)  ptrptr    <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtrPtr  ,__VA_ARGS__)  cptrptr   <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,PtrCPtr  ,__VA_ARGS__)  ptrcptr   <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT( ,x,CPtrCPtr ,__VA_ARGS__)  cptrcptr  <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
                                                                                              \
   __CPCT_TMPL_LEFT(C,x,         ,__VA_ARGS__)  const      __CPCT_TMPL_RIGHT(x,y,__VA_ARGS__); \
   __CPCT_TMPL_LEFT(C,x,Ref      ,__VA_ARGS__)  cref      <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,Ptr      ,__VA_ARGS__)  ctptr     <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,CPtr     ,__VA_ARGS__)  ctcptr    <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,PtrPtr   ,__VA_ARGS__)  ctptrptr  <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,CPtrPtr  ,__VA_ARGS__)  ctcptrptr <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,PtrCPtr  ,__VA_ARGS__)  ctptrcptr <__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>;\
   __CPCT_TMPL_LEFT(C,x,CPtrCPtr ,__VA_ARGS__)  ctcptrcptr<__CPCT_TMPL_RIGHT(x,y,__VA_ARGS__)>


#endif
