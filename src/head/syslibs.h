#ifndef __SYSLIBS_H__
#define __SYSLIBS_H__

#include "head/platform.h"

#if (E_OS==OS_WIN32) || (E_OS==OS_WIN64)
#  if E_COMPILER==COMP_MSVC
#     pragma warning(push)
#     pragma warning(disable:4668) // not defined as a preprocessor macro
#  endif
#  include <WinSock2.h>
#  include <fcntl.h>
#  include <io.h>
#  include <direct.h>
#  include <Windows.h>
#  include "ntverp.h"
#  if !defined(VER_PRODUCTBUILD) ||  VER_PRODUCTBUILD<3790
#     pragma message ("********************************************************************************************")
#     pragma message ("   Notice (performance-warning): you are not using the Microsoft Platform SDK,")
#     pragma message ("                              we'll use CRITICAL_SECTION instead of InterLocked operation")
#     pragma message ("********************************************************************************************")
#  endif
#  ifdef min
#     undef min
#  endif
#  ifdef max
#     undef max
#  endif
#  if E_COMPILER==COMP_MSVC
#     pragma warning(pop)
#  endif
#endif

#if (E_OS==OS_LINUX)
   extern "C" {
#     include <unistd.h>
#     include <fcntl.h>
#     include <sys/types.h>
#     include <sys/resource.h>
#     include <sys/mman.h>
#     include <sys/time.h>
#     include <sys/stat.h>
#     include <sys/wait.h>
   };
#endif

#endif

