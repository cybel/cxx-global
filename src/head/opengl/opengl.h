#ifndef __OPENGL_OPENGL_H__
#define __OPENGL_OPENGL_H__

#include <GL/glew.h>
#include <GL/gl.h>

#if E_OS == OS_LINUX
#  include "head/os/lin/opengl.h"
#else
#  error "No OpenGL WM headers. You need head/os/<your-os>/opengl.h !"
#endif

#include <GL/glu.h>
#include <GL/glut.h>

#endif

