#define VM(name) auto CLS::CPP_EXPAND(name)
#define SM(name) auto CLS::CPP_EXPAND(name)
#define M(name)  auto CLS::CPP_EXPAND(name)
#define C(x)          CLS::CPP_EXPAND(x)CPP_EXPAND(CLS)
#define VC(x)         CLS::CPP_EXPAND(x)CPP_EXPAND(CLS)
#define O(op)    auto CLS::operator op
#define CO(type)      CLS::operator type
