#include "stdinc.h"

#include "head/threads/types.h"
#include "head/threads/thread-controls.h"

#ifndef _GNU_SOURCE
#  define _GNU_SOURCE
#endif
#include "pthread.h"

#include "sys/sysinfo.h"
#include "sched.h"

namespace {
   using std::thread;
   using namespace threads;
   using namespace threads::bits;

   alwaysinline
   auto getPThreadHandle 
   (thread* t) -> pthread_t {
      return t->native_handle();
   }

   alwaysinline
   auto getPThreadAffinity
   (pthread_t& t) -> Affinity {
      cpu_set_t set;
      int ret = 0;
      Affinity o = 0;

      CPU_ZERO(&set);
      ret = pthread_getaffinity_np(t, sizeof(cpu_set_t), &set);
      if (!ret)
         for (int i = 0; i < CPU_SETSIZE; i++) 
            if (CPU_ISSET(i, &set))
               o |= 1<<i;
         
      return o;
   }

   alwaysinline
   auto setPThreadAffinity
   (pthread_t& t, Affinity aff) -> int {
      using util::lowBit;
      using util::lowBitPos;

      cpu_set_t set;
      int ret = 0;

      CPU_ZERO(&set);
      while (aff) {
         int b = lowBit(aff);
         aff ^= b;
         CPU_SET(lowBitPos(b), &set);
      }

      ret = pthread_setaffinity_np(t, sizeof(cpu_set_t), &set);

      return ret;
   }

}

namespace threads {
namespace bits {
   using namespace threads;
   using std::thread;

   auto getThreadAffinity
   (thread* t) -> Affinity {
      pthread_t th = getPThreadHandle(t);
      Affinity aff = getPThreadAffinity(th);
      assert(aff);
      return aff;
   }

   auto setThreadAffinity 
   (thread* t, Affinity aff) -> void {
      assert(aff);
      assert((getAffinityMask() & aff) == aff);
      pthread_t th = getPThreadHandle(t);
      if (!setPThreadAffinity(th, aff)) {
         //std::cout << ">>>>>>>>>>> " << std::hex << aff << " .,., " << std::hex << getPThreadAffinity(th) << std::endl;
         assert(getPThreadAffinity(th) == aff);
      }
   }

   auto getAffinityMask () -> Affinity {
      // 1<<1 -1 = 0001
      // 1<<2 -1 = 0011
      // 1<<3 -1 = 0111
      return (1 << get_nprocs_conf()) - 1;
   }

}}