#include "stdinc.h"

#include "head/util/threaded.h"
#include "head/os/win/util/threaded.h"

namespace util {
   DWORD WINAPI __threadEntry(LPVOID threadObj) {
      return (DWORD)(*(Thread*)threadObj).__entry();
   }

}