#include "stdinc.h"

#include "head/log/log.h"

#include "head/threads/types.h"
#include "head/threads/cothread.h"
#include "head/threads/scheduler.h"
#include "head/threads/thread-controls.h"

using boost::mutex;
using boost::unique_lock;
using logging::loggerRef;
using std::atomic;
using std::bind;
using std::function;
using std::make_tuple;
using std::literals::chrono_literals::operator""us;
using std::this_thread::get_id;
using std::this_thread::sleep_for;
using std::tuple;
using namespace std::placeholders;
using namespace ::config::os;
using namespace ::config::threads;

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

fileScopes( scheduler,
          ; enscopeSrc(NAMESPACE_NAMED, "threads"                   )
          ; enscopeSrc(FUNC_CXX       , "getApplication"            )
          ; enscopeSrc(FUNC_CXX       , "threads.start"             )
          ; enscopeSrc(FUNC_CXX       , "threads.runnerMain"        ) 
          );

defineFileLogger(fileScope());

////////////////////////////////////////////////////////////////////////

#define CLS scheduler_S
#define C(x) CLS::CPP_EXPAND(x)CPP_EXPAND(CLS)
#define M(x) auto CLS::x
namespace threads {  
   using namespace bits;
   using std::chrono::microseconds;

   const Affinity CLS::AffinityAll = getAffinityMask();

   C()   ()
   :     cores  (0)
   {}

   C(~)  () {
      for (auto ru: runners)
         if (ru->joinable())
            ru->join();
      for (auto alloc: allocations)
         if (alloc->joinable())
            alloc->join();
   }

   M(registerThread)
   (thread& t, Affinity affinity) -> void {
      affinity &= AffinityAll;
      assert(affinity);
      unique_lock<mutex> lock(threadMutex);
      setThreadAffinity(&t, affinity);
      allocations.push_back(&t);
   }

   M(scheduleCothread)
   (cothreadCPtr thread, Affinity affinity) -> void {
      affinity &= AffinityAll;
      assert(affinity);
      
      while (affinity) {
         Affinity core = util::lowBit(affinity);
         affinity &= ~core;

         {
            unique_lock<mutex> lock(schedulesMutex);
            unique_lock<mutex> slock(schedules[core].first);
            schedules[core].second.push_back(thread);
         }
      }
   }

   M(start)
   (Affinity cores) -> void {
      funcLog(scoped(FUNC_CXX, "threads.start"));

      lout << level(NOTE) << "Launching threads: " << nformat(NF_HEX) << cores << nformat(NF_DEC) << endm;

      cores &= AffinityAll & ~this->cores;
      assert(cores);

      unique_lock<mutex> lock(threadMutex);

      while (cores) {
         thread* th;
         function<bool(runnerContext*)> runner;

         Affinity core = util::lowBit(cores);
         cores &= ~core;

         lout << level(TRACE) << "Launching thread: " << util::lowBitPos(core) << endm;

         runnerContext* ctx = new runnerContext;
         ctx->cores = core;
         this->cores |= core;

         runner = bind(&scheduler::runnerEntry, this, _1);
         th = new thread(runner, ctx);
         setThreadAffinity(th, core);

         runners.push_back(th);
      }
   }

   M(halt)
   (Affinity affinity) -> void {
      Affinity core;
      cores &= ~affinity;

      while ((core = util::lowBit(affinity))) {
         affinity &= ~core;
         unique_lock<mutex> lock(threadMutex);

         for (auto th: runners) {
            if (th->joinable() && (core & getThreadAffinity(th)) == core)
               th->join();
         }
      }
   }

   M(runnerEntry) 
   (runnerContext* ctx) -> bool {
      assert(ctx);

      return runnerMain(*ctx);
   }

   M(runnerMain)
   (runnerContext& ctx) -> bool {
      funcLog(scoped(FUNC_CXX, "threads.runnerMain"));
      lout.begin("runner-main");

      Affinity focus = 0;
      bool starveNotice = false;;
      pair<mutex, deque<cothreadPtr>>* q;
      cothreadPtr job;
      microseconds slp;

      while (cores & ctx.cores) {
         if (!ctx.schedule)
            ctx.schedule = ctx.cores;

         if (slp.count()) {
            if (slp > SLEEP_STARVING && !starveNotice) { 
               lout << level(NOTE) << "Starving, sleeping for " << slp.count() << "us" << endm;
               starveNotice = true;
            }
            sleep_for(slp);
         }

         focus = util::lowBit(ctx.schedule);
         ctx.schedule &= ~focus;

         {
            unique_lock<mutex> lock(schedulesMutex);
            q = &schedules[focus];
         }

         {
            unique_lock<mutex> lock(q->first);
            if (!q->second.size()) {
               slp = slp.count() ? (microseconds) (uint64) (slp.count()*SLEEP_GROW) : SLEEP_BASE;
               if (slp > SLEEP_CAP)
                  slp = SLEEP_CAP;
               continue;
            } else {
               starveNotice = false;
               slp = 0us;
            }
            job = q->second.front();
            q->second.pop_front();
         }

         if (job->canEnter()) {
            job->enter();
         }

         {
            unique_lock<mutex> lock(q->first);
            q->second.push_back(job);
         }
      }

      return false;
   }

}

