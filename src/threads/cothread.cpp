#include "stdinc.h"

#include "head/libco.h"

#include "head/threads/types.h"
#include "head/threads/cothread.h"

using namespace ::config::os;
using boost::mutex;
using boost::unique_lock;
using std::atomic;
using std::bind;
using std::function;
using std::literals::chrono_literals::operator""s;
using std::make_tuple;
using std::this_thread::sleep_for;
using std::tuple;

namespace {
   typedef function<void()> CoEnter;

#  define DefT(x)                             \
      mutex      trampoline_parm_##x##_lock;  \
      CoEnter    trampoline_parm_##x;         \
      cothread_t trampoline_return_##x;       \
                                              \
      void trampoline_entry_##x(void) {       \
         CoEnter enter = trampoline_parm_##x; \
         trampoline_parm_##x = nullptr;       \
         assert(enter);                       \
         co_switch(trampoline_return_##x);    \
         /*----^ Init ^-----*/                \
         /* Lock Held Above */                \
         /*******************/                \
                                              \
         /*******************/                \
         /*    Reentrant    */                \
         /*---v Execute v---*/                \
         enter();                             \
         /* NEVER GETS HERE */                \
         sleep_for(3s);                       \
         assert(false);                       \
         exit(EXIT_COTHREAD_RETURNED);        \
      }                                                        

   DefT(0x0); DefT(0x1); DefT(0x2); DefT(0x3); 
   DefT(0x4); DefT(0x5); DefT(0x6); DefT(0x7);
   DefT(0x8); DefT(0x9); DefT(0xA); DefT(0xB); 
   DefT(0xC); DefT(0xD); DefT(0xE); DefT(0xF);
#  undef DefT

#  define DefT(x) \
      { &trampoline_parm_##x##_lock \
      , &trampoline_entry_##x       \
      , &trampoline_parm_##x        \
      , &trampoline_return_##x      \
      }

   struct {
      mutex *const tlock;
      void  (*trampoline)(void);

      CoEnter*     tparm;
      cothread_t*  tret;
   } trampolines[0x10] =

   { DefT(0x0), DefT(0x1), DefT(0x2), DefT(0x3)
   , DefT(0x4), DefT(0x5), DefT(0x6), DefT(0x7)
   , DefT(0x8), DefT(0x9), DefT(0xA), DefT(0xB)   
   , DefT(0xC), DefT(0xD), DefT(0xE), DefT(0xF)
   };

#  undef DefT

   atomic<uint64> threadTrampolineCounter;

   auto coPrepare(msize heapSize, CoEnter enter) -> cothread_t {
      uint64 i = threadTrampolineCounter++ & 0xF;
      unique_lock<mutex> __lock(*trampolines[i].tlock);
      cothread_t thr;

      *trampolines[i].tret = co_active();
      *trampolines[i].tparm = enter;
      thr = co_create(heapSize, *trampolines[i].trampoline);
      co_switch(thr);
      *trampolines[i].tret  = nullptr;
      *trampolines[i].tparm = nullptr;

      return thr;
   }
}

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

#define CLS cothread_S
#define C(x) CLS::CPP_EXPAND(x)CPP_EXPAND(CLS)
#define M(x) auto CLS::x
namespace threads {  
   C()  (msize heapsize)
   :    isOccupied  (false)
   ,    isAvailable (true)
   ,    isRunning   (false)
   ,    isWanting   (true)
   ,    detachSoon  (false)
   ,    entry       (nullptr)
   ,    arg         (nullptr)
   ,    coReturn    (nullptr)
   ,    handle      (coPrepare(heapsize, bind(&cothread_S::_main, this)))
   {
      assert(handle);
   }

   C(~) () {}

   ///////////////////
   // Attach/Detach //
   ///////////////////

   M(canAttach)
   () const -> bool {
      return !isOccupied && !isRunning && isAvailable && !entry;
   }

   M(canDetach)
   () const -> bool {
      return !isOccupied && !isRunning && isAvailable && entry;
   }

   M(attach)
   (CothreadEntry _entry, rptr _arg) -> bool {
      unique_lock<mutex> __modifyEntry(modifyEntry);
      if (!canAttach())
         return false;

      isWanting = true;
      entry = _entry;
      arg = _arg;

      return true;
   }

   M(detach)
   () -> tuple<CothreadEntry, rptr> {
      unique_lock<mutex> __modifyEntry(modifyEntry);
      if (!canDetach())
         return make_tuple(nullptr, nullptr);

      tuple<CothreadEntry, rptr> o = make_tuple(entry, arg);

      entry = nullptr;
      arg = nullptr;

      return o;
   }


   /////////////////////////
   // CPU Time Enter/Exit //
   /////////////////////////

   M(canEnter)
   () const -> bool {
      return !isOccupied;
   }

   M(enter)
   () -> void {
      if (isOccupied || !isAvailable)
         return;

      assert(!isOccupied && !coReturn);

      isOccupied = true;
      coReturn = co_active();
      co_switch(handle);
      coReturn = nullptr;
      isOccupied = false;
   }

   /////////////////
   // Destruction //
   /////////////////

   M(signalShutdown)
   () -> bool {
      isAvailable = false;
      return canDelete();
   }

   M(canDelete)
   () -> bool {
      return !isAvailable && !isRunning && !isOccupied;
   }


   //////////
   // MAIN //
   //////////

   M(_main)
   () -> void {
      do {
         while (!_start())
            co_switch(coReturn);


         ccothreadEnv env = 
         {  arg
         ,  [this]()               { co_switch(coReturn); }
         ,  [](cothreadRef co)     { co.enter();          }
         ,  [this]()               { detachSoon = true;   }
         };

         assert(entry);
         isWanting = entry(env);
         isRunning = false;

         if (detachSoon) {
            detach();
            detachSoon = false;
         }

      } while (isAvailable && isWanting);

      assert(coReturn);
      co_switch(coReturn);

      // should never get here
      while (true)
         assert(false);
   }

   M(_start)
   () -> bool {
      unique_lock<mutex> __modifyEntry(modifyEntry);

      if (!isRunning && isAvailable && entry && isWanting) {
         isRunning = true;
         return true;
      }

      return false;
   }

}
